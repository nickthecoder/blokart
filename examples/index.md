## Examples

* [A Simple Box](box.txt)
* [Various Box Styles](boxStyles.txt)
* [Various Line Styles](lineStyles.txt)
* [Class Diagram](classDiagram.txt)
* [Explaining Comment Line-Prefixes](comments.txt)

