package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.blokart.model.DocumentImpl

class TestTJoint : GlokTestCase() {

    fun testTopT() {
        val doc = DocumentImpl()
        doc.setText("""
               ┌─────────┬─────────┐
            ┏━━┷━━┓   ┏━━┷━━┓   ┏━━┷━━┓
            ┃     ┃   ┃     ┃   ┃     ┃
            ┗━━━━━┛   ┗━━━━━┛   ┗━━━━━┛
            """.trimIndent(), autoParse = true)

        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        assertEquals(3, diagram.boxes.size)
        //assertEquals(1, diagram.connections.size)

        val box0 = diagram.boxes[0]
        assertEquals(1, box0.rowA)
        assertEquals(3, box0.rowB)
        assertEquals(0, box0.columnA)
        assertEquals(6, box0.columnB)

        val box1 = diagram.boxes[1]
        assertEquals(1, box1.rowA)
        assertEquals(3, box1.rowB)
        assertEquals(10, box1.columnA)
        assertEquals(16, box1.columnB)

        val box2 = diagram.boxes[2]
        assertEquals(1, box2.rowA)
        assertEquals(3, box2.rowB)
        assertEquals(20, box2.columnA)
        assertEquals(26, box2.columnB)

        assertEquals(2, diagram.connections.size)
        val line0 = diagram.connections[0]
        assertEquals(1, line0.waypoints[0].row)
        assertEquals(3, line0.waypoints[0].column)
        assertEquals(0, line0.waypoints[1].row)
        assertEquals(3, line0.waypoints[1].column)
        assertEquals(0, line0.waypoints[2].row)
        assertEquals(23, line0.waypoints[2].column)
        assertEquals(1, line0.waypoints[3].row)
        assertEquals(23, line0.waypoints[3].column)

        val line1 = diagram.connections[1]
        assertEquals(0, line1.waypoints[0].row)
        assertEquals(13, line1.waypoints[0].column)
        assertEquals(1, line1.waypoints[1].row)
        assertEquals(13, line1.waypoints[1].column)

    }

}
