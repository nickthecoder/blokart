package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.blokart.model.DocumentImpl
import uk.co.nickthecoder.glok.scene.Side

class TestConnectionParser : GlokTestCase() {

    fun testHConnection() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the line
            ───
            After the line
            """.trimIndent(), autoParse = true
        )
        val diagram = doc.diagrams[0]

        val line0 = diagram.connections[0]
        val waypoints0 = line0.waypoints
        assertEquals(2, waypoints0.size)
        assertEquals(0, waypoints0[0].row)
        assertEquals(0, waypoints0[0].column)
        assertEquals(0, waypoints0[1].row)
        assertEquals(2, waypoints0[1].column)

    }

    fun testVConnection() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the line
            │
            │
            │
            After the line
            """.trimIndent(), autoParse = true
        )
        val diagram = doc.diagrams[0]

        val line0 = diagram.connections[0]
        val waypoints0 = line0.waypoints
        assertEquals(2, waypoints0.size)
        assertEquals(0, waypoints0[0].row)
        assertEquals(0, waypoints0[0].column)
        assertEquals(2, waypoints0[1].row)
        assertEquals(0, waypoints0[1].column)

    }

    fun testHConnectionWithEndCaps() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the line
            ╶───╴
            After the line
            """.trimIndent(), autoParse = true
        )
        val diagram = doc.diagrams[0]

        val line0 = diagram.connections[0]
        val waypoints0 = line0.waypoints
        assertEquals(2, waypoints0.size)
        assertEquals(0, waypoints0[0].row)
        assertEquals(0, waypoints0[0].column)
        assertEquals(0, waypoints0[1].row)
        assertEquals(4, waypoints0[1].column)

    }

    fun testVConnectionWithEndCaps() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the line
            ╷
            │
            ╵
            After the line
            """.trimIndent(), autoParse = true
        )
        val diagram = doc.diagrams[0]

        val line0 = diagram.connections[0]
        val waypoints0 = line0.waypoints
        assertEquals(2, waypoints0.size)
        assertEquals(0, waypoints0[0].row)
        assertEquals(0, waypoints0[0].column)
        assertEquals(2, waypoints0[1].row)
        assertEquals(0, waypoints0[1].column)

    }

    fun testConnectionWithEndCaps() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the line
            ╶───╴
            After the line
            """.trimIndent(), autoParse = true
        )
        val diagram = doc.diagrams[0]

        val line0 = diagram.connections[0]
        val waypoints0 = line0.waypoints
        assertEquals(2, waypoints0.size)
        assertEquals(0, waypoints0[0].row)
        assertEquals(0, waypoints0[0].column)
        assertEquals(0, waypoints0[1].row)
        assertEquals(4, waypoints0[1].column)

    }

    fun testBentConnection() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the line
            ──┐
              │
            After the line
            """.trimIndent(), autoParse = true
        )
        val diagram = doc.diagrams[0]

        val line0 = diagram.connections[0]
        val waypoints0 = line0.waypoints
        assertEquals(3, waypoints0.size)
        assertEquals(0, waypoints0[0].row)
        assertEquals(0, waypoints0[0].column)
        assertEquals(0, waypoints0[1].row)
        assertEquals(2, waypoints0[1].column)
        assertEquals(1, waypoints0[2].row)
        assertEquals(2, waypoints0[2].column)

    }

    fun testBentConnection2() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the line
              │
              └──
            After the line
            """.trimIndent(), autoParse = true
        )
        val diagram = doc.diagrams[0]

        val line0 = diagram.connections[0]
        val waypoints0 = line0.waypoints
        assertEquals(3, waypoints0.size)
        assertEquals(0, waypoints0[0].row)
        assertEquals(2, waypoints0[0].column)
        assertEquals(1, waypoints0[1].row)
        assertEquals(2, waypoints0[1].column)
        assertEquals(1, waypoints0[2].row)
        assertEquals(4, waypoints0[2].column)

    }

    fun testSnake() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the line
            
              │
              └──┐
                 │
               │ │
               │ │
               └─┘
            After the line
            """.trimIndent(), autoParse = true
        )
        val diagram = doc.diagrams[0]

        val line0 = diagram.connections[0]
        val waypoints0 = line0.waypoints
        assertEquals(6, waypoints0.size)
        assertEquals(1, waypoints0[0].row)
        assertEquals(2, waypoints0[0].column)
        assertEquals(2, waypoints0[1].row)
        assertEquals(2, waypoints0[1].column)
        assertEquals(2, waypoints0[2].row)
        assertEquals(5, waypoints0[2].column)
        assertEquals(6, waypoints0[3].row)
        assertEquals(5, waypoints0[3].column)
        assertEquals(6, waypoints0[4].row)
        assertEquals(3, waypoints0[4].column)
        assertEquals(4, waypoints0[5].row)
        assertEquals(3, waypoints0[5].column)

    }


    fun testCrossedLines() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the line
              │
              │
             ─┼──
              │
              
            After the line
            """.trimIndent(), autoParse = true
        )
        val diagram = doc.diagrams[0]

        val line0 = diagram.connections[0]
        val waypoints0 = line0.waypoints
        assertEquals(2, waypoints0.size)
        assertEquals(2, waypoints0[0].row)
        assertEquals(1, waypoints0[0].column)
        assertEquals(2, waypoints0[1].row)
        assertEquals(4, waypoints0[1].column)

        val line1 = diagram.connections[0]
        val waypoints1 = line1.waypoints
        assertEquals(2, waypoints1.size)
        assertEquals(2, waypoints1[0].row)
        assertEquals(1, waypoints1[0].column)
        assertEquals(2, waypoints1[1].row)
        assertEquals(4, waypoints1[1].column)

    }

    fun testHUniversalMarkers() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the line
              ◇───◈
            After the line
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]

        val line0 = diagram.connections[0]
        val waypoints0 = line0.waypoints
        assertEquals(2, waypoints0.size)
        assertEquals(0, waypoints0[0].row)
        assertEquals(2, waypoints0[0].column)
        assertEquals(0, waypoints0[1].row)
        assertEquals(6, waypoints0[1].column)

        assertEquals('◇', line0.startMarker?.forDirection(Side.LEFT))
        assertEquals('◇', line0.startMarker?.forDirection(Side.RIGHT))
        assertEquals('◇', line0.startMarker?.forDirection(Side.TOP))
        assertEquals('◇', line0.startMarker?.forDirection(Side.BOTTOM))

        assertEquals('◈', line0.endMarker?.forDirection(Side.LEFT))
        assertEquals('◈', line0.endMarker?.forDirection(Side.RIGHT))
        assertEquals('◈', line0.endMarker?.forDirection(Side.TOP))
        assertEquals('◈', line0.endMarker?.forDirection(Side.BOTTOM))

    }


    fun testOneLineMarker() {
        val doc = DocumentImpl()
        doc.setText(
            """
              ◇─▶─◈
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]

        val line0 = diagram.connections[0]
        assertEquals('▲', line0.midMarker?.north)
    }
}

