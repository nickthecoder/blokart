package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.blokart.model.DocumentImpl

class TestParser : GlokTestCase() {


    fun testEmpty() {
        val doc = DocumentImpl()
        doc.setText("", autoParse = true)
        assertEquals("", doc.textDocument.text)
        assertEquals(0, doc.diagrams.size)
    }

    fun testOnlyPlainText() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Hello
            World
            """.trimIndent(), autoParse = true
        )
        assertEquals("Hello\nWorld", doc.textDocument.text)
        assertEquals(0, doc.diagrams.size)
    }

}
