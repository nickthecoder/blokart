package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.blokart.model.DocumentImpl

/**
 * Tests well-formed documents are parsed correctly.
 * I created the diagrams by hand... I wish I had an ascii-art diagram editor ;-))
 */
class TestBoxParser : GlokTestCase() {

    fun testBox() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the box
            ┌───┐
            │   │
            └───┘
            After the box
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        assertEquals(1, diagram.boxes.size)
        assertEquals(1, diagram.fromRow)
        assertEquals(3, diagram.toRow)

        val box0 = diagram.boxes[0]
        assertEquals(0, box0.rowA)
        assertEquals(2, box0.rowB)
        assertEquals(0, box0.columnA)
        assertEquals(4, box0.columnB)

        //TODO assertEquals("Before the box\n\n\n\nAfter the box", doc.textDocument.text)
    }

    fun testBoxWithTitle() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the box
            ┌─Hi─┐
            │    │
            └────┘
            After the box
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        println("Boxes = ${diagram.boxes}")
        assertEquals(1, diagram.boxes.size)
        assertEquals(1, diagram.labels.size)
        assertEquals(1, diagram.fromRow)
        assertEquals(3, diagram.toRow)

        val box0 = diagram.boxes[0]
        assertEquals(0, box0.rowA)
        assertEquals(2, box0.rowB)
        assertEquals(0, box0.columnA)
        assertEquals(5, box0.columnB)

        val label0 = diagram.labels[0]
        assertEquals(0, label0.row)
        assertEquals(2, label0.fromColumn)
        assertEquals("Hi", label0.text)
        assertEquals(3, label0.rightColumn())

        //TODO assertEquals("Before the box\n\n\n\nAfter the box", doc.textDocument.text)
    }


    fun testBoxWithTitleWithSpaces() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the box
            ┌─ Hi ─┐
            │      │
            └──────┘
            After the box
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        assertEquals(1, diagram.boxes.size)
        assertEquals(1, diagram.labels.size)
        assertEquals(1, diagram.fromRow)
        assertEquals(3, diagram.toRow)

        val box0 = diagram.boxes[0]
        assertEquals(0, box0.rowA)
        assertEquals(2, box0.rowB)
        assertEquals(0, box0.columnA)
        assertEquals(7, box0.columnB)

        val label0 = diagram.labels[0]
        assertEquals(0, label0.row)
        assertEquals(2, label0.fromColumn)
        assertEquals(" Hi ", label0.text)
        assertEquals(5, label0.rightColumn())

        //TODO assertEquals("Before the box\n\n\n\nAfter the box", doc.textDocument.text)
    }

    fun testBoxWithMargins() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the box
            
              ┌───┐
              │   │
              └───┘
              
            After the box
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        assertEquals(1, diagram.boxes.size)
        assertEquals(1, diagram.fromRow)
        assertEquals(5, diagram.toRow)

        val box0 = diagram.boxes[0]
        assertEquals(1, box0.rowA)
        assertEquals(3, box0.rowB)
        assertEquals(2, box0.columnA)
        assertEquals(6, box0.columnB)

        //TODO assertEquals("Before the box\n\n\n\nAfter the box", doc.textDocument.text)
    }


    fun test2Boxes() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the box
            ┌───┐
            │   │ ┌───┐
            └───┘ │   │
                  └───┘
            After the box
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        assertEquals(1, diagram.fromRow)
        assertEquals(4, diagram.toRow)
        assertEquals(2, diagram.boxes.size)

        val box0 = diagram.boxes[0]
        assertEquals(0, box0.rowA)
        assertEquals(2, box0.rowB)
        assertEquals(0, box0.columnA)
        assertEquals(4, box0.columnB)

        val box1 = diagram.boxes[1]
        assertEquals(1, box1.rowA)
        assertEquals(3, box1.rowB)
        assertEquals(6, box1.columnA)
        assertEquals(10, box1.columnB)

        // TODO assertEquals("Before the box\n\n\n\n\nAfter the box", doc.textDocument.text)
    }


    fun testHAlignedBoxes() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the box
            ┌───┐ ┌───┐
            │   │ │   │
            └───┘ └───┘
            After the box
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        assertEquals(2, diagram.boxes.size)
        assertEquals(1, diagram.fromRow)
        assertEquals(3, diagram.toRow)

        val box0 = diagram.boxes[0]
        assertEquals(0, box0.rowA)
        assertEquals(2, box0.rowB)
        assertEquals(0, box0.columnA)
        assertEquals(4, box0.columnB)

        val box1 = diagram.boxes[1]
        assertEquals(0, box1.rowA)
        assertEquals(2, box1.rowB)
        assertEquals(6, box1.columnA)
        assertEquals(10, box1.columnB)

        //TODO assertEquals("Before the box\n\n\n\nAfter the box", doc.textDocument.text)
    }

    fun testHAlignedBoxesNoGap() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the box
            ┌───┐┌───┐
            │   ││   │
            └───┘└───┘
            After the box
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        assertEquals(2, diagram.boxes.size)
        assertEquals(1, diagram.fromRow)
        assertEquals(3, diagram.toRow)

        val box0 = diagram.boxes[0]
        assertEquals(0, box0.rowA)
        assertEquals(2, box0.rowB)
        assertEquals(0, box0.columnA)
        assertEquals(4, box0.columnB)

        val box1 = diagram.boxes[1]
        assertEquals(0, box1.rowA)
        assertEquals(2, box1.rowB)
        assertEquals(5, box1.columnA)
        assertEquals(9, box1.columnB)

        //TODO assertEquals("Before the box\n\n\n\nAfter the box", doc.textDocument.text)
    }

    fun testVAlignedBoxes() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the box
            ┌───┐
            │   │
            └───┘
            
            ┌───┐
            │   │
            └───┘
            After the box
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        assertEquals(2, diagram.boxes.size)
        assertEquals(1, diagram.fromRow)
        assertEquals(7, diagram.toRow)

        val box0 = diagram.boxes[0]
        assertEquals(0, box0.rowA)
        assertEquals(2, box0.rowB)
        assertEquals(0, box0.columnA)
        assertEquals(4, box0.columnB)

        val box1 = diagram.boxes[1]
        assertEquals(4, box1.rowA)
        assertEquals(6, box1.rowB)
        assertEquals(0, box1.columnA)
        assertEquals(4, box1.columnB)

        //TODO assertEquals("Before the box\n\n\n\nAfter the box", doc.textDocument.text)
    }

    fun testVAlignedBoxesNoGap() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the box
            ┌───┐
            │   │
            └───┘
            ┌───┐
            │   │
            └───┘
            After the box
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        assertEquals(2, diagram.boxes.size)
        assertEquals(1, diagram.fromRow)
        assertEquals(6, diagram.toRow)

        val box0 = diagram.boxes[0]
        assertEquals(0, box0.rowA)
        assertEquals(2, box0.rowB)
        assertEquals(0, box0.columnA)
        assertEquals(4, box0.columnB)

        val box1 = diagram.boxes[1]
        assertEquals(3, box1.rowA)
        assertEquals(5, box1.rowB)
        assertEquals(0, box1.columnA)
        assertEquals(4, box1.columnB)

        //TODO assertEquals("Before the box\n\n\n\nAfter the box", doc.textDocument.text)
    }

    /*
    Boxes filled with spaces are no longer supported.

    fun testOverlappingBoxesA() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the box
            ┌───┐
            │  ┌┴──┐
            └──┤   │
               └───┘
            After the box
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        assertEquals(1, diagram.fromRow)
        assertEquals(4, diagram.toRow)
        assertEquals(2, diagram.shapes.size)
        println("Shapes ${diagram.shapes}")

        val box0 = diagram.shapes[0] as Box
        assertEquals(0, box0.fromRow)
        assertEquals(2, box0.toRow)
        assertEquals(0, box0.fromColumn)
        assertEquals(4, box0.toColumn)

        val box1 = diagram.shapes[1] as Box
        assertEquals(1, box1.fromRow)
        assertEquals(3, box1.toRow)
        assertEquals(3, box1.fromColumn)
        assertEquals(7, box1.toColumn)

    }
    */

    /*
    Boxes filled with spaces are no longer supported.

    fun testOverlappingBoxesB() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the box
            ┌───┐
            │   ├──┐
            └──┬┘  │
               └───┘
            After the box
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        assertEquals(1, diagram.fromRow)
        assertEquals(4, diagram.toRow)
        assertEquals(2, diagram.shapes.size)

        val box0 = diagram.shapes[1] as Box
        assertEquals(0, box0.fromRow)
        assertEquals(2, box0.toRow)
        assertEquals(0, box0.fromColumn)
        assertEquals(4, box0.toColumn)

        val box1 = diagram.shapes[0] as Box
        assertEquals(1, box1.fromRow)
        assertEquals(3, box1.toRow)
        assertEquals(3, box1.fromColumn)
        assertEquals(7, box1.toColumn)

    }
    */

    fun testOverlappingBoxesC() {
        val doc = DocumentImpl()
        doc.setText(
            """
            Before the box
            ┌───┐
            │  ┌┼──┐
            └──┼┘  │
               └───┘
            After the box
            """.trimIndent(), autoParse = true
        )
        assertEquals(1, doc.diagrams.size)
        val diagram = doc.diagrams[0]
        assertEquals(1, diagram.fromRow)
        assertEquals(4, diagram.toRow)
        assertEquals(2, diagram.boxes.size)

        val box0 = diagram.boxes[0]
        assertEquals(0, box0.rowA)
        assertEquals(2, box0.rowB)
        assertEquals(0, box0.columnA)
        assertEquals(4, box0.columnB)

        val box1 = diagram.boxes[1]
        assertEquals(1, box1.rowA)
        assertEquals(3, box1.rowB)
        assertEquals(3, box1.columnA)
        assertEquals(7, box1.columnB)

        // TODO assertEquals("Before the box\n\n\n\n\nAfter the box", doc.textDocument.text)

    }
}
