package uk.co.nickthecoder.blokart

import junit.framework.TestCase
import uk.co.nickthecoder.glok.backend.DummyBackend
import uk.co.nickthecoder.glok.backend.backend

abstract class GlokTestCase : TestCase() {
    override fun setUp() {
        backend = DummyBackend()
    }
}
