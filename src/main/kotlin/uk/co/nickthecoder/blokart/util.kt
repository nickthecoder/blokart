package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.control.ContentDisplay
import uk.co.nickthecoder.glok.control.FormGrid
import uk.co.nickthecoder.glok.control.HBox
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.BooleanUnaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.ObservableBoolean
import uk.co.nickthecoder.glok.property.boilerplate.SimpleBooleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.StringProperty
import uk.co.nickthecoder.glok.property.functions.and
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.TINTED
import java.awt.Desktop
import java.io.File

val TRUE_PROPERTY = SimpleBooleanProperty(true).asReadOnly()

fun Side.deltaX() = when (this) {
    Side.RIGHT -> 1
    Side.LEFT -> - 1
    else -> 0
}
fun Side.deltaY() = when (this) {
    Side.BOTTOM -> 1
    Side.TOP -> - 1
    else -> 0
}

fun String.contains(anyOf: Set<Char>): Boolean {
    for (c in anyOf) {
        if (contains(c)) {
            // println("Found special character '$c'")
            return true
        }
    }
    return false
}

fun String.isBlankOrEmptyComment(): Boolean {
    if (isBlank()) return true
    val stripped = trim()
    return (stripped == "*" || stripped == "//")
}


internal fun settingsForm(block: FormGrid.() -> Unit) = scrollPane {
    content = formGrid {
        style(".form")
        block()
    }
}

internal fun information(text: String) = label(text) {
    style(".information")
}

internal fun StringProperty.isDirectory(): ObservableBoolean = BooleanUnaryFunction(this) { File(it).isDirectory }

internal fun Node.withResetProperties(vararg properties: Property<*>) =
    withResetProperties(true, *properties)

internal fun errorMessage(text: String, error: ObservableBoolean) = label(text) {
    textColorProperty.bindTo(Tantalum.accentColorProperty)
    visibleProperty.bindTo(error)
}

internal fun <V> Node.withResetProperty(
    property: Property<V>,
    value: V,
    postUpdate: (() -> Unit)? = null
): HBox {
    val disabled = property.equalTo(value)
    return hBox {
        growPriority = 1f
        alignment = Alignment.TOP_LEFT
        spacing(4)
        + this@withResetProperty
        + button("Reset") {
            style(TINTED)
            disabledProperty.bindTo(disabled)
            graphic = imageView(BlokartApp.resizableIcons.getResizable("refresh"))
            tooltip = TextTooltip("Reset to the default value")
            contentDisplay = ContentDisplay.GRAPHIC_ONLY
            onAction {
                property.value = value
                postUpdate?.invoke()
            }
        }
    }
}

internal fun Node.withResetProperties(
    grow: Boolean,
    vararg properties: Property<*>,
    postUpdate: (() -> Unit)? = null
): HBox {
    var disabled = BlokartSettings.hasDefaultValueProperty(properties.first())
    for (i in 1 until properties.size) {
        disabled = disabled and BlokartSettings.hasDefaultValueProperty(properties[i])
    }

    return hBox {
        if (grow) {
            growPriority = 1f
        }
        fillHeight = false
        alignment = Alignment.TOP_CENTER
        spacing(4)

        + this@withResetProperties
        + spacer()
        + button("Reset") {
            style(TINTED)
            disabledProperty.bindTo(disabled)
            graphic = imageView(Tantalum.icons["refresh"])
            tooltip = TextTooltip("Reset to the default value")
            contentDisplay = ContentDisplay.GRAPHIC_ONLY
            onAction {
                for (property in properties) {
                    BlokartSettings.restoreProperty(property)
                }
                postUpdate?.invoke()
            }
        }
    }
}

