package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.blokart.model.BlockMetaData
import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.GlokSettings.defaultThemeProperty
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.application.posixArguments
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.dock.load
import uk.co.nickthecoder.glok.dock.save
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.row
import uk.co.nickthecoder.glok.scene.dsl.texture
import uk.co.nickthecoder.glok.scene.icons
import uk.co.nickthecoder.glok.text.BitmapFont
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.util.load
import uk.co.nickthecoder.glok.util.save
import java.io.File
import java.util.prefs.Preferences

class BlokartApp : Application() {

    override fun start(primaryStage: Stage) {
        BlokartSettings.load()
        GlokSettings.load()

        // Ensure bitmap fonts include the diagram-drawing glyphs and other special characters.
        // Now, the fonts won't need to be re-built on-the-fly.
        for (c in BlockMetaData.variationsByChar.keys) {
            BitmapFont.requireGlyph(c)
        }
        for (c in BlokartSettings.characterMapProperty.value) {
            if (! c.isWhitespace()) BitmapFont.requireGlyph(c)
        }

        defaultThemeProperty.unbind()
        defaultThemeProperty.bindTo(Tantalum combineWith BlokartTheme)
        gui = MainWindow(primaryStage).apply {
            harbour.load(preferences())
        }

        val posix = posixArguments(emptyList(), emptyList(), emptyList(), rawArguments)

        for (arg in posix.remainder()) {
            val file = File(arg)
            if (file.exists() && file.isFile) {
                gui.openFile(file)
            } else {
                System.err.println("File '$file' not found. Ignoring.")
            }
        }

        primaryStage.onClosed {
            BlokartSettings.save()
            gui.harbour.save(preferences())
            GlokSettings.save()
        }
    }

    companion object {

        lateinit var gui: MainWindow
            private set

        fun preferences(): Preferences = Preferences.userNodeForPackage(BlokartApp::class.java)

        val resources by lazy { backend.resources("uk/co/nickthecoder/blokart") }
        val icons by lazy {
            icons(resources) {
                texture("blokartIcons.png", 64) {
                    row("mode_", 2, 2, 4, "select", "box", "diamond", "line", "label")
                    row(
                        2, 70, 4,
                        "file_new", "file_open", "file_save", "file_save_as", "document_properties"
                    )
                    row(
                        2, 2 + 68 * 2, 4, "edit_copy", "edit_paste", "edit_cut", "edit_delete",
                        "edit_undo", "edit_redo", "select_all", "select_none", "find", "replace"
                    )
                    row(
                        2, 2 + 68 * 3, 4, "to_bottom", "lower", "raise", "to_top", "remove", "add",
                        "select_all", "select_none", "blank"
                    )
                    row(
                        2, 2 + 68 * 4, 4,
                        "insert_plain_before", "insert_plain_after", "insert_diagram", "plain_to_diagram",
                        "option_move_labels_with_boxes", "option_move_connections_with_boxes"
                    )
                    row(2, 2 + 68 * 5, 4, "zoom_in", "zoom_out", "zoom_reset", "find", "replace")
                    row(
                        2, 2 + 68 * 6, 4,
                        "refresh", "file_settings", "debug", "tree", "clear", "edit_reverse_direction"
                    )
                }
            }
        }
        val resizableIcons by lazy {
            icons.resizableIcons(Tantalum.iconSizeProperty)
        }
        val recentFiles = RecentFiles(preferences().node("recent"))

        @JvmStatic
        fun main(vararg args: String) {
            launch(BlokartApp::class, args)
        }
    }
}
