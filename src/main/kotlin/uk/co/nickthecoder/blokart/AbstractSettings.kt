package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.invalidationListener
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.util.log
import java.io.File
import java.util.prefs.Preferences

/**
 * Used to load/save settings as Java [Preferences].
 * I copy/paste this class in many of my projects.
 */
abstract class AbstractSettings(val preferences: Preferences) {

    /**
     * Should preferences be saved automatically?
     */
    val autoSavePreferencesProperty by booleanProperty(true)
    var autoSavePreferences by autoSavePreferencesProperty

    private var loading = false

    /**
     * Listens to all settings properties (when auto-save is enabled).
     */
    private val allPropertiesListener = invalidationListener { property ->
        if (property is Property<*>) {
            if (! loading) {
                val name = property.beanName
                saveProperty(preferences, name, property.value)
                preferences.flush()
            }
        }
    }

    private val defaultValues = mutableMapOf<String, Any?>()


    open fun autoSaveIgnoredProperties(): Set<Property<*>> = emptySet()

    /**
     * Adds or removes listeners to each property.
     * If [autoSavePreferences] == `true`, then any time a property changes, it will be saved.,
     * Note, those properties listed in [autoSaveIgnoredProperties] are never auto-saved.
     */
    protected fun autoSaveChanged() {
        if (autoSavePreferences) {
            val ignore = autoSaveIgnoredProperties()
            for (property in findProperties()) {
                if (ignore.contains(property)) continue
                property.addListener(allPropertiesListener)
            }
        } else {
            for (property in findProperties()) {
                property.removeListener(allPropertiesListener)
            }
        }
    }

    private fun findProperties(): List<Property<*>> {
        val result = mutableListOf<Property<*>>()
        val klass = this.javaClass
        for (method in klass.methods) {
            if (method.name.endsWith("Property")) {
                val returnType = method.returnType
                if (method.parameterCount == 0 && Property::class.java.isAssignableFrom(returnType)) {
                    val property = method.invoke(this) as Property<*>
                    if (property.beanName?.isNotBlank() == false) {
                        log.warn("Property ${method.name} does not have a bean name. Ignoring it.")
                    } else {
                        result.add(property)
                    }
                }
            }
        }
        return result
    }

    protected fun rememberDefaultValues() {
        for (prop in findProperties()) {
            prop.beanName?.let { defaultValues[it] = prop.value }
        }
    }

    fun restoreProperty(property: Property<*>) {
        try {
            // This is a bit naughty, but the value *must* be the correct type.
            @Suppress("unchecked_cast")
            property.beanName?.let { (property as Property<Any?>).value = defaultValues[it] }
        } catch (e: Exception) {
            log.error("Failed to reset default value of $property")
        }
    }

    fun <T> hasDefaultValueProperty( property : Property<T>) : ObservableBoolean {
        val defaultValue = defaultValues[property.beanName] ?: SimpleBooleanProperty(false)
        @Suppress("unchecked_cast")
        return property.equalTo(defaultValue as T)
    }

    fun restoreDefaultValues() {
        for (prop in findProperties()) {
            restoreProperty(prop)
        }
    }


    open fun saveProperty(preferences: Preferences, name: String?, value: Any?) {
        name ?: return
        when (value) {
            is Boolean -> preferences.putBoolean(name, value)
            is Int -> preferences.putInt(name, value)
            is Float -> preferences.putFloat(name, value)
            is Double -> preferences.putDouble(name, value)
            is Enum<*> -> preferences.put(name, value.name)

            // toString is sufficient for all of these types
            is File, is String, is Color ->
                preferences.put(name, value.toString())

            else -> log.warn("Unsupported type ${value?.javaClass} for Setting $name")
        }
    }

    open fun loadProperty(property: Property<*>) {
        val name = property.beanName

        when (property) {
            is BooleanProperty -> property.value = preferences.getBoolean(name, property.value)
            is IntProperty -> property.value = preferences.getInt(name, property.value)
            is FloatProperty -> property.value = preferences.getFloat(name, property.value)
            is DoubleProperty -> property.value = preferences.getDouble(name, property.value)
            is StringProperty -> property.value = preferences.get(name, property.value)
            is FileProperty -> property.value = File(preferences.get(name, property.value.toString()))
            is ColorProperty -> property.value =
                Color[preferences.get(name, property.value.toHashRGBA())]

            else -> {
                val oldValue = property.value
                if (oldValue is Enum<*>) {
                    val enumName = preferences.get(name, oldValue.name)
                    val enumClass: Class<Enum<*>> = oldValue.javaClass
                    val enumValue = enumClass.enumConstants.firstOrNull { it.name == enumName }
                    if (enumValue != null) {
                        log.warn("Ignoring Enum Setting : $property")

                    } else {
                        log.warn("Setting $name. Couldn't find enum value for string : $enumName")
                    }
                } else {
                    log.warn("Ignoring Setting : $property")
                }
            }
        }
    }


    open fun save() {
        log.info("Saving Settings")
        for (property in findProperties()) {
            saveProperty(preferences, property.beanName, property.value)
        }
        preferences.flush()
    }

    open fun load() {
        loading = true

        for (property in findProperties()) {
            try {
                loadProperty(property)
            } catch (e: Exception) {
                log.warn("Failed to load Settings : ${property.beanName}")
            }
        }
        loading = false
    }

}
