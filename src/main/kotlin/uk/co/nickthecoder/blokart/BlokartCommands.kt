package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.scene.NamedImages
import java.io.File

abstract class BlokartCommands() : Commands() {
    abstract fun openFile( file : File)
}
