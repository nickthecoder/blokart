package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.blokart.model.*
import uk.co.nickthecoder.glok.control.Tab
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.and
import uk.co.nickthecoder.glok.property.functions.not
import java.io.File

class State(tabProperty: ObservableValue<Tab?>) {

    private val defaultModeProperty = SimpleProperty<Mode>(Mode.SELECT)

    val currentTabProperty = UnaryFunction(tabProperty) { it as? DocumentTab }
    val currentTab by currentTabProperty

    val noCurrentTabProperty = currentTabProperty.isNull()

    val undoableProperty = DefaultIndirectObservableBoolean(currentTabProperty, true) {
        it.documentView.document.history.undoableProperty
    }
    //val undoable by undoableProperty

    val redoableProperty: ObservableBoolean = DefaultIndirectObservableBoolean(currentTabProperty, true) {
        it.documentView.document.history.redoableProperty
    }
    //val redoable by redoableProperty


    val currentFileProperty: ObservableValue<File?> = DefaultIndirectObservableValue(currentTabProperty, null) {
        it.fileProperty
    }
    //val currentFile by currentFileProperty

    val currentTabTitleProperty: ObservableString = DefaultIndirectObservableString(currentTabProperty, "") {
        it.titleProperty
    }

    val windowTitleProperty = StringUnaryFunction( currentTabTitleProperty ) { title ->
        if (title.isBlank()) {
            "Blokart"
        } else {
            "Blokart - $title"
        }
    }

    val currentDocumentProperty = UnaryFunction(currentTabProperty) {
        it?.documentView?.document
    }
    val currentDocument by currentDocumentProperty


    val modeProperty: Property<Mode> = DefaultIndirectProperty(currentTabProperty, defaultModeProperty) {
        it.modeProperty
    }
    var mode by modeProperty

    val selectedShapeProperty: ObservableValue<Shape?> = DefaultIndirectObservableValue(currentTabProperty, null) {
        it.documentView.selectedShapeProperty
    }
    val selectedShape by selectedShapeProperty

    val selectedShapeDescription: ObservableString = DefaultIndirectObservableString(selectedShapeProperty, "") {
        it.observableDescription
    }

    val noSelectedShapeProperty = selectedShapeProperty.isNull()

    val labelSelectedProperty = BooleanUnaryFunction(selectedShapeProperty) { it is DiagramLabel }
    val boxOrConnectionSelectedProperty = BooleanUnaryFunction(selectedShapeProperty) { it is BoxOrConnection }
    val notBoxOrConnectionSelectedProperty = ! boxOrConnectionSelectedProperty
    val boxSelectedProperty = BooleanUnaryFunction(selectedShapeProperty) { it is Box }
    val connectionSelectedProperty = BooleanUnaryFunction(selectedShapeProperty) { it is Connection }

    val selectedShapeHasLinesProperty: ObservableBoolean = boxOrConnectionSelectedProperty and
        DefaultIndirectObservableBoolean(selectedShapeProperty, true) { shape ->
            if (shape is Box) {
                shape.fillProperty.isNull()
            } else {
                TRUE_PROPERTY
            }
        }

    val cursorRowProperty: ObservableOptionalInt = DefaultIndirectObservableOptionalInt(currentTabProperty, null) {
        it.documentView.cursorRowProperty
    }
    val cursorColumnProperty: ObservableOptionalInt = DefaultIndirectObservableOptionalInt(currentTabProperty, null) {
        it.documentView.cursorColumnProperty
    }

    val caretInDiagramProperty: ObservableBoolean = DefaultIndirectObservableBoolean(currentTabProperty, true) {
        it.documentView.caretInDiagramProperty
    }

    val hintProperty: ObservableString = DefaultIndirectObservableString(currentTabProperty, "") {
        it.documentView.hintProperty
    }
}
