package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.blokart.model.*
import uk.co.nickthecoder.blokart.model.Box
import uk.co.nickthecoder.blokart.model.history.*
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.event.HandlerCombination
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.property
import uk.co.nickthecoder.glok.scene.MousePointer
import uk.co.nickthecoder.glok.text.HighlightRange
import uk.co.nickthecoder.glok.text.ThemedHighlight
import kotlin.math.max
import kotlin.math.min

/**
 * The view is simply a [StyledTextArea], with additional mouse event handlers.
 * When you mouse-press on a diagram, the events are intercepted, and handled here.
 * When you mouse-press elsewhere, the events are handled by [StyledTextArea].
 */
class DocumentView(val document: Document) : WrappedNode<StyledTextArea>(StyledTextArea(document.textDocument)) {

    // region ==== Properties ====

    val modeProperty = SimpleProperty(Mode.SELECT)

    val selectedShapeProperty by property<Shape?>(null)
    var selectedShape by selectedShapeProperty

    private val mutableCursorRowProperty by optionalIntProperty(null)
    val cursorRowProperty = mutableCursorRowProperty.asReadOnly()
    var cursorRow by mutableCursorRowProperty
        private set

    private val mutableCursorColumnProperty by optionalIntProperty(null)
    val cursorColumnProperty = mutableCursorColumnProperty.asReadOnly()
    var cursorColumn by mutableCursorColumnProperty
        private set

    private val behaviourProperty by property<Behaviour>(SelectBehaviour())
    private var behaviour by behaviourProperty

    private val mousePointerProperty: ObservableMousePointer = MousePointerUnaryFunction(modeProperty) { mode ->
        when (mode) {
            Mode.SELECT -> MousePointer.ARROW
            Mode.LABEL -> MousePointer.I_BEAM
            else -> MousePointer.CROSS_HAIR
        }
    }
    private val mousePointer by mousePointerProperty

    val hintProperty: ObservableString =
        StringBinaryFunction(behaviourProperty, selectedShapeProperty) { behaviour, shape ->
            if (shape == null) {
                when (behaviour) {
                    is SelectBehaviour -> "[Click] to select"
                    is BoxBehaviour -> "[Drag] to draw a new [Box]"
                    is ConnectionBehaviour -> "[Drag] to draw a new [Line]"
                    is LabelBehaviour -> "[Click] to add a new [Label]"
                    else -> ""
                }
            } else {
                when (behaviour) {
                    is SelectBehaviour -> {
                        when (shape) {
                            is Connection -> "[Drag] points. [Ctrl+Drag] to add an extra corner"
                            is Box -> "[Drag] corners to [Resize]"
                            is DiagramLabel -> "[Drag] to move. Edit text in the toolbar"
                            else -> ""
                        }
                    }

                    is BoxBehaviour -> "[Drag] corners to resize. [Drag] elsewhere to draw a new [Box]"
                    is ConnectionBehaviour -> "[Drag] corners. [Ctrl+Drag] add extra corners. [Drag] elsewhere to draw a new [Line]"
                    is LabelBehaviour -> "[Drag] to move. [Click] elsewhere to add a new [Label]"
                    else -> ""
                }
            }
        }


    // endregion Properties

    // region ==== Controls ====
    val textArea = inner

    /**
     * This uses knowledge of how a StyledTextArea is composed,
     * which might change in future versions of glok. BAD!
     */
    private val textContent = ((textArea.children[0] as ScrollPane).content as HBox).children[1] as Region

    // endregion controls

    // region ==== Fields ====

    private var diagram: Diagram? = null

    private var controlPoints: List<ControlPoint> = emptyList()

    private val controlPointHighlight = ThemedHighlight(".control_point")

    // endregion Fields

    // region ==== Listeners ====

    @Suppress("unused")
    private val documentListener = document.addWeakListener { _, change, isUndo ->
        when (change) {
            is StructuralChange -> {
                textArea.document.ranges.removeIf { it.owner is Diagram }
                for (diagram in change.document.diagrams) {
                    diagram.invalidate()
                }
                textArea.requestRedraw()
                selectedShape = null
            }

            is DiagramChange -> {
                change.diagram.invalidate()
                textArea.requestRedraw()
                if ((change is DeleteShape && ! isUndo) || (change is NewShape && isUndo)) {
                    selectedShape = null
                }
                if ((change is PlainTextToDiagram && isUndo) || (change is DiagramToPlainText && ! isUndo)) {
                    (change.diagram as DiagramImpl).removeHighlight(textArea.document)
                }
            }
        }
    }


    @Suppress("unused")
    private val selectedShapeListener = selectedShapeProperty.addWeakChangeListener { _, _, shape ->
        textArea.document.ranges.removeIf { it.owner == this }
        rebuildControlPoints(shape)
        // Give the focus to the DocumentView, and NOT the TextArea.
        // Therefore, keyboard shortcuts (such as DELETE) won't be handled by the TextArea.
        // When shape == DiagramLabel, the focus is moved to the TextField where the label is edited.
        if (shape != null && shape !is DiagramLabel) requestFocus()
    }

    @Suppress("unused")
    private val caretListener = textArea.caretPositionProperty.addWeakListener {
        caretMoved()
    }.apply {
        textArea.anchorPositionProperty.addListener(this)
    }

    val caretInDiagramProperty: ObservableBoolean = textArea.readOnlyProperty
    //val caretInDiagram by caretInDiagramProperty

    // endregion

    // region ==== init ====
    init {
        // StyledTextArea must never perform its own undo/redo.
        // Undo/redo shortcuts are handled by GUI's commands.
        with(TextAreaActions) {
            with(textArea.commands) {
                removeAction(UNDO)
                removeAction(REDO)
            }
        }

        modeProperty.addChangeListener { _, _, mode ->
            selectedShape = null
            behaviour = when (mode) {
                Mode.SELECT -> SelectBehaviour()
                Mode.BOX -> BoxBehaviour()
                Mode.LINE -> ConnectionBehaviour()
                Mode.LABEL -> LabelBehaviour()
            }
        }

        onMouseEntered {
            scene?.stage?.mousePointer = mousePointer
        }
        onMouseMoved {
            scene?.stage?.mousePointer = mousePointer
        }
        onMouseExited {
            scene?.stage?.mousePointer = MousePointer.ARROW
        }

        with(textArea) {
            onMouseMoved { event ->
                val globalRow = glassToRow(event.sceneY)
                val diagram = this@DocumentView.document.diagramAtRow(globalRow)
                if (diagram == null) {
                    cursorRow = null
                    cursorColumn = null
                } else {
                    cursorRow = globalRow - diagram.fromRow
                    cursorColumn = glassToColumn(event.sceneX)
                }
            }
            onPopupTrigger(HandlerCombination.BEFORE) { event ->
                event.consume() // Prevent StyledTextArea from handling this event.
            }
            onMousePressed(HandlerCombination.BEFORE) { event ->
                val globalRow = glassToRow(event.sceneY)
                diagram = this@DocumentView.document.diagramAtRow(globalRow)
                diagram?.let { diagram ->
                    val row = globalRow - diagram.fromRow
                    val column = glassToColumn(event.sceneX)
                    behaviour.onMousePressed(event, diagram, row, column)
                }
                if (diagram == null) {
                    selectedShape = null
                } else {
                    event.consume() // Prevent StyledTextArea from handling this event.
                }
            }
            onMouseReleased(HandlerCombination.BEFORE) { event ->
                diagram?.let { diagram ->
                    val row = glassToRow(event.sceneY) - diagram.fromRow
                    val column = glassToColumn(event.sceneX)
                    behaviour.onMouseReleased(event, diagram, row, column)
                    event.consume() // Prevent StyledTextArea from handling this event.
                }
            }
            onMouseClicked(HandlerCombination.BEFORE) { event ->
                diagram?.let { diagram ->
                    val row = glassToRow(event.sceneY) - diagram.fromRow
                    val column = glassToColumn(event.sceneX)
                    behaviour.onMouseClicked(event, diagram, row, column)
                }
                event.consume() // Prevent StyledTextArea from handling this event.
            }
            onMouseDragged(HandlerCombination.BEFORE) { event ->
                diagram?.let { diagram ->
                    val row = glassToRow(event.sceneY) - diagram.fromRow
                    val column = glassToColumn(event.sceneX)
                    cursorRow = row
                    cursorColumn = column
                    behaviour.onMouseDragged(event, diagram, row, column)
                }
                event.consume() // Prevent StyledTextArea from handling this event.
            }
        }
    }

    // endregion init

    // region ==== Methods ====

    fun selectAll() {
        val diagram = diagram
        if (diagram == null) {
            selectAllText()
        } else {
            selectAllShapes(diagram)
        }
    }

    fun selectAllText() {
        selectedShape = null
        with(textArea) {
            caretPosition = TextPosition(0, 0)
            anchorPosition = TextPosition(document.lines.size - 1, document.lines.last().length)
        }
    }

    fun selectAllShapes(diagram: Diagram) {
        val group = ShapeGroup(diagram)
        for (shapes in diagram.allShapes) {
            group.shapes.addAll(shapes)
        }
        selectedShape = group
    }

    fun selectNone() {
        selectedShape = null
        with(textArea) {
            anchorPosition = caretPosition
        }
    }


    private fun glassToColumn(sceneX: Float): Int {
        val fontWidth = textArea.font.fixedWidth
        return ((sceneX - textContent.sceneX - textContent.surroundLeft()) / fontWidth).toInt()
    }

    private fun glassToRow(sceneY: Float): Int {
        val lineHeight = textArea.font.height * textArea.lineHeight
        return ((sceneY - textContent.sceneY - textContent.surroundTop()) / lineHeight).toInt()
    }

    private fun caretMoved() {
        val caretRow = textArea.caretPosition.row
        val anchorRow = textArea.anchorPosition.row
        val fromRow = min(caretRow, anchorRow)
        val toRow = max(caretRow, anchorRow)

        val inDiagram = document.diagrams.firstOrNull { diagram ->
            fromRow <= diagram.toRow && toRow >= diagram.fromRow
        }
        textArea.readOnly = inDiagram != null
    }

    private fun rebuildControlPoints(shape: Shape?) {
        textArea.document.ranges.removeIf { it.owner == this }
        if (shape == null) {
            controlPoints = emptyList()
        } else {
            controlPoints = shape.controlPoints()
            diagram?.let { diagram ->

                for (cp in controlPoints) {
                    textArea.document.ranges.add(
                        HighlightRange(
                            TextPosition(cp.row + diagram.fromRow, cp.column),
                            TextPosition(cp.row + diagram.fromRow, cp.column + 1),
                            controlPointHighlight,
                            owner = this
                        )
                    )
                }
            }
        }
    }
    // endregion

    // region ==== Layout ====
    override fun drawChildren() {
        for (diagram in document.diagrams) {
            if (diagram.ensureUpdated(textArea.document)) {
                selectedShape?.let { rebuildControlPoints(selectedShape) }
            }
        }
        super.drawChildren()
    }
    // endregion

    // region == inner class Behaviour ==
    open inner class Behaviour {
        open fun onMousePressed(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {}
        open fun onMouseReleased(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {}
        open fun onMouseClicked(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {}
        open fun onMouseDragged(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {}
    }
    // endregion

    // region == inner class SelectBehaviour ==
    inner class SelectBehaviour : Behaviour() {

        override fun onMousePressed(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {
            // We do not drag DiagramLabel control point.
            // We can move a label my dragging ANY part of it, not just the control point.
            val shape = selectedShape
            val controlPoint = controlPoints.firstOrNull { it.row == row && it.column == column }
            if (controlPoint != null) {
                when (shape) {
                    is Box -> {
                        event.capture()
                        behaviour = DragBoxCornerBehaviour(shape, controlPoint)
                        return
                    }

                    is Connection -> {
                        event.capture()
                        behaviour = DragWaypointBehaviour(shape, controlPoint as Waypoint)
                        return
                    }
                }
            }

            if (shape != null && shape.isTouching(row, column)) {
                selectedShape = shape
                event.capture()
                behaviour = DragShapeBehaviour(shape, row, column)
                return
            }

            for (shapes in diagram.allShapes.asReversed()) {
                for (foundShape in shapes.asReversed()) {
                    if (foundShape.isTouching(row, column)) {
                        selectedShape = foundShape
                        event.capture()
                        behaviour = DragShapeBehaviour(foundShape, row, column)
                        return
                    }
                }
            }
            selectedShape = null
            // Might be a start of a drag to select shapes within a bounding box.
            behaviour = DragBoundingBox(diagram, row, column)
            event.capture()
        }
    }
    // endregion

    // region == inner class DragBoundingBox ==
    inner class DragBoundingBox(diagram: Diagram, val rowA: Int, val columnA: Int) : Behaviour() {

        val group = ShapeGroup(diagram)

        override fun onMouseDragged(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {
            val left = min(columnA, column)
            val right = max(columnA, column)
            val top = min(rowA, row)
            val bottom = max(rowA, row)

            group.shapes.clear()
            for (shapes in diagram.allShapes) {
                for (shape in shapes) {
                    if (shape.leftColumn() >= left && shape.rightColumn() <= right &&
                        shape.topRow() >= top && shape.bottomRow() <= bottom
                    ) {
                        group.shapes.add(shape)
                    }
                }
            }
            rebuildControlPoints(group)
        }

        override fun onMouseReleased(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {
            behaviour = SelectBehaviour()
            if (group.shapes.isEmpty()) {
                selectedShape = null
            } else {
                selectedShape = group
            }
        }
    }

    // endregion DragBoundingBox

    // region == inner class DragBoxCornerBehaviour ==
    inner class DragBoxCornerBehaviour(
        val box: Box,
        val controlPoint: ControlPoint,
        val onFinished: ((Boolean) -> Unit)? = null
    ) : Behaviour() {

        init {
            if (controlPoint.shape.diagram.document.history.currentBatch() == null) {
                controlPoint.shape.diagram.document.history.beginBatch("Drag control point")
            }
        }

        override fun onMouseDragged(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {
            val batch = diagram.document.history.currentBatch() ?: return
            val clippedColumn = max(column, 0)
            val clippedRow = max(row, 0)
            if (controlPoint.row != row || controlPoint.column != clippedColumn) {
                if (controlPoint.shape.canMoveControlPoint(controlPoint, row, clippedColumn)) {
                    if (clippedRow >= diagram.height) {
                        batch.addChange(GrowDiagramImpl(diagram, false, clippedRow - diagram.height + 1))
                    }
                    batch.addChange(
                        MoveBoxCornerImpl(
                            box,
                            controlPoint as CornerControlPoint,
                            clippedRow,
                            clippedColumn
                        )
                    )
                }
            }
        }

        override fun onMouseReleased(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {
            diagram.document.history.endBatch()
            if (onFinished == null) {
                behaviour = SelectBehaviour()
                rebuildControlPoints(controlPoint.shape)
            } else {
                onFinished.invoke(true)
            }
        }
    }
    // endregion

    // region == inner class DragWayPointBehaviour ==
    inner class DragWaypointBehaviour(
        val connection: Connection,
        val controlPoint: Waypoint,
        val onFinished: ((Boolean) -> Unit)? = null
    ) : Behaviour() {

        private var addedBend = false
        private val history = controlPoint.shape.diagram.document.history
        private var batchAlreadyStarted = history.currentBatch() != null

        init {
            if (! batchAlreadyStarted) {
                history.beginBatch("Drag waypoint point")
            }
        }

        override fun onMouseDragged(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {
            val batch = history.currentBatch() ?: return
            if (event.isControlDown) {
                if (! addedBend) {
                    batch.addChange(AddBendImpl(connection, controlPoint as WaypointImpl))
                    addedBend = true
                }
            } else {
                addedBend = false
            }

            val clippedRow = max(row, 0)
            val clippedColumn = max(column, 0)
            if (controlPoint.row != clippedRow || controlPoint.column != clippedColumn) {
                if (clippedRow >= diagram.height) {
                    batch.addChange(GrowDiagramImpl(diagram, false, clippedRow - diagram.height + 1))
                }
                if (controlPoint.shape.canMoveControlPoint(controlPoint, clippedRow, clippedColumn)) {
                    batch.addChange(
                        MoveWaypointImpl(connection, controlPoint as WaypointImpl, clippedRow, clippedColumn)
                    )
                }
            }
        }

        override fun onMouseReleased(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {
            history.currentBatch()?.addChange(PostMoveWaypoint(connection))
            if (!batchAlreadyStarted) {
                diagram.document.history.endBatch()
            }
            if (onFinished == null) {
                selectedShape = null // Force waypoints to be rebuilt.
                selectedShape = connection
                behaviour = SelectBehaviour()
            } else {
                onFinished.invoke(true)
            }
        }
    }

    // endregion

    // region == inner class DragShapeBehaviour ==
    inner class DragShapeBehaviour(
        val shape: Shape,
        var currentRow: Int,
        var currentColumn: Int,
        val onFinished: ((Boolean) -> Unit)? = null
    ) : Behaviour() {

        private val additionalShapes: List<Shape> = if (BlokartSettings.moveLabelsWithBoxes && shape is Box) {
            shape.diagram.labels.filter { shape.contains(it) }
        } else {
            emptyList()
        }

        private val touchingWaypoints = if (BlokartSettings.moveConnectionsWithBoxes && shape is Box) {
            shape.touchingWaypoints()
        } else {
            emptyList()
        }

        init {
            shape.diagram.document.history.beginBatch("Move shape")
        }

        override fun onMouseDragged(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {
            val batch = diagram.document.history.currentBatch() ?: return
            val rowDelta = row - currentRow
            var columnDelta = column - currentColumn
            if (rowDelta != 0 || columnDelta != 0) {
                val newLeft = shape.leftColumn() + columnDelta
                val newTop = shape.topRow() + rowDelta
                val newBottom = shape.bottomRow() + rowDelta
                if (newLeft < 0) {
                    columnDelta -= newLeft
                } else {
                    currentColumn = column
                }
                if (newTop < 0) {
                    batch.addChange(GrowDiagramImpl(diagram, true, - newTop))
                    // Note. currentRow is later set to row, which makes the mouse out-of sync with the point being dragged.
                    // However, not setting it is worse, as it causes rows to be continuously added with barely
                    // and mouse movements.
                } else if (newBottom >= diagram.height) {
                    batch.addChange(GrowDiagramImpl(diagram, false, newBottom - diagram.height + 1))
                }
                currentRow = row
                batch.addChange(
                    MoveShapeImpl(shape, rowDelta, columnDelta, additionalShapes, touchingWaypoints)
                )
            }
        }

        override fun onMouseReleased(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {
            diagram.document.history.endBatch()
            if (onFinished == null) {
                behaviour = SelectBehaviour()
                rebuildControlPoints(shape)
            } else {
                onFinished.invoke(true)
            }
        }

    }

    // endregion

    // region == inner class BoxBehaviour ==
    inner class BoxBehaviour : Behaviour() {
        override fun onMousePressed(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {
            val shape = selectedShape
            if (shape is Box) {
                val controlPoint = controlPoints.firstOrNull { it.row == row && it.column == column }
                if (controlPoint != null) {
                    event.capture()
                    behaviour = DragBoxCornerBehaviour(shape, controlPoint) {
                        behaviour = this
                    }
                    return
                }
            }

            event.capture()
            val newBox = Box(diagram, row, column, row + 1, column + 1)
            diagram.document.history.beginBatch("New Box")
            diagram.document.history.currentBatch()?.addChange(NewShapeImpl(newBox))
            selectedShape = newBox
            behaviour = DragBoxCornerBehaviour(newBox, newBox.controlPoints()[3]) { finished ->
                selectedShape = null
                if (finished) {
                    selectedShape = newBox
                    diagram.document.history.endBatch()
                } else {
                    diagram.document.history.abortBatch()
                }
                behaviour = this
            }
        }
    }
    // endregion

    // region == inner class ConnectionBehaviour ==
    inner class ConnectionBehaviour : Behaviour() {
        override fun onMousePressed(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {

            val shape = selectedShape
            if (shape is Connection) {
                val controlPoint = controlPoints.firstOrNull { it.row == row && it.column == column } as? Waypoint
                if (controlPoint != null) {
                    event.capture()
                    behaviour = DragWaypointBehaviour(shape, controlPoint) {
                        behaviour = this
                    }
                    return
                }
            }
            event.capture()
            val newConnection = Connection(diagram).apply {
                waypoints.add(WaypointImpl(this, row, column, event.isShiftDown))
                waypoints.add(WaypointImpl(this, row, column, ! event.isShiftDown))
                waypoints.add(WaypointImpl(this, row, column, event.isShiftDown))
            }

            diagram.document.history.beginBatch("New Line")
            diagram.document.history.currentBatch()?.addChange(NewShapeImpl(newConnection))
            selectedShape = newConnection
            behaviour = DragWaypointBehaviour(newConnection, newConnection.controlPoints()[2]) { finished ->
                selectedShape = null
                if (finished) {
                    if (newConnection.waypoints.size > 1) {
                        selectedShape = newConnection
                        diagram.document.history.endBatch()
                    } else {
                        diagram.document.history.abortBatch()
                    }
                } else {
                    diagram.document.history.abortBatch()
                }
                behaviour = this
            }
        }
    }
    // endregion LineBehaviour

    // region == inner class LabelBehaviour ==
    inner class LabelBehaviour : Behaviour() {
        override fun onMousePressed(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {
            val label = diagram.labels.asReversed().firstOrNull { it.isTouching(row, column) }
            if (label is DiagramLabel) {
                event.capture()
                behaviour = DragShapeBehaviour(label, row, column) { _ ->
                    selectedShape = null
                    behaviour = this
                }
            }

        }

        override fun onMouseClicked(event: MouseEvent, diagram: Diagram, row: Int, column: Int) {
            val shape = DiagramLabel(diagram, row, column, "New")
            NewShapeImpl(shape).now()
            selectedShape = shape
        }
    }
// endregion

}
