package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.blokart.controls.*
import uk.co.nickthecoder.blokart.model.BoxOrConnection
import uk.co.nickthecoder.blokart.model.Connection
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dialog.alert
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.dock.MapDockFactory
import uk.co.nickthecoder.glok.dock.inspector.NodeInspectorDock
import uk.co.nickthecoder.glok.dock.places.PlacesDock
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.borderPane
import uk.co.nickthecoder.glok.scene.dsl.documentTabPane
import uk.co.nickthecoder.glok.scene.dsl.scene
import uk.co.nickthecoder.glok.scene.dsl.vBox
import java.io.File

class MainWindow(val stage: Stage) {

    // region ==== Controls ====
    private val tabPane = documentTabPane()

    val commands = object : BlokartCommands() {
        override fun openFile(file: File) {
            this@MainWindow.openFile(file)
        }
    }

    val state = State(tabPane.selection.selectedItemProperty)

    val harbour = Harbour()

    private val nodeInspectorDock = NodeInspectorDock(harbour)
    private val characterMapDock = CharacterMapDock(harbour)
    private val placesDock = object : PlacesDock(harbour) {
        override fun openFile(file: File) {
            this@MainWindow.openFile(file)
        }

        override fun editPlaces() {
            SettingsDialog.show(scene !!.stage !!, SettingsTab.PLACES)
        }
    }

    init {
        harbour.dockFactory = MapDockFactory(nodeInspectorDock, characterMapDock, placesDock)
    }
    // endregion controls


    // region ==== Commands ====
    init {
        commands.apply {
            with(BlokartActions) {
                ESCAPE { state.mode = Mode.SELECT }
                FILE_NEW { newTab(DocumentTab(null)) }
                FILE_OPEN { fileOpen() }
                FILE_SAVE { state.currentTab?.save() }.disable(state.currentFileProperty.isNull())
                FILE_SAVE_AS { state.currentTab?.saveAs() }.disable(state.currentTabProperty.isNull())
                FILE_SETTINGS { SettingsDialog.show(stage) }
                FILE_CLOSE_TAB { tabPane.selection.selectedItem?.requestClose() }

                EDIT_UNDO {
                    state.currentDocument?.history?.undo()
                    state.mode = Mode.SELECT
                }.disable(! state.undoableProperty)

                EDIT_REDO {
                    state.currentDocument?.history?.redo()
                    state.mode = Mode.SELECT
                }.disable(! state.redoableProperty)

                EDIT_REVERSE_CONNECTION {
                    (state.selectedShape as? Connection)?.reverse()
                }.disable(! state.connectionSelectedProperty)

                EDIT_DELETE {
                    state.selectedShape?.let { shape ->
                        shape.diagram.deleteShape(shape)
                    }
                }.disable(state.noSelectedShapeProperty)

                CYCLE_LINE_THICKNESS {
                    (state.selectedShape as? BoxOrConnection)?.let { shape ->
                        shape.changeThickness((shape.thickness + 1) % 3)
                    }
                }.disable(state.notBoxOrConnectionSelectedProperty)
                CYCLE_LINE_STYLE {
                    (state.selectedShape as? BoxOrConnection)?.let { shape ->
                        shape.changeLineStyle((shape.lineStyle + 1) % 3)
                    }
                }.disable(state.notBoxOrConnectionSelectedProperty)
                TOGGLE_ROUNDED {
                    (state.selectedShape as? BoxOrConnection)?.let { shape ->
                        shape.changeRounded(! shape.rounded)
                    }
                }.disable(state.notBoxOrConnectionSelectedProperty)

                MOVE_UP { state.selectedShape?.move(- 1, 0) }.disable(state.noSelectedShapeProperty)
                MOVE_DOWN { state.selectedShape?.move(1, 0) }.disable(state.noSelectedShapeProperty)
                MOVE_LEFT { state.selectedShape?.move(0, - 1) }.disable(state.noSelectedShapeProperty)
                MOVE_RIGHT { state.selectedShape?.move(0, 1) }.disable(state.noSelectedShapeProperty)

                SELECT_ALL { state.currentTab?.documentView?.selectAll() }.disable(state.noCurrentTabProperty)
                SELECT_NONE { state.currentTab?.documentView?.selectNone() }.disable(state.noCurrentTabProperty)

                toggle(VIEW_PLACES, placesDock.visibleProperty)
                toggle(VIEW_CHARACTER_MAP, characterMapDock.visibleProperty)
                toggle(VIEW_NODE_INSPECTOR, nodeInspectorDock.visibleProperty)

                toggle(OPTION_MOVE_LABELS_WITH_BOXES, BlokartSettings.moveLabelsWithBoxesProperty)
                toggle(OPTION_MOVE_CONNECTIONS_WITH_BOXES, BlokartSettings.moveConnectionsWithBoxesProperty)

                INSERT_PLAIN_BEFORE {
                    state.selectedShape?.let { shape ->
                        val diagram = shape.diagram
                        state.currentTab?.documentView?.document?.insertPlainText(
                            TextPosition(diagram.fromRow, 0)
                        )
                    }
                }.disable(state.noSelectedShapeProperty)
                INSERT_PLAIN_AFTER {
                    state.selectedShape?.let { shape ->
                        val diagram = shape.diagram
                        state.currentTab?.documentView?.document?.insertPlainText(
                            TextPosition(diagram.toRow, diagram.document.textDocument.lines[diagram.toRow].length)
                        )
                    }
                }.disable(state.noSelectedShapeProperty)

                INSERT_DIAGRAM {
                    state.currentTab?.let { tab ->
                        tab.documentView.document.insertDiagram(tab.documentView.textArea.caretPosition.row)
                    }
                }.disable(state.noCurrentTabProperty)

                PLAIN_TO_DIAGRAM { plainTextToDiagram() }.disable(state.noCurrentTabProperty)


                radioChoices(state.modeProperty) {
                    choice(MODE_SELECT, Mode.SELECT).disable(state.noCurrentTabProperty)
                    choice(MODE_BOX, Mode.BOX).disable(state.noCurrentTabProperty)
                    choice(MODE_LINE, Mode.LINE).disable(state.noCurrentTabProperty)
                    choice(MODE_LABEL, Mode.LABEL).disable(state.noCurrentTabProperty)
                }
            }
        }
    }
    // endregion

    init {
        stage.apply {
            with(stage) {
                titleProperty.bindTo(state.windowTitleProperty)
                scene(900, 650) {
                    root = borderPane {
                        commands.attachTo(this)
                        top = vBox {
                            fillWidth = true
                            + mainMenuBar(commands)
                            + mainToolBar(commands, state)
                            + selectionToolBar(commands, state)
                        }
                        bottom = mainStatusBar(state)

                        center = harbour.apply {
                            content = borderPane {
                                left = modeToolBar(commands)
                                center = tabPane
                            }
                        }
                    }
                }
                show()
                onCloseRequested { event ->
                    if (checkUnsavedTabs()) event.consume()
                }
            }
        }
    }

    // region ==== Private Methods ====

    /**
     * @return true to prevent the window being closed.
     */
    private fun checkUnsavedTabs(): Boolean {
        val unsavedNames = mutableListOf<String>()
        for (tab in tabPane.tabs) {
            tab as? DocumentTab ?: continue
            if (! tab.documentView.document.history.isSaved) {
                unsavedNames.add(tab.file?.name ?: "New Document")
            }
        }

        alert(
            stage, "Save Documents?", "Save Documents?",
            """
                    The following documents have been modified.
                    Do you want to save them before closing?
                    
                    """.trimIndent() + unsavedNames.joinToString(prefix = "\n    ", separator = "\n    "),
            AlertType.INFORMATION, listOf(ButtonType.SAVE, ButtonType.DISCARD, ButtonType.CANCEL)
        ) { buttonType ->
            when (buttonType) {
                ButtonType.SAVE -> {
                    // If there is more than 1 unsaved New Documents, then saveAs the first one, but leave
                    // the others unsaved and still open.
                    var savedNewDocument = false

                    for (tab in tabPane.tabs.toList()) {
                        tab as? DocumentTab ?: continue
                        val documentView = tab.documentView
                        if (! documentView.document.history.isSaved) {
                            if (tab.file == null) {
                                if (! savedNewDocument) {
                                    tabPane.selection.selectedItem = tab
                                    tab.saveAs(close = true)
                                    savedNewDocument = true
                                }
                            } else {
                                tab.save()
                            }
                        }
                        if (documentView.document.history.isSaved) {
                            tabPane.tabs.remove(tab)
                        }
                    }
                }

                ButtonType.DISCARD -> {
                    tabPane.tabs.clear()
                }

                else -> {}
            }
            if (tabPane.tabs.isEmpty()) {
                stage.close()
            }
        }

        return unsavedNames.isNotEmpty()
    }

    private fun plainTextToDiagram() {
        state.currentTab?.let { tab ->
            val document = tab.documentView.document
            val selectedShape = tab.documentView.selectedShape
            if (selectedShape == null) {
                val textArea = tab.documentView.textArea
                if (textArea.caretPosition == textArea.anchorPosition) {
                    alert(
                        stage, "Convert between PlainText and Diagram",
                        "Nothing selected",
                        """
                        Select a shape to convert the diagram to text,
                        or select a region of plain-text to convert it to a diagram.
                        """.trimIndent(),
                        AlertType.INFORMATION,
                        listOf(ButtonType.OK),
                    ) {}
                } else {
                    val startRow = textArea.selectionStart.row
                    val endRow = textArea.selectionEnd.row
                    textArea.anchorPosition = textArea.caretPosition
                    // Convert plain-text to diagram
                    document.plainTextToDiagram(startRow, endRow)
                }
            } else {
                // Convert diagram to plain-text.
                document.diagramToPlainText(selectedShape.diagram)
            }
        }
    }

    private fun newTab(tab: DocumentTab) {
        tabPane.tabs.add(tab)
        tabPane.selection.selectedItem = tab
    }

    private fun fileOpen() {
        FileDialog().apply {
            title = "Open"
            state.currentTab?.file?.parentFile?.let { initialDirectory = it }
            extensions.add(ExtensionFilter("All files", "*"))
            showOpenDialog(stage) { file ->
                if (file != null) {
                    openFile(file)
                }
            }
        }
    }

    fun openFile(file: File) {
        newTab(DocumentTab(file))
        BlokartApp.recentFiles.remember(file)
    }

    // endregion
}
