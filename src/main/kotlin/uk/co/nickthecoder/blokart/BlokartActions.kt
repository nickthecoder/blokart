package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.event.Key

object BlokartActions : Actions(BlokartApp.icons) {

    val ESCAPE = define("escape", "Escape", Key.ESCAPE.noMods())

    // region ==== FILE ====
    val FILE = define("file", "File")

    val FILE_NEW = define("file_new", "New", Key.N.control())
    val FILE_OPEN = define("file_open", "Open", Key.O.control())
    val FILE_SAVE = define("file_save", "Save", Key.S.control())
    val FILE_SAVE_AS = define("file_save_as", "Save As...", Key.S.control().shift())
    val FILE_SETTINGS = define("file_settings", "Settings...") { tinted = true }
    val FILE_CLOSE_TAB = define("close_tab", "Close Tab", Key.W.control())
    // endregion

    // region ==== EDIT ====
    val EDIT = define("edit", "Edit")
    val EDIT_UNDO = define("edit_undo", "Undo", Key.Z.control())
    val EDIT_REDO = define("edit_redo", "Redo", Key.Z.control().shift())
    val EDIT_DELETE = define("delete", "Delete", Key.DELETE.noMods())
    val EDIT_REVERSE_CONNECTION =
        define("edit_reverse_direction", "Reverse Direction", Key.R.control()) { tinted = true }
    val SELECT_ALL = define("select_all", "Select All", Key.A.control())
    val SELECT_NONE = define("select_none", "Clear Selection", Key.A.control().shift())

    val CYCLE_LINE_THICKNESS = define("cycle_line_thickness", "Cycle Line Thickness", Key.T.noMods())
    val CYCLE_LINE_STYLE = define("cycle_line_style", "Cycle Line Style", Key.L.noMods())
    val TOGGLE_ROUNDED = define("toggle_rounded", "Toggle Rounded", Key.R.noMods())

    // endregion

    // region ==== Movement ===
    val MOVE_LEFT = define("move_left", "Move Left", Key.LEFT.noMods())
    val MOVE_RIGHT = define("move_right", "Move Right", Key.RIGHT.noMods())
    val MOVE_UP = define("move_up", "Move Up", Key.UP.noMods())
    val MOVE_DOWN = define("move_down", "Move Down", Key.DOWN.noMods())
    // endregion Movement

    // region ==== VIEW ====
    val VIEW = define("view", "View")

    val VIEW_PLACES = define("view_places", "Places", Key.DIGIT_1.alt())
    val VIEW_CHARACTER_MAP = define("view_node_inspector", "Character Map", Key.DIGIT_2.alt())
    val VIEW_NODE_INSPECTOR = define("view_node_inspector", "Node Inspector", Key.DIGIT_9.alt())
    // endregion

    // region ==== INSERT ====
    val INSERT = define("insert", "Insert")
    val INSERT_PLAIN_BEFORE = define("insert_plain_before", "Plain Text before Diagram") {
        tooltip = "Insert plain-text before the current diagram"
    }
    val INSERT_PLAIN_AFTER = define("insert_plain_after", "Plain Text after Diagram") {
        tooltip = "Insert plain-text at after the current diagram"
    }
    val INSERT_DIAGRAM = define("insert_diagram", "Insert Diagram")
    val PLAIN_TO_DIAGRAM = define("plain_to_diagram", "Plain-Text <-> Diagram")
    // endregion

    // region ==== OPTIONS ====
    val OPTION_MOVE_LABELS_WITH_BOXES = define("option_move_labels_with_boxes", "Move Labels with Boxes")
    val OPTION_MOVE_CONNECTIONS_WITH_BOXES = define("option_move_connections_with_boxes", "Move Connections with Boxes")
    // endregion options

    // region ==== MODE ====
    val MODE_SELECT = define("mode_select", "Select", Key.DIGIT_1.control())
    val MODE_BOX = define("mode_box", "Box", Key.DIGIT_2.control())
    val MODE_LINE = define("mode_line", "Line", Key.DIGIT_3.control())
    val MODE_LABEL = define("mode_label", "Diagram Label", Key.DIGIT_4.control())
    // endregion
}
