package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.glok.property.IndirectProperty
import uk.co.nickthecoder.glok.property.boilerplate.colorProperty
import uk.co.nickthecoder.glok.property.boilerplate.floatProperty
import uk.co.nickthecoder.glok.scene.Color
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.Theme
import uk.co.nickthecoder.glok.theme.ThemeBuilder
import uk.co.nickthecoder.glok.theme.dsl.theme
import uk.co.nickthecoder.glok.theme.styles.LABEL
import uk.co.nickthecoder.glok.theme.styles.STYLED_TEXT_AREA
import uk.co.nickthecoder.glok.theme.styles.TEXT


object BlokartTheme : ThemeBuilder() {

    val lineHeightProperty by floatProperty(1.2f)
    var lineHeight by lineHeightProperty

    val darkDiagramColorProperty by colorProperty(Color["#b1ecb1"])
    val lightDiagramColorProperty by colorProperty(Color["#0a0"])
    var diagramColorProperty = IndirectProperty(Tantalum.darkProperty) { isDark ->
        if (isDark) darkDiagramColorProperty else lightDiagramColorProperty
    }
    var diagramColor by diagramColorProperty

    val darkDiagramBackgroundColorProperty by colorProperty(Color["#02a10211"])
    val lightDiagramBackgroundColorProperty by colorProperty(Color["#0f01"])
    val diagramBackgroundColorProperty = IndirectProperty(Tantalum.darkProperty) { isDark ->
        if (isDark) darkDiagramBackgroundColorProperty else lightDiagramBackgroundColorProperty
    }
    var diagramBackgroundColor by diagramBackgroundColorProperty

    val darkControlPointColorProperty by colorProperty(Color["#196318ff"])
    val lightControlPointColorProperty by colorProperty(Color["#ccf"])
    val controlPointColorProperty = IndirectProperty(Tantalum.darkProperty) { isDark ->
        if (isDark) darkControlPointColorProperty else lightControlPointColorProperty
    }
    var controlPointColor by controlPointColorProperty


    init {
        dependsOn(
            lineHeightProperty,
            diagramColorProperty, diagramBackgroundColorProperty, controlPointColorProperty
        )
    }

    override fun buildTheme(): Theme = theme {

        "SettingsDialog" {
            padding(10)
        }

        LABEL {
            ".information" {
                font(Tantalum.font.italic())
                textColor(Tantalum.fontColor2.opacity(0.8f))
            }
        }

        LABEL {
            ".key_combination" {
                font(Tantalum.font.italic())
                textColor(Tantalum.fontColor2.opacity(0.8f))
            }
        }

        ".settings_form" {
            padding(20)
            borderSize(1)
            plainBorder()
            borderColor(Tantalum.strokeColor)
        }


        STYLED_TEXT_AREA {
            lineHeight(lineHeight)

            descendant(TEXT) {
                ".blokart_diagram" {
                    textColor(diagramColor)
                    plainBackground()
                    backgroundColor(diagramBackgroundColor)
                }
                ".control_point" {
                    roundedBackground(5)
                    backgroundColor(controlPointColor)
                }

                ".syntax_keyword" { textColor("#008") }
                ".syntax_string" { textColor("#080") }
                ".syntax_comment" { textColor("#888") }
                ".syntax_paren" { textColor("#c3c") }
                ".syntax_brace" { textColor("#3cc") }
                ".syntax_bracket" { textColor("#da0") }
                ".syntax_semicolon" { textColor("#979") }
                ".syntax_number" { textColor("#00f") }
                ".syntax_error" {
                    underlineBackground()
                    backgroundColor(Color["#800"])
                }
                ".syntax_pair" {
                    textColor("#9fc")
                    plainBackground()
                    backgroundColor(Color["#000"])
                }
            }
        }
    }

}
