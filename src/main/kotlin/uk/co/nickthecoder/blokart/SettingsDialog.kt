package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.control.TabPane
import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.dock.places.Places
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.FIXED_WIDTH


class SettingsDialog : Dialog() {

    val tabPane = TabPane()

    private val characterMapTextArea = textArea(BlokartSettings.characterMapProperty.value)
    private val snippetsTextArea = textArea(BlokartSettings.snippetsProperty.value)

    @Suppress("unused")
    private val characterMapListener = characterMapTextArea.focusedProperty.addWeakChangeListener { _, _, focused ->
        if (! focused) {
            BlokartSettings.characterMapProperty.value = characterMapTextArea.text
        }
    }

    @Suppress("unused")
    private val snippetsListener = snippetsTextArea.focusedProperty.addWeakChangeListener { _, _, focused ->
        if (! focused) {
            BlokartSettings.snippetsProperty.value = snippetsTextArea.text
        }
    }

    init {
        content = buildContent()
        title = "Settings"
        buttonBar.visible = false
    }


    private fun buildContent() = singleContainer {
        overridePrefWidth = 1000f
        overridePrefHeight = 670f
        fillHeight = true
        fillWidth = true
        style("SettingsDialog")
        padding(0)

        content = borderPane {
            center = tabPane.apply {
                side = Side.LEFT

                + tab("Appearance") {
                    content = appearanceSettings()
                }
                + tab("Layout") {
                    content = layoutForm()
                }
                + tab("Comments") {
                    content = commentsSettings()
                }
                + tab("Snippets") {
                    content = snippetsSettings()
                }
                + tab("Character Map") {
                    content = characterMapSettings()
                }
                + tab("Places") {
                    content = placesForm()
                }
                + tab("Miscellaneous") {
                    content = miscellaneousSettings()
                }
            }
        }
    }


    private fun miscellaneousSettings() = settingsForm {

        + row("Mid Line Markers") {
            right = hBox {
                alignment = Alignment.CENTER_LEFT
                spacing(10)
                + label("X:")
                + intSpinner {
                    editor.prefColumnCount = 3
                    min = 3
                    max = 100
                    valueProperty.bidirectionalBind(BlokartSettings.minXDistanceForMidLineMarkersProperty)
                }
                + label("Y:")
                + intSpinner {
                    editor.prefColumnCount = 3
                    min = 3
                    max = 100
                    valueProperty.bidirectionalBind(BlokartSettings.minYDistanceForMidLineMarkersProperty)
                }
            }.withResetProperties(
                BlokartSettings.minXDistanceForMidLineMarkersProperty,
                BlokartSettings.minYDistanceForMidLineMarkersProperty
            )
            below = information("The minimum distance for markers to appear")
        }

        + row("Trim Ends of Lines") {
            right = checkBox {
                selectedProperty.bidirectionalBind(BlokartSettings.trimEndsOfLinesProperty)
            }.withResetProperties(BlokartSettings.trimEndsOfLinesProperty)
            below = information( "Only applies to diagrams, not plain-text sections." )
        }
    }

    private fun layoutForm() = settingsForm {

        + row("Show Menu Bar") {
            right = checkBox {
                selectedProperty.bidirectionalBind(BlokartSettings.showMenuBarProperty)
            }
        }
        + row("Show Toolbar") {
            right = checkBox {
                selectedProperty.bidirectionalBind(BlokartSettings.showToolBarProperty)
            }
        }
        + row("Show Status Bar") {
            right = checkBox {
                selectedProperty.bidirectionalBind(BlokartSettings.showStatusBarProperty)
            }
        }
    }

    private fun appearanceSettings() = scrollPane {

        content = Tantalum.appearanceForm().apply {

            fun indexAfterRow(id: String): Int {
                val existing = rows.firstOrNull { it.id == id }
                return if (existing == null) rows.size else rows.indexOf(existing) + 1
            }

            val lineHeight = row("Line Height") {
                right = floatSpinner {
                    editor.prefColumnCount = 6
                    valueProperty.bidirectionalBind(BlokartSettings.lineHeightProperty)
                    smallStep = 0.05f
                    largeStep = 0.1f
                }
                below = vBox {
                    + information("Most text editors use 1.2.")
                    + information("Diagrams look better using 1.0, but plain-text looks cramped.")
                }
            }
            rows.add(indexAfterRow("textSize"), lineHeight)

            val toolLineHeight = row("Tool Icon Size") {
                right = intSpinner {
                    editor.prefColumnCount = 6
                    valueProperty.bidirectionalBind(BlokartSettings.toolIconSizeProperty)
                    smallStep = 1
                    largeStep = 4
                }
            }
            rows.add(indexAfterRow("iconSize"), toolLineHeight)

            + rowSeparator()
            + rowHeading("BlokArt Extra Colors")

            + row("Diagram Text Color") {
                visibleProperty.bindTo(Tantalum.darkProperty)
                right = colorButton {
                    title = "Diagram Text Color"
                    colorProperty.bidirectionalBind(BlokartSettings.darkDiagramTextColorProperty)
                }.withResetProperties(BlokartSettings.darkDiagramTextColorProperty)
            }
            + row("Diagram Text Color") {
                visibleProperty.bindTo(! Tantalum.darkProperty)
                right = colorButton {
                    title = "Diagram Text Color"
                    colorProperty.bidirectionalBind(BlokartSettings.lightDiagramTextColorProperty)
                }.withResetProperties(BlokartSettings.lightDiagramTextColorProperty)
            }

            + row("Diagram Background") {
                visibleProperty.bindTo(Tantalum.darkProperty)
                right = colorButton {
                    title = "Diagram Background Color"
                    chooseAlpha = true
                    colorProperty.bidirectionalBind(BlokartSettings.darkDiagramBackgroundColorProperty)
                }.withResetProperties(BlokartSettings.darkDiagramBackgroundColorProperty)
            }
            + row("Diagram Background") {
                visibleProperty.bindTo(! Tantalum.darkProperty)
                right = colorButton {
                    title = "Diagram Background Color"
                    chooseAlpha = true
                    colorProperty.bidirectionalBind(BlokartSettings.lightDiagramBackgroundColorProperty)
                }.withResetProperties(BlokartSettings.lightDiagramBackgroundColorProperty)
            }

            + row("Control Point") {
                visibleProperty.bindTo(Tantalum.darkProperty)
                right = colorButton {
                    title = "Control Point Color"
                    chooseAlpha = true
                    colorProperty.bidirectionalBind(BlokartSettings.darkControlPointColorProperty)
                }.withResetProperties(BlokartSettings.darkControlPointColorProperty)
            }

            + row("Control Point Color") {
                visibleProperty.bindTo(! Tantalum.darkProperty)
                right = colorButton {
                    title = "Control Point Color"
                    chooseAlpha = true
                    colorProperty.bidirectionalBind(BlokartSettings.lightControlPointColorProperty)
                }.withResetProperties(BlokartSettings.lightControlPointColorProperty)
            }

        }
    }

    private fun placesForm() = scrollPane {
        content = Places.editPlacesForm(Places.sharedPlaces)
    }

    private fun commentsSettings() = settingsForm {

        + row {
            above = vBox {
                + label("Line prefixes which are not included as part of a diagram.")
                + label("The comment-prefix is shown in the diagram, but cannot be changed")
            }
        }

        + row("Enable Comments") {
            right = checkBox {
                selectedProperty.bidirectionalBind(BlokartSettings.enableCommentsProperty)
            }.withResetProperties(BlokartSettings.enableCommentsProperty)
        }

        + row("Comments") {
            right = textArea {
                growPriority = 1f
                textProperty.bidirectionalBind(BlokartSettings.commentsProperty)
                prefRowCount = 12
            }.withResetProperties(BlokartSettings.commentsProperty)
            below = information("Place each prefix on a separate line. Leading spaces are ignored.")
        }
    }

    private fun snippetsSettings() = vBox {
        fillWidth = true
        spacing(4)
        overridePrefHeight = 10_000f
        overridePrefWidth = 10_000f
        padding(12)
        + vBox {
            + information("Each line should be in the format")
            + information("   LABEL : TEXT")
        }.withResetProperties(false, BlokartSettings.snippetsProperty) {
            snippetsTextArea.text = BlokartSettings.snippetsProperty.value
        }
        + snippetsTextArea.apply {
            style(FIXED_WIDTH)
            growPriority = 1f
        }
    }

    private fun characterMapSettings() = vBox {
        fillWidth = true
        overridePrefHeight = 10_000f
        overridePrefWidth = 10_000f
        padding(12)
        + vBox {
            + label("Choose the characters available within the Character Map dock")
            + information("Each section must start with :")
            + information("[SECTION LABEL]")
            + information("And then on subsequent lines, any characters. Spaces are ignored.")
        }.withResetProperties(false, BlokartSettings.characterMapProperty) {
            characterMapTextArea.text = BlokartSettings.characterMapProperty.value
        }
        + characterMapTextArea.apply {
            style(FIXED_WIDTH)
            growPriority = 1f
        }
    }

    companion object {
        private var dialog: SettingsDialog? = null

        fun show(fromStage: Stage, settingsTab: SettingsTab = SettingsTab.APPEARANCE) {
            val dialog = dialog ?: SettingsDialog().apply {
                dialog = this
                createStage(fromStage) {
                    onClosed {
                        BlokartSettings.characterMapProperty.value = characterMapTextArea.text
                        BlokartSettings.save()
                        dialog = null
                    }
                }
            }
            dialog.apply {
                tabPane.selection.selectedIndex = settingsTab.index
                stage.show()
            }
        }
    }
}

enum class SettingsTab(val index: Int) {
    APPEARANCE(0),
    LAYOUT(1),
    COMMENTS(2),
    SNIPPETS(3),
    CHARACTER_MAP(4),
    PLACES(5),
    MISCELLANEOUS(6),
}
