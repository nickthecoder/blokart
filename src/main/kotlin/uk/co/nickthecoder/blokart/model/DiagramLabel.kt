package uk.co.nickthecoder.blokart.model

import uk.co.nickthecoder.glok.property.boilerplate.StringBinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty

/**
 * Some text, which is part of the diagram, rather than
 */
class DiagramLabel(
    override val diagram: Diagram,
    row: Int,
    fromColumn: Int,
    text: String
) : Shape {

    val rowProperty by intProperty(row)
    var row by rowProperty

    val fromColumnProperty by intProperty(fromColumn)
    var fromColumn by fromColumnProperty

    val textProperty by stringProperty(text)
    var text by textProperty


    override val observableDescription = StringBinaryFunction(rowProperty, fromColumnProperty) { row, column ->
        "Label @ $column , $row"
    }
    override val description by observableDescription

    private val controlPoints = listOf(LabelControlPoint(this))

    override fun copyTo(diagram: Diagram): DiagramLabel {
        val new = DiagramLabel(diagram, row, fromColumn, text)
        return new
    }

    override fun topRow() = row
    override fun bottomRow() = row
    override fun rightColumn() = fromColumn + text.length - 1
    override fun leftColumn() = fromColumn

    override fun render(grid: List<CharArray>) {
        for ((index, c) in text.withIndex()) {
            grid[row][fromColumn + index] = c
        }
    }

    override fun isTouching(row: Int, column: Int): Boolean {
        return row == this.row && column >= fromColumn && column <= rightColumn()
    }

    override fun controlPoints() = controlPoints

    override fun toString() = "DiagramLabel '$text' @ $row,$fromColumn"
}
