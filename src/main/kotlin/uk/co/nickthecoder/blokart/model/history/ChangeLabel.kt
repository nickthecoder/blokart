package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.Diagram
import uk.co.nickthecoder.blokart.model.DiagramLabel
import uk.co.nickthecoder.blokart.model.Document
import uk.co.nickthecoder.glok.history.ChangeImpl

interface ChangeLabel : DiagramChange {
    val diagramLabel: DiagramLabel
    val newText: String
}

class ChangeLabelImpl(
    override val diagramLabel: DiagramLabel,
    override val newText: String
) : ChangeLabel, ChangeImpl {

    override val label get() = "Label"
    override val diagram: Diagram get() = diagramLabel.diagram
    override val document: Document get() = diagramLabel.diagram.document

    private val oldText = diagramLabel.text

    override fun redo() {
        diagramLabel.text = newText
    }

    override fun undo() {
        diagramLabel.text = oldText
    }

    // TODO Merge
}

