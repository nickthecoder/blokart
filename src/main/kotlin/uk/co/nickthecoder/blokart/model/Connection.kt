package uk.co.nickthecoder.blokart.model

import uk.co.nickthecoder.blokart.BlokartSettings
import uk.co.nickthecoder.blokart.deltaX
import uk.co.nickthecoder.blokart.deltaY
import uk.co.nickthecoder.blokart.model.history.ReverseConnectionImpl
import uk.co.nickthecoder.glok.property.boilerplate.SimpleStringProperty
import uk.co.nickthecoder.glok.scene.Side
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

/**
 * A line, made up of multiple alternating horizontal/vertical lines.
 */
class Connection(
    override val diagram: Diagram,
) : BoxOrConnection {

    override val observableDescription = SimpleStringProperty("")
    override val description by observableDescription

    override var rounded: Boolean = false
    override var thickness = 0
    override var lineStyle = 0

    val waypoints = mutableListOf<Waypoint>()

    var startMarker: LineMarker? = null
    var midMarker: LineMarker? = null
    var endMarker: LineMarker? = null

    init {
        updateDescription()
    }

    fun updateDescription() {
        val first = waypoints.firstOrNull() ?: return
        val last = waypoints.lastOrNull() ?: return
        observableDescription.value = "Connection ${first.column},${first.row} to ${last.column},${last.row}"
    }

    fun reverse() {
        ReverseConnectionImpl(this).now()
    }

    override fun copyTo(diagram: Diagram): Connection {
        val new = Connection(diagram)
        new.rounded = rounded
        new.thickness = thickness
        new.lineStyle = lineStyle
        new.startMarker = startMarker
        new.midMarker = midMarker
        new.endMarker = endMarker
        new.waypoints.addAll(waypoints.map { WaypointImpl(new, it.row, it.column, it.isHorizontal) })
        return new
    }

    override fun topRow() = waypoints.minOf { it.row }
    override fun bottomRow() = waypoints.maxOf { it.row }
    override fun rightColumn() = waypoints.maxOf { it.column }
    override fun leftColumn() = waypoints.minOf { it.column }

    override fun render(grid: List<CharArray>) {
        val horizontal = bestHorizontalLine(thickness, lineStyle = lineStyle)
        val vertical = bestVerticalLine(thickness, lineStyle = lineStyle)
        val bestTopLeft = bestCorner(top = true, left = true, thickness, rounded = rounded)
        val bestTopRight = bestCorner(top = true, left = false, thickness, rounded = rounded)
        val bestBottomLeft = bestCorner(top = false, left = true, thickness, rounded = rounded)
        val bestBottomRight = bestCorner(top = false, left = false, thickness, rounded = rounded)

        var prev = waypoints.firstOrNull() ?: return
        var current = waypoints[1]

        val startRow = prev.row
        val startColumn = prev.column
        val startDX = prev.column - current.column
        val startDY = prev.row - current.row

        var index = 1
        while (index < waypoints.size) {
            val next = if (index < waypoints.size - 1) waypoints[index + 1] else null

            // If there are two waypoints at the same place, skip both of them
            if (next != null && current.row == next.row && current.column == next.column) {
                if (index + 2 < waypoints.size) current = waypoints[index + 2]
                index += 2
                continue
            }
            if (current.isHorizontal) {
                // Horizontal line
                grid.mergeHLineExclusive(current.row, prev.column, current.column, horizontal)
                // Corner
                if (next != null) {
                    val corner = if (prev.column < current.column) {
                        if (next.row > current.row) bestTopRight else bestBottomRight
                    } else {
                        if (next.row > current.row) bestTopLeft else bestBottomLeft
                    }
                    grid.merge(current.row, current.column, corner)
                }
                // Mid line marker
                if (midMarker != null && abs(prev.column - current.column) > BlokartSettings.minXDistanceForMidLineMarkers) {
                    val marker = if (current.column > prev.column) midMarker !!.east else midMarker !!.west
                    grid[current.row][(prev.column + current.column) / 2] = marker
                }
            } else {
                // Vertical line
                grid.mergeVLineExclusive(prev.row, current.row, current.column, vertical)
                // Corner
                if (next != null) {
                    val corner = if (prev.row < current.row) {
                        if (next.column > current.column) bestBottomLeft else bestBottomRight
                    } else {
                        if (next.column > current.column) bestTopLeft else bestTopRight
                    }
                    grid.merge(current.row, current.column, corner)
                }
                // Mid line marker
                if (midMarker != null && abs(prev.row - current.row) > BlokartSettings.minYDistanceForMidLineMarkers) {
                    val marker = if (current.row > prev.row) midMarker !!.south else midMarker !!.north
                    grid[(prev.row + current.row) / 2][current.column] = marker
                }
            }

            index ++
            if (next != null) {
                prev = current
                current = next
            }
        }

        // Initial line-ending
        val startDirection = if (startDY < 0) {
            Side.TOP
        } else if (startDY > 0) {
            Side.BOTTOM
        } else if (startDX < 0) {
            Side.LEFT
        } else {
            Side.RIGHT
        }
        if (startMarker == null) {
            // Draw an end-cap
            grid.merge(startRow, startColumn, bestEndCap(startDirection, thickness))
        } else {
            // Draw a LineMarker
            // Are we touching a box?
            val touchingBox = boxTouchingFirst()
            val startChar = startMarker !!.forDirection(startDirection)
            if (touchingBox == null) {
                grid[startRow][startColumn] = startChar
            } else {
                // Draw the line-ending pointing towards the box (1 away), not over the box's edge.
                grid[startRow - startDirection.deltaY()][startColumn - startDirection.deltaX()] = startChar
            }
        }

        // Final line ending
        val endRow = current.row
        val endColumn = current.column
        val endDirection = if (prev.row < endRow) {
            Side.BOTTOM
        } else if (prev.row > endRow) {
            Side.TOP
        } else if (prev.column < endColumn) {
            Side.RIGHT
        } else {
            Side.LEFT
        }
        if (endMarker == null) {
            // Draw an end-cap
            grid.merge(endRow, endColumn, bestEndCap(endDirection, thickness))
        } else {
            // Draw a LineMarker
            // Are we touching a box?
            val touchingBox = boxTouchingLast()
            val endChar = endMarker !!.forDirection(endDirection)
            if (touchingBox == null) {
                grid[endRow][endColumn] = endChar
            } else {
                // Draw the line-ending pointing towards the box (1 away), not over the box's edge.
                grid[endRow - endDirection.deltaY()][endColumn - endDirection.deltaX()] = endChar
            }
        }

    }

    fun boxTouchingFirst(): Box? {
        val first = waypoints.firstOrNull() ?: return null
        return diagram.findBoxEdge(first.row, first.column)
    }

    fun boxTouchingLast(): Box? {
        val last = waypoints.lastOrNull() ?: return null
        return diagram.findBoxEdge(last.row, last.column)
    }

    override fun isTouching(row: Int, column: Int): Boolean {
        var prev = waypoints.firstOrNull() ?: return false
        for (i in 1 until waypoints.size) {
            val next = waypoints[i]
            if (next.isHorizontal) {
                if (row == next.row &&
                    column >= min(prev.column, next.column) &&
                    column <= max(prev.column, next.column)
                ) return true
            } else {
                if (column == next.column &&
                    row >= min(prev.row, next.row) &&
                    row <= max(prev.row, next.row)
                ) return true
            }
            prev = next
        }
        return false
    }

    override fun controlPoints() = waypoints

    override fun canMoveControlPoint(controlPoint: ControlPoint, toRow: Int, toColumn: Int): Boolean {
        return waypoints.none { it !== controlPoint && it.row == toRow && it.column == toColumn }
    }

    override fun toString() = "Connection " + waypoints.joinToString(separator = " : ") { "${it.row},${it.column}" }

}
