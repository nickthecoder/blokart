package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.Diagram
import uk.co.nickthecoder.blokart.model.DiagramImpl
import uk.co.nickthecoder.blokart.model.Document
import uk.co.nickthecoder.blokart.model.DocumentImpl
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.history.ChangeImpl

interface InsertDiagram : StructuralChange, DiagramChange {
    val fromRow: Int
}

class InsertDiagramImpl(
    override val document: Document,
    override val fromRow: Int,
    val lines: Int = 5
) : InsertDiagram, ChangeImpl {

    override val label get() = "Insert Diagram"
    override val diagram: Diagram = DiagramImpl(document, fromRow, fromRow + lines)

    override fun redo() {
        document.changingPlainText {
            document.textDocument.insert(TextPosition(fromRow, 0), "\n".repeat(lines))
        }
        (document as DocumentImpl).diagrams.add(diagram)

    }

    override fun undo() {
        (document as DocumentImpl).diagrams.add(diagram)
        document.changingPlainText {
            document.textDocument.delete(TextPosition(fromRow, 0), TextPosition(fromRow + lines, 0))
        }
    }
}
