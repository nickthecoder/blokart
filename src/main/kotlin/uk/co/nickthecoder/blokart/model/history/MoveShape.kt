package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.*

interface MoveShape : ShapeChange<Shape>

data class TouchingWaypoint(
    val waypoint: Waypoint,
    val neighbour: Waypoint,
    val isHorizontal: Boolean
)

class MoveShapeImpl(
    shape: Shape,
    val rowDelta: Int,
    val columnDelta: Int,
    val additionalShapes: List<Shape> = emptyList(),
    val moveWaypoints: List<TouchingWaypoint> = emptyList(),

    ) : MoveShape, ShapeChangeImpl<Shape>(shape) {

    override val label get() = "Move Shape"

    override fun redo() {
        if (shape is ShapeGroup) {
            for (child in shape.shapes) {
                moveShape(child, rowDelta, columnDelta)
            }
        } else {
            moveShape(shape, rowDelta, columnDelta)
            for (other in additionalShapes) {
                moveShape(other, rowDelta, columnDelta)
            }
            moveWaypoints(moveWaypoints, rowDelta, columnDelta)
        }
    }

    override fun undo() {
        if (shape is ShapeGroup) {
            for (child in shape.shapes) {
                moveShape(child, - rowDelta, - columnDelta)
            }
        } else {
            moveShape(shape, - rowDelta, - columnDelta)
            for (other in additionalShapes) {
                moveShape(other, - rowDelta, - columnDelta)
            }
            moveWaypoints(moveWaypoints, - rowDelta, - columnDelta)
        }
    }

    companion object {

        internal fun moveWaypoints(moveWaypoints: List<TouchingWaypoint>, rowDelta: Int, columnDelta: Int) {
            for (moveWaypoint in moveWaypoints) {
                val point = moveWaypoint.waypoint as WaypointImpl
                val neighbour = moveWaypoint.neighbour as WaypointImpl
                point.row += rowDelta
                point.column += columnDelta
                if (moveWaypoint.isHorizontal) {
                    neighbour.row += rowDelta
                } else {
                    neighbour.column += columnDelta
                }
            }
        }

        internal fun moveShape(shape: Shape, rowDelta: Int, columnDelta: Int) {
            when (shape) {
                is Box -> {
                    shape.rowA += rowDelta
                    shape.rowB += rowDelta
                    shape.columnA += columnDelta
                    shape.columnB += columnDelta
                }

                is Connection -> {
                    for (point in shape.waypoints) {
                        point as WaypointImpl
                        point.row += rowDelta
                        point.column += columnDelta
                    }
                }

                is DiagramLabel -> {
                    shape.row += rowDelta
                    shape.fromColumn += columnDelta
                }
            }
        }
    }
}

