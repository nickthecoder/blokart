package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.BlokartSettings
import uk.co.nickthecoder.blokart.model.*
import uk.co.nickthecoder.glok.util.log
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

/**
 * Parse source text, and splits it into plain-text and diagrams.
 * As this class mutates the model, it must only be called from `redo` of
 * [PlainTextToDiagram] if you wish `Undo/Redo` to work correctly.
 *
 * Parsing the diagram sections is akin to recovering an .svg file from an exported .png file.
 * This won't produce good results for some documents. e.g.
 *
 *    ┌───┐
 *    │   │
 *    └──┬┴┐
 *       └─┘
 *
 * The large box's bottom-right corner isn't a corner glyph, it is an inverted T.
 * Ditto for the small box's top-left corner.
 *
 */
internal class Parser(val diagram: DiagramImpl, val lines: List<String>) {

    val width = lines.maxOfOrNull { it.trimEnd().length + 1 } ?: 1
    val grid: List<CharArray> = lines.map { line ->
        val extra = width - line.length + 1
        if (extra > 0) {
            line + " ".repeat(extra)
        } else {
            line
        }.toCharArray()
    }

    private val hLines = mutableListOf<HorizontalLine>()
    private val vLines = mutableListOf<VerticalLine>()
    private val boxes = mutableListOf<Box>()

    /*
        Look for lines starting with spaces, followed by "comments", such as // or * (for javadoc comments)

        Find all portions of text which are not spaces nor block-building character.
            Create DiagramLabel's for these, and then replace them with WILDCARD.
            NOTE. At this point, we also include additional spaces before and after the label, if they exist.
            We later trim the label UNLESS the spaces obscure a part of a Box.

        Find all horizontal and vertical line segments.
            WILDCARD is considered part of a horizontal or vertical line segment.
            Later we'll also check if the ends are line endings (arrows etc.)

        Look for complete boxes
            (four corners, joined by horizontal/vertical symbols, or WILDCARD)
            Remove the boxes, replacing them with WILDCARD
            Order them by width, the smallest first, so that boxes inside boxes work correctly
                (with the inner boxes higher in the z-order).
                This is only needed if Boxes can be filled with spaces, which is no longer supported.

            Replace the box edges with WILDCARD.

        Look for partial boxes (any 3 corners, and their 2 edges are complete)
            Replace these boxes with WILDCARD as before.

        Next look for solid boxes like these :
                ▒▒▒▒▒▒▒            ▒Title▒
                ▒Hello▒   ▒▒▒X     ▒▒▒▒▒▒▒
                ▒▒▒▒▒▒▒   ▒▒▒▒     ▒▒▒▒▒▒▒

            As most editors use a line-height of 120%, these look a little ugly due to the gap between lines.
            Note that the text would be replaced by WILDCARD at this point, so the parser would see :

                ▒▒▒▒▒▒▒            ▒*****▒
                ▒*****▒   ▒▒▒*     ▒▒▒▒▒▒▒
                ▒▒▒▒▒▒▒   ▒▒▒▒     ▒▒▒▒▒▒▒

            Look for "background" characters : ░▒▓█
            Group them into connected regions of the same shading.
            In the example above we would have 3 regions.
            For each connected region, check if they form a box containing only the shading character or WILDCARD.
                (That check isn't being done!)
                Create a filled Box.
            Note, two boxes touching (with the same background) are invalid. (the parser would fail).
            We don't need to replace with WILDCARD, as the shading characters cannot interfere with the rest of the Parser.

        Any remaining lines are not considered as boxes, only as Connections.

        For each h/v line,
            Tag it as used
            Recursively look for unused lines starting/ending at the start/end of the line
                Tag them as used
            Join all connected h/v lines together to form a single Connection object.

    */
    /**
     *
     * Tabs are not allowed here. Pre-process the tabs if required.
     */
    fun parse() {
        /*
        for (line in grid) {
            println(line.joinToString(separator = ", ") {
                BlockMetaData.variationsByChar[it]?.basicBlock?.javaClass?.simpleName ?: "?"
            })
        }
        */
        if (BlokartSettings.enableComments) {
            findCommentPrefix()
        }
        findLabels()
        findHLines()
        findVLines()

        //println("HLines\n" + hLines.joinToString(separator = "\n"))
        //println("VLines\n" + vLines.joinToString(separator = "\n"))

        findWholeBoxes()
        for (box in boxes) {
            finishBox(box)
        }
        diagram.boxes.addAll(0, boxes)
        boxes.clear()

        while (findPartialBoxes()) {
            // Do nothing
        }
        for (box in boxes) {
            finishBox(box)
        }
        diagram.boxes.addAll(0, boxes)
        boxes.clear()

        findFilledBoxes()

        trimLabels()

        findConnections()
        //println("Parsed Shapes : \n" + diagram.shapes.joinToString(separator = "\n"))
    }

    private fun findFilledBoxes() {
        for (group in BackgroundScanner(grid).scan()) {
            val box = Box(diagram, group.fromRow, group.fromColumn, group.toRow, group.toColumn)
            box.fill = group.c
            diagram.boxes.add(0, box)
            //println("Fill box : $box")
        }
    }

    private fun findCommentPrefix() {
        val firstLine = lines.first()
        for (column in firstLine.indices) {
            val stripped = firstLine.substring(column)
            for (comment in BlokartSettings.comments.split("\n").sortedBy { - it.length }) {
                if (stripped.startsWith(comment)) {
                    val prefix = " ".repeat(firstLine.indexOf(comment)) + comment
                    // Check that ALL rows start the same
                    for (line in lines) {
                        if (! line.startsWith(prefix)) {
                            // We only add a prefix if ALL lines match.
                            return
                        }
                    }
                    for (row in grid.indices) {
                        for (col in prefix.indices) {
                            grid[row][col] = ' '
                        }
                    }
                    diagram.prefix = prefix
                }
            }
        }
    }

    private fun trimLabels() {
        for (shape in diagram.labels) {
            if (shape.text.startsWith(' ') && shape.text.endsWith(' ')) {
                if (diagram.findBox(shape.row, shape.fromColumn) == null &&
                    diagram.findBox(shape.row, shape.rightColumn()) == null
                ) {
                    shape.text = shape.text.trim()
                    shape.fromColumn ++
                }
            }
        }
    }

    private fun findConnections() {
        hLines.removeIf { it.used }
        vLines.removeIf { it.used }

        var hLine = hLines.firstOrNull()
        while (hLine != null) {
            //println("Started parsing $hLine")
            hLine.used = true

            val linesBefore = mutableListOf<HOrVLine>()
            val linesAfter = mutableListOf<HOrVLine>()
            extendHLine(hLine.row, hLine.startColumn, linesBefore)
            extendHLine(hLine.row, hLine.endColumn, linesAfter)
            //println("Before $linesBefore")
            //println("After $linesAfter")

            val allParts = mutableListOf<HOrVLine>()
            allParts.addAll(linesBefore.asReversed())
            allParts.add(hLine)
            allParts.addAll(linesAfter)
            createConnection(allParts)

            hLines.removeIf { it.used }
            vLines.removeIf { it.used }
            hLine = hLines.firstOrNull()
        }

        hLines.removeIf { it.used }
        vLines.removeIf { it.used }

        var vLine = vLines.firstOrNull()
        while (vLine != null) {
            //println("Started parsing $vLine")
            vLine.used = true

            val linesBefore = mutableListOf<HOrVLine>()
            val linesAfter = mutableListOf<HOrVLine>()
            extendHLine(vLine.startRow, vLine.column, linesBefore)
            extendHLine(vLine.endRow, vLine.column, linesAfter)
            //println("Before $linesBefore")
            //println("After $linesAfter")

            val allParts = mutableListOf<HOrVLine>()
            allParts.addAll(linesBefore.asReversed())
            allParts.add(vLine)
            allParts.addAll(linesAfter)
            createConnection(allParts)

            hLines.removeIf { it.used }
            vLines.removeIf { it.used }
            vLine = vLines.firstOrNull()
        }
    }

    /**
     * Note [allParts] are alternating [HorizontalLine]s and [VerticalLine]s which can be
     * joined head to tail.
     * However, as we iterate through them, we need to work out whether to use the
     * startRow/Column or endRow/Column as the next [Waypoint].
     */
    private fun createConnection(allParts: List<HOrVLine>) {
        if (allParts.isEmpty()) return
        val points = mutableListOf<Pair<Int, Int>>()
        if (allParts.size == 1) {
            val part = allParts.first()
            if (part is HorizontalLine) {
                points.add(Pair(part.row, part.startColumn))
                points.add(Pair(part.row, part.endColumn))
            } else if (part is VerticalLine) {
                points.add(Pair(part.startRow, part.column))
                points.add(Pair(part.endRow, part.column))
            } else {
                return
            }
        } else {

            var prevRow: Int
            var prevColumn: Int
            val firstPart = allParts[0]
            val secondPart = allParts[1]

            // Add the initial waypoint.
            if (firstPart is HorizontalLine && secondPart is VerticalLine) {
                if (firstPart.endColumn == secondPart.column) {
                    points.add(Pair(firstPart.row, firstPart.startColumn))
                    prevRow = firstPart.row
                    prevColumn = firstPart.startColumn
                } else {
                    points.add(Pair(firstPart.row, firstPart.endColumn))
                    prevRow = firstPart.row
                    prevColumn = firstPart.endColumn
                }
            } else if (firstPart is VerticalLine && secondPart is HorizontalLine) {
                if (firstPart.endRow == secondPart.row) {
                    points.add(Pair(firstPart.startRow, firstPart.column))
                    prevRow = firstPart.startRow
                    prevColumn = firstPart.column
                } else {
                    points.add(Pair(firstPart.endRow, firstPart.column))
                    prevRow = firstPart.endRow
                    prevColumn = firstPart.column
                }
            } else {
                log.warn("Not alternating H/V lines")
                return
            }

            // Add the remaining waypoints.
            for (part in allParts) {
                if (part is HorizontalLine) {
                    if (part.row != prevRow) {
                        log.warn("Disjoint Row $prevRow , $prevColumn")
                        return
                    }
                    if (part.startColumn == prevColumn) {
                        points.add(Pair(part.row, part.endColumn))
                        prevColumn = part.endColumn
                    } else if (part.endColumn == prevColumn) {
                        points.add(Pair(part.row, part.startColumn))
                        prevColumn = part.startColumn
                    } else {
                        log.warn("Disjoint column @ $prevRow , $prevColumn")
                        return
                    }
                } else if (part is VerticalLine) {
                    if (part.column != prevColumn) {
                        log.warn("Disjoint Column @ $prevRow , $prevColumn")
                        return
                    }
                    if (part.startRow == prevRow) {
                        points.add(Pair(part.endRow, part.column))
                        prevRow = part.endRow
                    } else if (part.endRow == prevRow) {
                        points.add(Pair(part.startRow, part.column))
                        prevRow = part.startRow
                    } else {
                        log.warn("Disjoint row @ $prevRow , $prevColumn")
                    }
                }
            }
        }

        var isHorizontal = allParts.first() is VerticalLine
        val connection = Connection(diagram).apply {
            for (point in points) {
                waypoints.add(WaypointImpl(this, point.first, point.second, isHorizontal))
                isHorizontal = ! isHorizontal
            }
        }

        // Count the line thickness, and lineStyle.
        var count = 1
        var cornerCount = 0
        var sumThickness = 0
        var sumLineStyle = 0
        var roundedCount = 0
        // A Count of each type of LineMarker
        val midLineMarkers = mutableMapOf<LineMarker, Int>()

        var prevWaypoint = connection.waypoints.firstOrNull() ?: return
        for (waypoint in connection.waypoints) {
            if (waypoint != prevWaypoint) {
                val fromRow = min(prevWaypoint.row, waypoint.row)
                val toRow = max(prevWaypoint.row, waypoint.row)
                val fromColumn = min(prevWaypoint.column, waypoint.column)
                val toColumn = max(prevWaypoint.column, waypoint.column)
                // Prevent mid-line markings being found in the "middle" of a line, when in fact, it is
                // at the end of the line, but the line-ending is "1 away", so that it doesn't overlap a box edge).
                if (abs(toRow - fromRow) > 2 || abs(toColumn - fromColumn) > 2) {
                    val midRow = (fromRow + toRow) / 2
                    val midColumn = (fromColumn + toColumn) / 2
                    BlockMetaData.lineMarkersByChar[grid[midRow][midColumn]]?.let { lineMarker ->
                        val current = midLineMarkers[lineMarker] ?: 0
                        midLineMarkers[lineMarker] = current + 1
                    }
                }

                BlockMetaData.variationsByChar[grid[waypoint.row][waypoint.column]]?.let { cornerVariation ->
                    if (cornerVariation.basicBlock is CornerBlock) {
                        cornerCount ++
                        if (cornerVariation.rounded) roundedCount ++
                    }
                }


                if (fromRow == toRow) {
                    for (column in fromColumn + 1 until toColumn) {
                        BlockMetaData.variationsByChar[grid[fromRow][column]]?.let { variation ->
                            count ++
                            sumThickness += variation.thickness
                            sumLineStyle += variation.lineStyle
                        }
                    }
                } else {
                    for (row in fromRow + 1 until toRow) {
                        BlockMetaData.variationsByChar[grid[row][fromColumn]]?.let { variation ->
                            count ++
                            sumThickness += variation.thickness
                            sumLineStyle += variation.lineStyle
                        }
                    }
                }
            }
            prevWaypoint = waypoint
        }

        if (count > 0) {
            connection.thickness = (sumThickness.toFloat() / count).roundToInt()
            connection.lineStyle = (sumLineStyle.toFloat() / count).roundToInt()
            connection.rounded = roundedCount > cornerCount / 2
        }

        val firstWaypoint = connection.waypoints.first() as WaypointImpl
        val lastWaypoint = connection.waypoints.last() as WaypointImpl
        BlockMetaData.lineMarkersByChar[grid[firstWaypoint.row][firstWaypoint.column]]?.let { lineMarker ->
            connection.startMarker = lineMarker
            // Adjust row/column by 1 if the start lineMarker is 1 shy of a Box.
            val secondWaypoint = connection.waypoints[1]
            val firstPart = allParts.first()
            if (firstPart is HorizontalLine) {
                val betterColumn = firstWaypoint.column + if (secondWaypoint.column > firstWaypoint.column) - 1 else 1
                diagram.boxes.firstOrNull {
                    it.isTouchingEdge(firstWaypoint.row, betterColumn) &&
                        (it.columnA == betterColumn || it.columnB == betterColumn)
                }?.let {
                    firstWaypoint.column = betterColumn
                }
            } else {
                val betterRow = firstWaypoint.row + if (secondWaypoint.row > firstWaypoint.row) - 1 else 1
                diagram.boxes.firstOrNull {
                    it.isTouchingEdge(betterRow, firstWaypoint.column) &&
                        (it.rowA == betterRow || it.rowB == betterRow)
                }?.let {
                    firstWaypoint.row = betterRow
                }
            }
        }
        BlockMetaData.lineMarkersByChar[grid[lastWaypoint.row][lastWaypoint.column]]?.let { lineMarker ->
            connection.endMarker = lineMarker
            // Adjust row/column by 1 if the end lineMarker is 1 shy of a Box.
            val penultimateWaypoint = connection.waypoints[connection.waypoints.size - 2]
            val lastPart = allParts.last()
            if (lastPart is HorizontalLine) {
                val betterColumn = lastWaypoint.column + if (penultimateWaypoint.column < firstWaypoint.column) - 1 else 1
                diagram.boxes.firstOrNull {
                    it.isTouchingEdge(lastWaypoint.row, betterColumn) &&
                        (it.columnA == betterColumn || it.columnB == betterColumn)
                }?.let {
                    lastWaypoint.column = betterColumn
                }
            } else {
                val betterRow = lastWaypoint.row + if (penultimateWaypoint.row > lastWaypoint.row) - 1 else 1
                diagram.boxes.firstOrNull {
                    it.isTouchingEdge(betterRow, lastWaypoint.column) &&
                        (it.rowA == betterRow || it.rowB == betterRow)
                }?.let {
                    lastWaypoint.row = betterRow
                }
            }
        }
        if (midLineMarkers.isNotEmpty()) {
            connection.midMarker = midLineMarkers.maxBy { it.value }.key
        }

        diagram.connections.add(connection)
    }

    private fun extendHLine(row: Int, column: Int, linesAfter: MutableList<HOrVLine>) {
        val vLine = vLines.firstOrNull {
            ! it.used && it.column == column && (it.startRow == row || it.endRow == row)
        }
        if (vLine != null) {
            vLine.used = true
            linesAfter.add(vLine)

            if (vLine.startRow == row) {
                extendVLine(vLine.endRow, vLine.column, linesAfter)
            } else {
                extendVLine(vLine.startRow, vLine.column, linesAfter)
            }
        }
    }

    private fun extendVLine(row: Int, column: Int, linesAfter: MutableList<HOrVLine>) {
        val hLine = hLines.firstOrNull {
            ! it.used && it.row == row && (it.startColumn == column || it.endColumn == column)
        }
        if (hLine != null) {
            hLine.used = true
            linesAfter.add(hLine)

            if (hLine.startColumn == column) {
                extendHLine(hLine.row, hLine.endColumn, linesAfter)
            } else {
                extendHLine(hLine.row, hLine.startColumn, linesAfter)
            }
        }
    }

    /**
     * Fills the grid where [box] resides with the [WILDCARD] character.
     */
    private fun finishBox(box: Box) {

        var count = 0
        var thicknessTotal = 0
        var lineStyleTotal = 0
        for (row in listOf(box.topRow(), box.bottomRow())) {
            for (column in listOf(box.leftColumn(), box.rightColumn())) {
                BlockMetaData.variationsByChar[grid[row][column]]?.let { variation ->
                    if (variation.rounded) {
                        box.rounded = true
                    }
                }
                grid[row][column] = WILDCARD
            }
            // Count the thickness of the horizontals
            for (column in box.leftColumn() + 1 until box.rightColumn()) {
                BlockMetaData.variationsByChar[grid[row][column]]?.let { variation ->
                    count ++
                    thicknessTotal += variation.thickness
                    lineStyleTotal += variation.lineStyle
                }
            }
        }
        // Count the thickness of the verticals
        for (column in listOf(box.leftColumn(), box.rightColumn())) {
            for (row in box.topRow() + 1 until box.bottomRow()) {
                BlockMetaData.variationsByChar[grid[row][column]]?.let { variation ->
                    count ++
                    thicknessTotal += variation.thickness
                    lineStyleTotal += variation.lineStyle
                }
            }
        }
        box.thickness = (thicknessTotal.toFloat() / count).roundToInt()
        box.lineStyle = (lineStyleTotal.toFloat() / count).roundToInt()

        if (box.fill != null) {
            for (row in box.rowA..box.rowB) {
                for (column in box.columnA..box.columnB) {
                    grid[row][column] = WILDCARD
                }
            }
        }
    }

    private fun findHLines() {
        for (y in grid.indices) {
            var startX = 0
            val line = grid[y]
            while (startX < width) {
                val startChar = line[startX]

                val startVariation = BlockMetaData.variationsByChar[startChar]
                val startBlock = startVariation?.basicBlock
                if (startBlock is HorizontalPart) {
                    var midX = startX + 1
                    // The latest column where a HorizontalPart or lineMarker was found.
                    var lastLineX = startX
                    var endX = width - 1
                    while (midX < width) {
                        val endChar = line[midX]
                        val endBlock = BlockMetaData.variationsByChar[endChar]?.basicBlock
                        val isLineMarker = BlockMetaData.lineMarkerCharacters.contains(endChar)
                        if (endBlock is CornerBlock) {
                            // When we hit a corner, this MUST be the end of the line
                            endX = if (endBlock.isLeft) midX - 1 else midX
                            break
                        }
                        // Have we got to a character which CANNOT be part of a line?
                        if (endChar != WILDCARD && ! isLineMarker && endBlock !is HorizontalPart) {
                            endX = lastLineX
                            break
                        }
                        if (endChar != WILDCARD) {
                            lastLineX = midX
                        }
                        midX ++
                    }
                    if (startX != endX) {
                        hLines.add(HorizontalLine(y, startX, endX))
                    }
                    startX = endX
                }

                startX ++
            }
        }
    }

    private fun findVLines() {
        for (x in 0 until width) {
            var startY = 0
            while (startY < grid.size) {
                val startChar = grid[startY][x]

                val startVariation = BlockMetaData.variationsByChar[startChar]
                val startBlock = startVariation?.basicBlock
                if (startBlock is VerticalPart) {
                    var midY = startY + 1
                    // The latest row where a VerticalPart or lineMarker was found
                    var lastLineY = startY
                    var endY = grid.size - 1
                    while (midY < grid.size) {
                        val endChar = grid[midY][x]
                        val endBlock = BlockMetaData.variationsByChar[endChar]?.basicBlock
                        val isLineMarker = BlockMetaData.lineMarkerCharacters.contains(endChar)
                        if (endBlock is CornerBlock) {
                            // When we hit a corner, this MUST be the end of the line
                            endY = if (endBlock.isTop) midY - 1 else midY
                            break
                        }
                        // Have we got to a character which CANNOT be part of a line?
                        if (endChar != WILDCARD && ! isLineMarker && endBlock !is VerticalPart) {
                            endY = lastLineY
                            break
                        }
                        if (endChar != WILDCARD) {
                            lastLineY = midY
                        }
                        midY ++
                    }
                    if (startY != endY) {
                        vLines.add(VerticalLine(startY, endY, x))
                    }
                    startY = endY
                }

                startY ++
            }
        }
    }

    private fun findWholeBoxes() {

        for (top in hLines) {
            val topStartBlock = top.startVariation?.basicBlock ?: continue
            if (top.used || topStartBlock !is CornerBlock || ! topStartBlock.isLeft || ! topStartBlock.isTop) continue

            val left =
                vLines.firstOrNull { ! it.used && it.column == top.startColumn && it.startRow == top.row }
                    ?: continue
            val leftEndBlock = left.endVariation?.basicBlock ?: continue
            if (leftEndBlock !is CornerBlock || ! leftEndBlock.isLeft || leftEndBlock.isTop) continue

            val right =
                vLines.firstOrNull { ! it.used && it.column == top.endColumn && it.startRow == top.row && it.endRow == left.endRow }
                    ?: continue
            val rightEndBlock = right.endVariation?.basicBlock ?: continue
            if (rightEndBlock !is CornerBlock || rightEndBlock.isLeft || rightEndBlock.isTop) continue

            val bottom =
                hLines.firstOrNull { ! it.used && it.row == left.endRow && it.startColumn == left.column && it.endColumn == right.column }
                    ?: continue

            val box = Box(diagram, top.row, top.startColumn, bottom.row, bottom.endColumn)
            boxes.add(box)
            top.used = true
            left.used = true
            right.used = true
            bottom.used = true
        }
    }

    /**
     * Look for boxes that are partially obscured.
     * There must be a complete horizontal and vertical edge
     *
     * @return true if a box was found.
     */
    private fun findPartialBoxes(): Boolean {

        var found = false

        fun usingTopLeft(top: HorizontalLine): Boolean {

            // We have a complete top line
            val left: VerticalLine? =
                vLines.firstOrNull { ! it.used && it.column == top.startColumn && it.startRow == top.row }
            val leftEndBlock = left?.endVariation?.basicBlock
            if (left != null && leftEndBlock is CornerBlock && leftEndBlock.isLeft) {
                // We have complete top and left edges.
                // Imagine the other two sides, and see if they could possibly exist.
                val right = VerticalLine(left.startRow, left.endRow, top.endColumn)
                if (! right.exists()) return false
                val bottom = HorizontalLine(left.endRow, left.column, top.endColumn)
                if (! bottom.exists()) return false

                val box = Box(diagram, top.row, left.column, bottom.row, right.column)
                boxes.add(box)
                found = true
                top.used = true
                left.used = true
                return true
            }
            return false
        }

        fun usingTopRight(top: HorizontalLine): Boolean {
            val right =
                vLines.firstOrNull { ! it.used && it.column == top.endColumn && it.startRow == top.row }
            val rightEndBlock = right?.endVariation?.basicBlock
            if (right != null && rightEndBlock is CornerBlock && ! rightEndBlock.isLeft) {
                // We have complete top and right edges.
                // Imagine the other two sides, and see if they could possibly exist.
                val left = VerticalLine(right.startRow, right.endRow, top.startColumn)
                if (! left.exists()) return false
                val bottom = HorizontalLine(left.endRow, left.column, top.endColumn)
                if (! bottom.exists()) return false

                val box = Box(diagram, top.row, left.column, bottom.row, right.column)
                boxes.add(box)
                found = true
                top.used = true
                right.used = true
                return true
            }
            return false
        }

        fun usingBottomLeft(bottom: HorizontalLine): Boolean {

            // We have a complete bottom line
            val left =
                vLines.firstOrNull { ! it.used && it.column == bottom.startColumn && it.endRow == bottom.row }
            val leftStartBlock = left?.endVariation?.basicBlock
            if (left != null && leftStartBlock is CornerBlock && leftStartBlock.isLeft) {
                // We have complete bottom and left edges.
                // Imagine the other two sides, and see if they could possibly exist.
                val right = VerticalLine(left.startRow, left.endRow, bottom.endColumn)
                if (! right.exists()) return false
                val top = HorizontalLine(left.startRow, left.column, bottom.endColumn)
                if (! top.exists()) return false

                val box = Box(diagram, top.row, left.column, bottom.row, right.column)
                boxes.add(box)
                found = true
                bottom.used = true
                left.used = true
                return true
            }
            return false
        }

        fun usingBottomRight(bottom: HorizontalLine): Boolean {
            val right =
                vLines.firstOrNull { ! it.used && it.column == bottom.endColumn && it.endRow == bottom.row }
            val rightStartBlock = right?.startVariation?.basicBlock
            if (right != null && rightStartBlock is CornerBlock && ! rightStartBlock.isLeft) {
                // We have complete top and right edges.
                // Imagine the other two sides, and see if they could possibly exist.
                val left = VerticalLine(right.startRow, right.endRow, bottom.startColumn)
                if (! left.exists()) return false
                val top = HorizontalLine(left.startRow, left.column, bottom.endColumn)
                if (! top.exists()) return false

                val box = Box(diagram, top.row, left.column, bottom.row, right.column)
                boxes.add(box)
                found = true
                bottom.used = true
                right.used = true
                return true
            }
            return false
        }

        for (horizontal in hLines) {
            if (horizontal.used) continue
            val horizontalStartBlock = horizontal.startVariation?.basicBlock ?: continue
            val horizontalEndBlock = horizontal.endVariation?.basicBlock ?: continue

            if (horizontalStartBlock is CornerBlock && horizontalEndBlock is CornerBlock) {
                if (! horizontalStartBlock.isLeft) continue
                if (horizontalEndBlock.isLeft) continue
                // We have a complete top or bottom line.

                if (horizontalStartBlock.isTop) {

                    if (! horizontalEndBlock.isTop) continue
                    if (! usingTopLeft(horizontal)) {
                        usingTopRight(horizontal)
                    }

                } else {
                    // Ok, is horizontal a bottom edge?

                    if (horizontalEndBlock.isTop) continue
                    if (! usingBottomLeft(horizontal)) {
                        usingBottomRight(horizontal)
                    }
                }
            }

        }
        return found
    }

    /**
     * Extract all plain-text as [DiagramLabel]s.
     *
     * Look for non-special characters, and allow a single space character too.
     * Replace the characters with [WILDCARD]
     *
     * Tag the label as 'maybe' having padding.
     * Post-processing will check if the text is over a H line,
     * then respect `maybe` padding, otherwise remove the 'maybe'.
     */
    private fun findLabels() {
        for (y in grid.indices) {
            var startX = 0
            val line = grid[y]
            while (startX < width) {
                val startChar = line[startX]
                if (! startChar.isWhitespace() && ! BlockMetaData.variationCharacters.contains(startChar)) {
                    // Found the start of a label/text
                    var spaceCount = 0
                    for (endX in startX + 1 until line.size) {
                        val midChar = line[endX]
                        var ended = false
                        if (midChar.isWhitespace()) {
                            if (spaceCount == 1) {
                                ended = true
                            } else {
                                spaceCount ++
                            }
                        } else if (BlockMetaData.variationCharacters.contains(midChar)) {
                            ended = true
                        } else {
                            spaceCount = 0
                        }
                        if (ended) {
                            val text = String(grid[y].slice(startX until endX).toCharArray())
                            // If there are spaces surrounding the label, then include them.
                            // Note, this is vital if the label is within a horizontal line like so :
                            // ┌── Title ──┐
                            // └───────────┘
                            // If the spaces aren't included in the label, then findWholeBoxes() won't find the box.
                            val label = if (text.last() == ' ' && startX > 0 && line[startX - 1] == ' ') {
                                DiagramLabel(diagram, y, startX - 1, " $text")
                            } else {
                                // Otherwise, remove the possible space at the end, for symmetry.
                                DiagramLabel(diagram, y, startX, text.trim())
                            }
                            diagram.labels.add(0, label)

                            // Add the [WILDCARD] special character where the label was.
                            // findHLines() and findVLines() will not stop at the [WILDCARD]
                            // character. e.g. Both of the following Strings contain ONE HorizontalLine,
                            // which are the same size, despite the first String _seemingly_ having two short lines.
                            // ┌── Title ──┐
                            // └───────────┘
                            // If '*' is the wildcard, we can see this IS one horizontal line : ┌,─,─,*,*,*,*,*,*,*,─,─,┐
                            for (tx in label.fromColumn .. label.rightColumn()) {
                                line[tx] = WILDCARD
                            }
                            startX = endX + 1
                            break
                        }
                    }
                }
                startX ++
            }
        }
    }

    interface HOrVLine

    private inner class HorizontalLine(val row: Int, val startColumn: Int, val endColumn: Int) : HOrVLine {
        var used = false
        val startVariation = BlockMetaData.variationsByChar[grid[row][startColumn]]
        val endVariation = BlockMetaData.variationsByChar[grid[row][endColumn]]

        fun exists(): Boolean {
            for (column in startColumn..endColumn) {
                val char = grid[row][column]
                val block = BlockMetaData.variationsByChar[char]?.basicBlock
                if (char != WILDCARD && block !is HorizontalPart) return false
            }
            return true
        }

        override fun toString() = "HorizontalLine $row , $startColumn .. $endColumn"
    }

    private inner class VerticalLine(val startRow: Int, val endRow: Int, val column: Int) : HOrVLine {
        var used = false
        val startVariation = BlockMetaData.variationsByChar[grid[startRow][column]]
        val endVariation = BlockMetaData.variationsByChar[grid[endRow][column]]

        fun exists(): Boolean {
            for (row in startRow..endRow) {
                val char = grid[row][column]
                val block = BlockMetaData.variationsByChar[char]?.basicBlock
                if (char != WILDCARD && block !is VerticalPart) return false
            }
            return true
        }

        override fun toString() = "VerticalLine $startRow .. $endRow , $column"
    }

    companion object {
        const val WILDCARD = '⁂'
    }

}

/**
 * Looks for box background characters, grouping them into groups of like-characters
 * which are touching each other.
 *
 * The result is a list of [Group], which contains the bounds of the group, and the
 * background character of the group.
 * It is up to [Parser] to determine if each [Group] should be converted to a [Box].
 * (The group may be comprised of few background characters, not an entire rectangular block.)
 */
private class BackgroundScanner(val grid: List<CharArray>) {

    private val backgroundMap = mutableMapOf<Pair<Int, Int>, Group>()

    fun scan(): Set<Group> {
        val backgrounds = BlockMetaData.background.variations.map { it.c }
        for ((row, line) in grid.withIndex()) {
            var prevGroup: Group? = null
            for ((column, c) in line.withIndex()) {
                if (backgrounds.contains(c)) {
                    if (prevGroup != null && prevGroup.c == c) {
                        prevGroup.toColumn = max(prevGroup.toColumn, column)
                        val groupAbove = backgroundMap[Pair(row - 1, column)]?.rootGroup()
                        if (groupAbove != null) {
                            prevGroup.mergeInto(groupAbove)
                            prevGroup = groupAbove
                        }
                        backgroundMap[Pair(row, column)] = prevGroup

                    } else {
                        val newGroup = Group(row, column, c)
                        val groupAbove = backgroundMap[Pair(row - 1, column)]?.rootGroup()
                        if (groupAbove != null) {
                            newGroup.mergeInto(groupAbove)
                            prevGroup = groupAbove
                        } else {
                            prevGroup = newGroup
                        }
                        backgroundMap[Pair(row, column)] = prevGroup
                    }
                } else {
                    prevGroup = null
                }
            }
        }

        return backgroundMap.values.map { it.rootGroup() }.toSet()
    }


    class Group(row: Int, column: Int, val c: Char) {

        private var parentGroup: Group? = null

        var fromRow = row
        var toRow = row

        var fromColumn = column
        var toColumn = column

        fun isRoot() = parentGroup == null
        fun rootGroup(): Group = parentGroup?.rootGroup() ?: this

        fun mergeInto(newParent: Group) {
            // Already merged?
            if (newParent.rootGroup() === rootGroup()) return

            val otherRoot = newParent.rootGroup()
            val myRoot = rootGroup()
            parentGroup = newParent

            otherRoot.fromRow = min(myRoot.fromRow, otherRoot.fromRow)
            otherRoot.toRow = max(myRoot.toRow, otherRoot.toRow)
            otherRoot.fromColumn = min(myRoot.fromColumn, otherRoot.fromColumn)
            otherRoot.toColumn = max(myRoot.toColumn, otherRoot.toColumn)
        }

        override fun toString() = "Group $fromRow,$toColumn .. $toRow,$toColumn  : $c ${isRoot()}"
    }
}
