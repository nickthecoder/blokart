package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.DiagramImpl
import uk.co.nickthecoder.blokart.model.DocumentImpl
import uk.co.nickthecoder.glok.history.Change
import uk.co.nickthecoder.glok.history.ChangeImpl
import uk.co.nickthecoder.glok.text.DeleteText
import uk.co.nickthecoder.glok.text.InsertText
import uk.co.nickthecoder.glok.text.TextChange


/**
 *
 */
interface PlainTextChange : BlokartChange {
    val plainTextChange: TextChange
}

/**
 * Changes to the plain-text.
 * This is a wrapper around [TextChange] instances.
 *
 * [ignoreFirstRedo] is `true` when this change has already been applied to the plain-text document.
 */
class PlainTextChangeImpl(

    override val document: DocumentImpl,
    override val plainTextChange: TextChange,
    val ignoreFirstRedo: Boolean = false

) : PlainTextChange, ChangeImpl {

    private var isFirst = true

    override val label: String
        get() = plainTextChange.label

    override fun redo() {
        if (! (isFirst && ignoreFirstRedo)) {
            document.changingPlainText {
                (plainTextChange as ChangeImpl).redo()
                document.textDocument.fireChange(plainTextChange, false)
            }
        }
        isFirst = false
        updateDiagrams(plainTextChange, false)
    }

    override fun undo() {
        updateDiagrams(plainTextChange, true)
        document.changingPlainText {
            (plainTextChange as ChangeImpl).undo()
            document.textDocument.fireChange(plainTextChange, true)
        }
    }

    private fun updateDiagrams(change: TextChange, isUndo: Boolean) {
        val (lines, from) = when (change) {
            is InsertText -> Pair(change.textList.size - 1, change.from)
            is DeleteText -> Pair(change.from.row - change.to.row, change.from)
            else -> return
        }
        val growShrink = if (isUndo) - lines else lines
        if (growShrink != 0) {
            for (diagram in document.diagrams) {
                diagram as DiagramImpl
                if (diagram.fromRow >= from.row) {
                    diagram.fromRow += growShrink
                    diagram.toRow += growShrink
                    //diagram.removeHighlight(textDocument)
                    diagram.invalidate()
                }
            }
        }
    }

    override fun canMergeWith(previous: Change): Boolean {
        return (previous is PlainTextChangeImpl) && ignoreFirstRedo == previous.ignoreFirstRedo &&
            (plainTextChange as ChangeImpl).canMergeWith(previous.plainTextChange)
    }

    override fun mergeWith(previous: Change) {
        (plainTextChange as ChangeImpl).mergeWith((previous as PlainTextChangeImpl).plainTextChange)
    }

}
