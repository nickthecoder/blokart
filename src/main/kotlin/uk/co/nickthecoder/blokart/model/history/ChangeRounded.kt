package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.BoxOrConnection
import uk.co.nickthecoder.blokart.model.Diagram
import uk.co.nickthecoder.blokart.model.Document
import uk.co.nickthecoder.glok.history.ChangeImpl


interface ChangeRounded : DiagramChange {
    val shape: BoxOrConnection
    val newRounded: Boolean
}

class ChangeRoundedImpl(
    override val shape: BoxOrConnection,
    override val newRounded: Boolean
) : ChangeRounded, ChangeImpl {

    override val label get() = "Rounded"
    override val diagram: Diagram get() = shape.diagram
    override val document: Document get() = shape.diagram.document

    private val oldRounded = shape.rounded

    override fun redo() {
        shape.rounded = newRounded
    }

    override fun undo() {
        shape.rounded = oldRounded
    }

    // TODO Merge?
}

