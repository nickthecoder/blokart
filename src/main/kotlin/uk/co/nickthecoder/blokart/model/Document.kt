package uk.co.nickthecoder.blokart.model

import uk.co.nickthecoder.blokart.contains
import uk.co.nickthecoder.blokart.isBlankOrEmptyComment
import uk.co.nickthecoder.blokart.model.history.*
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.history.Change
import uk.co.nickthecoder.glok.history.HistoryDocument
import uk.co.nickthecoder.glok.history.HistoryDocumentBase
import uk.co.nickthecoder.glok.text.StyledTextDocument
import uk.co.nickthecoder.glok.text.TextChange
import uk.co.nickthecoder.glok.util.Log
import uk.co.nickthecoder.glok.util.log

/**
The `model` of a Blokart file.
It is composed of [textDocument] (a [StyledTextDocument]) and a set of [diagrams].

[textDocument] also contains the text representation of the [diagrams].
So to save the whole document, just save [textDocument]'s text.

The concrete implementation is [DocumentImpl].
Only [Change] subclasses should access it.

 *       ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮      ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮        ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *       ┆    Document     ┆      ┆  Diagram     ┆        ┆     Shape      ┆           ╭╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮
 *       ┆                 ┆      ┆              ┆        ┆                ┆           ┆  ControlPoint  ┆
 *       ┆ diagrams        ┆◆─────┤ document     ┆     ┌──┤ diagram        ┆           ┆                ┆
 *       ┆ textDocument    ┆◆──┐  ┆ fromRow      ┆     │  ┆                ┆       ┌───┤  shape         ┆
 *       ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯   │  ┆ toRow        ┆     │  ┆ topRow()       ┆       │   ┆  row           ┆
 *               ◇             │  ┆ allShapes    ┆◆────┘  ┆ bottomRow()    ┆       │   ┆  column        ┆
 *               │             │  ┆  boxes       ┆        ┆ leftColumn()   ┆       │   ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯
 *       ┏━━━━━━━┷━━━━━━━━┓    │  ┆  connections ┆        ┆ rightColumn()  ┆       │   Each Shape implementation
 *       ┃  DocumentImpl  ┃    │  ┆  labels      ┆        ┆ isTouching()   ┆       │   has its own ControlPoint
 *       ┃                ┃    │  ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯        ┆ controlPoints()┆◆──────┘   implementation.
 *       ┃                ┃    │          ◇               ┆ render()       ┆
 *       ┗━━━━━━━━━━━━━━━━┛    │          │               ┆ ...            ├────────────────────────────┐
 *                             │  ┏━━━━━━━┷━━━━━━┓        ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯                            │
 *                             │  ┃ DiagramImpl  ┃                 ◇                                    │
 *                             │  ┃              ┃                 │                                    │
 *                             │  ┃              ┃                 │                                    │
 *                             │  ┗━━━━━━━━━━━━━━┛                 │                                    │
 *                             │                                   │                                    │
 *                             │                                   │                                    │
 *    ┏━━━━━━━━━━━━━┓  ┏━━━━━━━┷━━━━━━┓           ┌────────────────┴─────┬────────────────────┐         │
 *    ┃   Styled    ┃  ┃ TextDocument ┃           │                      │                    │         │
 *    ┃  TextArea   ┠──┨              ┃ ╭╌╌╌╌╌╌╌╌╌┴╌╌╌╌╌╌╌╌╌╌╮   ┏━━━━━━━┷━━━━━━━━┓   ┏━━━━━━━┷━━━━━━┓  │
 *    ┗━━━━━━━━━━━━━┛  ┗━━━━━━━━━━━━━━┛ ┆   BoxOrConnection  ┆   ┃  DiagramLabel  ┃   ┃  ShapeGroup  ┃  │
 *             (Part of Glok)           ┆                    ┆   ┃                ┃   ┃              ┃  │
 *                                      ┆  rounded: Boolean  ┆   ┃ text           ┃   ┃  shapes      ┃◆─┘
 *                                      ┆  thickness: Int    ┆   ┃ row            ┃   ┃              ┃
 *                                      ┆  lineStyle: Int    ┆   ┃ column         ┃   ┃              ┃
 *                                      ╰╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╯   ┗━━━━━━━━━━━━━━━━┛   ┗━━━━━━━━━━━━━━┛
 *                                                ◇                                   Not used as part of a Diagram.
 *                                                │                                   Used by DiagramView when
 *                                      ┌─────────┴────────┐                          multiple shapes are selected.
 *                                      │                  │
 *                                ┏━━━━━┷━━━━━━┓   ┏━━━━━━━┷━━━━━━━━┓
 *                                ┃    Box     ┃   ┃   Connection   ┃
 *                                ┃            ┃   ┃                ┃       ┏━━━━━━━━━━━━━━━━┓
 *                                ┃ rowA       ┃   ┃ waypoints      ┃       ┃   LineMarker   ┃
 *                                ┃ rowB       ┃   ┃ startMarker    ┃       ┃                ┃
 *                                ┃ columnA    ┃   ┃ midMarker      ┠──────▶┃ north: Char    ┃
 *                                ┃ columnB    ┃   ┃ endMarker      ┃       ┃ south: Char    ┃
 *                                ┗━━━━━━━━━━━━┛   ┃                ┃       ┃ east: Char     ┃
 *                                                 ┃ reverse()      ┃       ┃ west: Char     ┃
 *                                                 ┃ ...            ┃       ┗━━━━━━━━━━━━━━━━┛
 *                                                 ┗━━━━━━━━━━━━━━━━┛
 *
Created with [Blokart](https://gitlab.com/nickthecoder/blokart)

NOTE. Ideally, each Shape implementation, such as [Box] should have an interface (`Box`), which is immutable,
and an implementation (`BoxImpl`), which is only used by [Change] objects.
As it stands, it is too easy to accidentally modify the model outside of the `History` mechanism.
 */
interface Document : HistoryDocument {

    /**
     * The plain-text parts of the document.
     */
    val textDocument: StyledTextDocument

    /**
     * The diagram parts of the document
     */
    val diagrams: ObservableList<Diagram>

    fun setText(text: String, autoParse: Boolean = true)

    fun diagramAtRow(row: Int): Diagram? = diagrams.firstOrNull { diagram ->
        row >= diagram.fromRow && row <= diagram.toRow
    }

    fun insertPlainText(position: TextPosition, lines: Int = 3)
    fun insertDiagram(fromRow: Int, lines: Int = 5): Diagram
    fun plainTextToDiagram(fromRow: Int, toRow: Int): Diagram
    fun diagramToPlainText(diagram: Diagram)

    fun changingPlainText(block: () -> Unit)

}

/**
 * The concrete implementation of [Document].
 * This should only ever be used by [Change] classes.
 */
class DocumentImpl : Document, HistoryDocumentBase() {

    override val textDocument = StyledTextDocument("")

    override val diagrams = mutableListOf<Diagram>().asMutableObservableList()

    private var ignorePlainTextChanges: Boolean = false

    // region ==== init ====
    init {
        addListener { _, change, _ ->
            when (change) {
                is PlainTextToDiagram -> change.diagram.invalidate()
                is DiagramToPlainText -> change.diagram.invalidate()
                is DiagramChange -> change.diagram.invalidate()
            }
        }

        textDocument.addListener { _, change, isUndo ->
            if (! ignorePlainTextChanges && change is TextChange) {
                if (isUndo) {
                    log.warn("Undo of TextChanges should not be possible. Undo/Redo should be applied to the Document, not the TextDocument")
                } else {
                    PlainTextChangeImpl(this, change, true).now()
                }
            }
        }
    }
    // endregion init

    // region ==== Public Methods ====

    override fun insertPlainText(position: TextPosition, lines: Int) {
        textDocument.insert(position, List(lines + 1) { "" })
    }

    override fun insertDiagram(fromRow: Int, lines: Int): Diagram {
        InsertDiagramImpl(this, fromRow, lines).now()
        return diagrams.last()
    }

    override fun plainTextToDiagram(fromRow: Int, toRow: Int): Diagram {
        lateinit var diagram: Diagram

        history.batch {
            addChange(PlainTextToDiagramImpl(this@DocumentImpl, fromRow, toRow))
            diagram = diagrams.last()
            val before = diagrams.firstOrNull { it.toRow == diagram.fromRow - 1 }
            val after = diagrams.firstOrNull { it.fromRow == diagram.fromRow + 1 }
            if (before != null || after != null) {
                addChange(MergeDiagramsImpl(diagram, before, after))
            }
        }
        return diagram
    }

    override fun diagramToPlainText(diagram: Diagram) {
        DiagramToPlainTextImpl(this, diagram).now()
    }

    /**
     * Used when a [DiagramChange] needs to update the [textDocument],
     * or when the [textDocument] is updated to reflect the new state of the [diagrams],
     * we don't want the those changes to make alterations to the text document (again).
     */
    override fun changingPlainText(block: () -> Unit) {
        ignorePlainTextChanges = true
        block()
        ignorePlainTextChanges = false
        Platform.runLater { textDocument.history.clear() }
    }

    override fun setText(text: String, autoParse: Boolean) {

        history.batch("Set Text") {
            + plainTextChange(textDocument.clearChange())
            + plainTextChange(textDocument.insertChange(TextPosition(0, 0), text, false))

            var diagramStartRow = - 1
            var blankLineCount = 0

            if (autoParse) {
                for ((row, line) in textDocument.lines.toList().withIndex()) {
                    val isInDiagram = diagramStartRow >= 0

                    if (line.isBlankOrEmptyComment()) {
                        blankLineCount ++
                        continue
                    }

                    val lineIsDiagram = line.contains(BlockMetaData.variationCharacters)
                    if (lineIsDiagram != isInDiagram) {
                        if (lineIsDiagram) {
                            diagramStartRow = row - blankLineCount
                        } else {
                            + PlainTextToDiagramImpl(this@DocumentImpl, diagramStartRow, row - 1)
                            diagramStartRow = - 1
                        }
                    }
                    blankLineCount = 0
                }
                // Does the document END with a diagram (the for loop wouldn't have added it)
                if (diagramStartRow >= 0) {
                    + PlainTextToDiagramImpl(this@DocumentImpl, diagramStartRow, textDocument.lines.size - 1)
                }
            }
        }
    }

    // endregion public methods

    // region ==== Private methods ====

    private fun plainTextChange(plainTextChange: TextChange?) = if (plainTextChange == null) {
        null
    } else {
        PlainTextChangeImpl(this, plainTextChange)
    }
    // endregion


}
