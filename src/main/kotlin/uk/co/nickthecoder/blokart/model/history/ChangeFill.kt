package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.Box
import uk.co.nickthecoder.blokart.model.Diagram
import uk.co.nickthecoder.blokart.model.Document
import uk.co.nickthecoder.glok.history.ChangeImpl

interface ChangeFill : DiagramChange {
    val shape: Box
    val newFill: Char?
}

class ChangeFillImpl(
    override val shape: Box,
    override val newFill: Char?
) : ChangeFill, ChangeImpl {

    override val label get() = "Line Style"
    override val diagram: Diagram get() = shape.diagram
    override val document: Document get() = shape.diagram.document

    private val oldFill = shape.fill

    override fun redo() {
        shape.fill = newFill
    }

    override fun undo() {
        shape.fill = oldFill
    }

}

