package uk.co.nickthecoder.blokart.model

import uk.co.nickthecoder.blokart.BlokartSettings
import uk.co.nickthecoder.blokart.model.history.DeleteShapeImpl
import uk.co.nickthecoder.blokart.model.history.Parser
import uk.co.nickthecoder.glok.collections.ObservableList
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.text.Highlight
import uk.co.nickthecoder.glok.text.HighlightRange
import uk.co.nickthecoder.glok.text.StyledTextDocument
import uk.co.nickthecoder.glok.text.ThemedHighlight
import kotlin.math.max

/**
 * A part of a [Document] containing a diagram, and no plain-text.
 */
interface Diagram {

    val document: Document

    val fromRow: Int
    val toRow: Int

    val prefix: String

    val height: Int get() = toRow - fromRow + 1

    val boxes: ObservableList<Box>
    val labels: ObservableList<DiagramLabel>
    val connections: ObservableList<Connection>
    val allShapes: List<List<Shape>>

    fun invalidate()
    fun ensureUpdated(textDocument: StyledTextDocument): Boolean

    fun findBox(row: Int, column: Int): Box?

    /**
     * This uses [Box.isTouchingEdge], whereas [findBox] uses [Box.isTouching].
     * @return a Box whose edge is at [row], [column]. Or `null` if there isn't such a Box.
     */
    fun findBoxEdge(row: Int, column: Int): Box?

    fun deleteShape(shape: Shape)
}

internal class DiagramImpl(

    override val document: Document,
    override var fromRow: Int,
    override var toRow: Int

) : Diagram {

    private var dirty: Boolean = true

    private var diagramHighlight: Highlight = ThemedHighlight(".blokart_diagram")
    private var highlightRange: HighlightRange? = null

    override var prefix = ""

    override val boxes = mutableListOf<Box>().asMutableObservableList()
    override val labels = mutableListOf<DiagramLabel>().asMutableObservableList()
    override val connections = mutableListOf<Connection>().asMutableObservableList()
    override val allShapes = listOf(boxes, connections, labels)

    override fun deleteShape(shape: Shape) {
        if (shape.diagram != this) throw IllegalStateException("Shape not in the diagram")
        DeleteShapeImpl(shape).now()
    }

    override fun findBoxEdge(row: Int, column: Int): Box? {
        return boxes.firstOrNull { it.isTouchingEdge(row, column) }
    }

    override fun findBox(row: Int, column: Int): Box? {
        return boxes.firstOrNull { it.isTouching(row, column) }
    }

    override fun invalidate() {
        dirty = true
    }

    internal fun removeHighlight(textDocument: StyledTextDocument) {
        highlightRange?.let { textDocument.ranges.remove(it) }
        highlightRange = null
    }

    /**
     * @return true if the diagram was re-rendered.
     */
    override fun ensureUpdated(textDocument: StyledTextDocument): Boolean {
        if (dirty) {
            removeHighlight(textDocument)
            //println("Drawing $this height = $height")
            //println("Shapes =\n    ${shapes.joinToString(separator = "\n    ")}")

            val width: Int = 1 + max(
                boxes.maxOfOrNull { it.rightColumn() } ?: 0,
                max(
                    connections.maxOfOrNull { it.rightColumn() } ?: 0,
                    labels.maxOfOrNull { it.rightColumn() } ?: 0
                )
            )

            val grid = List(height) {
                CharArray(max(80, width + 1)) { ' ' }
            }

            // Draw the shapes to the [grid].
            // The Z-order of the different Shape types is hard-coded,
            // with DiagramLabels appearing above Connections, appearing above Boxes.
            for (shape in boxes) {
                shape.render(grid)
            }
            for (shape in connections) {
                shape.render(grid)
            }
            for (shape in labels) {
                shape.render(grid)
            }
            if (prefix.isNotBlank()) {
                for (row in 0 until height) {
                    for ((column, c) in prefix.withIndex()) {
                        grid[row][column] = c
                    }
                }
            }

            // Convert the [grid] to plain-text in the textArea.
            val replacement = grid.map { String(it) }
            val from = TextPosition(fromRow, 0)
            val to = TextPosition(toRow, textDocument.lines[toRow].length)

            document.changingPlainText {
                textDocument.replace(from, to, replacement)
                highlightRange = HighlightRange(from, TextPosition(toRow, 0), diagramHighlight, this)
                textDocument.ranges.add(highlightRange !!)
            }

            dirty = false
            return true
        }
        return false
    }

    override fun toString() = "Diagram $fromRow .. $toRow (${boxes.size + labels.size + connections.size} shapes)"
}
