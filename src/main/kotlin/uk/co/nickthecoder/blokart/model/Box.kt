package uk.co.nickthecoder.blokart.model

import uk.co.nickthecoder.blokart.model.history.TouchingWaypoint
import uk.co.nickthecoder.glok.property.boilerplate.IntBinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.StringBinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalCharProperty
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

class Box(
    override val diagram: Diagram, fromRow: Int, fromColumn: Int, toRow: Int, toColumn: Int
) : BoxOrConnection {

    /**
     * Either the top or bottom of the Box. [rowB] is the opposite side.
     */
    val rowAProperty by intProperty(fromRow)
    var rowA by rowAProperty

    /**
     * Either the left or right of the Box. [columnB] is the opposite side.
     */
    val columnAProperty by intProperty(fromColumn)
    var columnA by columnAProperty

    /**
     * Either the top or bottom of the Box. [rowA] is the opposite side.
     */
    val rowBProperty by intProperty(toRow)
    var rowB by rowBProperty

    /**
     * Either the left or right of the Box. [columnA] is the opposite side.
     */
    val columnBProperty by intProperty(toColumn)
    var columnB by columnBProperty

    val widthProperty = IntBinaryFunction(columnAProperty, columnBProperty) { from, to ->
        abs(from - to) + 1
    }
    val heightProperty = IntBinaryFunction(rowAProperty, rowBProperty) { from, to ->
        abs(from - to) + 1
    }

    override var rounded: Boolean = false
    override var thickness = 0
    override var lineStyle = 0

    val fillProperty by optionalCharProperty(null)
    var fill by fillProperty

    override val observableDescription = StringBinaryFunction(widthProperty, heightProperty) { width, height ->
        "Box @ ${leftColumn()},${topRow()} size $width x $height"
    }.apply {
        addDependent(rowAProperty)
        addDependent(columnAProperty)
    }
    override val description by observableDescription

    override fun copyTo(diagram: Diagram): Box {
        val new = Box(diagram, rowA, columnA, rowB, columnB)
        new.rounded = rounded
        new.thickness = thickness
        new.lineStyle = lineStyle
        new.fill = fill
        return new
    }

    override fun topRow() = min(rowA, rowB)
    override fun bottomRow() = max(rowA, rowB)
    override fun leftColumn() = min(columnA, columnB)
    override fun rightColumn() = max(columnA, columnB)

    private val controlPoints = listOf(
        CornerControlPoint(this, isRowA = true, isColumnA = true),
        CornerControlPoint(this, isRowA = true, isColumnA = false),
        CornerControlPoint(this, isRowA = false, isColumnA = true),
        CornerControlPoint(this, isRowA = false, isColumnA = false)
    )

    override fun render(grid: List<CharArray>) {
        val topRow = topRow()
        val bottomRow = bottomRow()
        val leftColumn = leftColumn()
        val rightColumn = rightColumn()
        val horizontal = bestHorizontalLine(thickness, lineStyle = lineStyle)
        val vertical = bestVerticalLine(thickness, lineStyle = lineStyle)

        if (fill == null) {
            // Corners
            grid[topRow][leftColumn] = bestCorner(top = true, left = true, thickness, rounded = rounded).c
            grid[topRow][rightColumn] = bestCorner(top = true, left = false, thickness, rounded = rounded).c
            grid[bottomRow][leftColumn] = bestCorner(top = false, left = true, thickness, rounded = rounded).c
            grid[bottomRow][rightColumn] = bestCorner(top = false, left = false, thickness, rounded = rounded).c

            // Horizontal lines
            if (rightColumn - leftColumn > 1) {
                grid.mergeHLine(topRow, leftColumn + 1, rightColumn - 1, horizontal)
                grid.mergeHLine(bottomRow, leftColumn + 1, rightColumn - 1, horizontal)
            }

            // Vertical lines
            if (bottomRow - topRow > 1) {
                grid.mergeVLine(topRow + 1, bottomRow - 1, leftColumn, vertical)
                grid.mergeVLine(topRow + 1, bottomRow - 1, rightColumn, vertical)
            }
        } else {
            fill?.let { fill ->
                grid.fill(topRow, leftColumn, bottomRow, rightColumn, fill)
            }
        }
    }

    fun isTouchingEdge(row: Int, column: Int): Boolean {
        val topRow = topRow()
        val bottomRow = bottomRow()
        val leftColumn = leftColumn()
        val rightColumn = rightColumn()

        return if (row == topRow || row == bottomRow) {
            column in leftColumn..rightColumn
        } else if (column == leftColumn || column == rightColumn) {
            row in topRow..bottomRow
        } else {
            false
        }
    }

    override fun isTouching(row: Int, column: Int): Boolean {
        return if (fill == null) {
            isTouchingEdge(row, column)
        } else {
            row in topRow()..bottomRow() && column in leftColumn()..rightColumn()
        }
    }

    fun touchingWaypoints(): List<TouchingWaypoint> {
        val result = mutableListOf<TouchingWaypoint>()
        for (connection in diagram.connections) {
            val first = connection.waypoints.firstOrNull() ?: break
            if (this.isTouchingEdge(first.row, first.column)) {
                result.add(
                    TouchingWaypoint(
                        first, connection.waypoints[1], ! first.isHorizontal
                    )
                )
            }
            val last = connection.waypoints.lastOrNull() ?: break
            if (this.isTouchingEdge(last.row, last.column)) {
                result.add(
                    TouchingWaypoint(
                        last, connection.waypoints[connection.waypoints.size - 2], last.isHorizontal
                    )
                )
            }
        }
        return result
    }

    fun contains(other: Shape) = other.rightColumn() <= rightColumn() && other.leftColumn() >= leftColumn() &&
        other.bottomRow() <= bottomRow() && other.topRow() >= topRow()

    //fun overlaps(other: Shape) = other.leftColumn() <= rightColumn() && other.rightColumn() >= leftColumn() &&
    //    other.topRow() <= bottomRow() && other.bottomRow() >= topRow()

    override fun controlPoints() = controlPoints

    override fun canMoveControlPoint(controlPoint: ControlPoint, toRow: Int, toColumn: Int): Boolean {
        if (controlPoint is CornerControlPoint) {
            // Don't allow a box to be 0 wide or 0 tall.
            if (controlPoint.isRowA && toRow == rowB) return false
            if (! controlPoint.isRowA && toRow == rowA) return false
            if (controlPoint.isColumnA && toColumn == columnB) return false
            if (! controlPoint.isColumnA && toColumn == columnA) return false
        }
        return super.canMoveControlPoint(controlPoint, toRow, toColumn)
    }

    override fun toString() = "Box $rowA,$columnA .. $rowB,$columnB"

}
