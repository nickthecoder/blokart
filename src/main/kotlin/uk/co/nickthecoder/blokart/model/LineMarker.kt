package uk.co.nickthecoder.blokart.model

import uk.co.nickthecoder.glok.scene.Side

class LineMarker(
    val north: Char,
    val east: Char,
    val south: Char,
    val west: Char
) {
    constructor(northSouth: Char, eastWest: Char) : this(northSouth, eastWest, northSouth, eastWest)
    constructor(all: Char) : this(all, all, all, all)

    val blocksBySide: Map<Side, BasicBlock> = if (north == south) {
        if (north == east) {
            val block = UniversalLineMarkerBlock(north)
            mapOf(
                Side.TOP to block,
                Side.RIGHT to block,
                Side.BOTTOM to block,
                Side.LEFT to block
            )
        } else {
            val vBlock = NorthSouthLineMarkerBlock(north)
            val hBlock = EastWestLineMarkerBlock(east)
            mapOf(
                Side.TOP to vBlock,
                Side.RIGHT to hBlock,
                Side.BOTTOM to vBlock,
                Side.LEFT to hBlock
            )
        }
    } else {
        mapOf(
            Side.TOP to NorthLineMarkerBlock(north),
            Side.RIGHT to EastLineMarkerBlock(east),
            Side.BOTTOM to SouthLineMarkerBlock(south),
            Side.LEFT to WestLineMarkerBlock(west)
        )
    }


    fun forDirection(side: Side): Char = when (side) {
        Side.TOP -> north
        Side.RIGHT -> east
        Side.BOTTOM -> south
        Side.LEFT -> west
    }

    override fun toString() = "LineMarker $north $east $south $west"
}
