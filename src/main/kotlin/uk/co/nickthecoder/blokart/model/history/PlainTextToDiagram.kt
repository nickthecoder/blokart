package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.Diagram
import uk.co.nickthecoder.blokart.model.DiagramImpl
import uk.co.nickthecoder.blokart.model.DocumentImpl
import uk.co.nickthecoder.glok.history.ChangeImpl

interface PlainTextToDiagram : DiagramChange {
    val fromRow: Int
    val toRow: Int

    val plainTextLines: List<String>
}

class PlainTextToDiagramImpl(
    override val document: DocumentImpl,
    override val fromRow: Int,
    override val toRow: Int
) : PlainTextToDiagram, ChangeImpl {

    override val label get() = "Plain-Text to Diagram"

    override val plainTextLines = document.textDocument.lines.slice(fromRow..toRow)
    override val diagram: Diagram = DiagramImpl(document, fromRow, toRow)

    init {
        Parser(diagram as DiagramImpl, plainTextLines).parse()
    }

    override fun redo() {
        document.diagrams.add(diagram)
        diagram.invalidate()
    }

    override fun undo() {
        document.diagrams.removeLast()
    }
}

interface DiagramToPlainText : DiagramChange

class DiagramToPlainTextImpl(
    override val document: DocumentImpl,
    override val diagram: Diagram,
) : DiagramToPlainText, ChangeImpl {

    override val label get() = "Plain-Text to Diagram"


    override fun redo() {
        document.diagrams.remove(diagram)
    }

    override fun undo() {
        document.diagrams.add(diagram)
        diagram.invalidate()
    }
}
