package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.*
import uk.co.nickthecoder.glok.history.ChangeImpl

interface NewShape : DiagramChange {
    val shape: Shape
}

internal class NewShapeImpl(override val shape: Shape) : NewShape, ChangeImpl {

    override val label get() = "New Shape"
    override val document get() = shape.diagram.document
    override val diagram get() = shape.diagram as DiagramImpl

    override fun redo() {
        when (shape) {
            is Box -> diagram.boxes.add(shape)
            is Connection -> diagram.connections.add(shape)
            is DiagramLabel -> diagram.labels.add(shape)
        }
    }

    override fun undo() {
        when (shape) {
            is Box -> diagram.boxes.remove(shape)
            is Connection -> diagram.connections.remove(shape)
            is DiagramLabel -> diagram.labels.remove(shape)
        }
    }
}
