package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.Connection
import uk.co.nickthecoder.blokart.model.Diagram
import uk.co.nickthecoder.blokart.model.Document
import uk.co.nickthecoder.blokart.model.LineMarker
import uk.co.nickthecoder.glok.history.ChangeImpl

interface ChangeLineMarker : DiagramChange {
    val shape : Connection
    val newStartMarker : LineMarker?
    val newMidMarker : LineMarker?
    val newEndMarker : LineMarker?
}

class ChangeLineMarkerImpl(
    override val shape : Connection,
    override val newStartMarker : LineMarker?,
    override val newMidMarker : LineMarker?,
    override val newEndMarker: LineMarker?
) : ChangeLineMarker, ChangeImpl {

    override val label get() = "Line Marker"
    override val diagram: Diagram get() = shape.diagram
    override val document: Document get() = shape.diagram.document

    private val oldStartMarker = shape.startMarker
    private val oldMidMarker = shape.midMarker
    private val oldEndMarker = shape.endMarker

    override fun redo() {
        shape.startMarker = newStartMarker
        shape.midMarker = newMidMarker
        shape.endMarker = newEndMarker
    }

    override fun undo() {
        shape.startMarker = oldStartMarker
        shape.midMarker = oldMidMarker
        shape.endMarker = oldEndMarker
    }

}
