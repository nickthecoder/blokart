package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.Connection
import uk.co.nickthecoder.blokart.model.Waypoint
import uk.co.nickthecoder.blokart.model.WaypointImpl
import uk.co.nickthecoder.glok.util.log

interface MoveWaypoint : ShapeChange<Connection>

/**
 * Used to remove pairs of redundant waypoints from [Connection.waypoints].
 * i.e. where the previous, current and next row/column are the same.
 */
private fun duplicateWaypoints(connection: Connection): List<Pair<Int, WaypointImpl>> {

    val waypoints = connection.waypoints
    var prev = waypoints.firstOrNull() ?: return emptyList()
    val result = mutableListOf<Pair<Int, WaypointImpl>>()

    for (i in 1 until waypoints.size) {
        val next = waypoints[i]
        if (next.row == prev.row && next.column == prev.column) {
            if (i != waypoints.size - 1) {
                result.add(Pair(i - 1, prev as WaypointImpl))
            }
            if (i != 1) {
                result.add(Pair(i, next as WaypointImpl))
            }
        }
        prev = next
    }

    return result
}

class PostMoveWaypoint(
    connection: Connection,
) : ShapeChangeImpl<Connection>(connection) {

    private val duplicates = duplicateWaypoints(shape)
    override val label get() = "Post-Move Box Corner"

    override fun redo() {
        for ((index, _) in duplicates.asReversed()) {
            shape.waypoints.removeAt(index)
        }
    }

    override fun undo() {
        for ((index, wayPoint) in duplicates) {
            shape.waypoints.add(index, wayPoint)
        }
    }
}

/**
 * Adds one or two additional waypoints.
 * Only one additional waypoint is added if the bend is at the start or end of the [Connection].
 */
class AddBendImpl(
    connection: Connection,
    val waypoint: WaypointImpl
) : ShapeChangeImpl<Connection>(connection) {

    override val label get() = "Add Bend"

    override fun redo() {
        val index = shape.waypoints.indexOf(waypoint)
        if (index < 0) {
            log.warn("preMoveWaypoint. Control point not found.")
            return
        }

        // NOTE. Order matters! (due to index no being updated).
        if (waypoint !== shape.waypoints.last()) {
            val after = shape.waypoints[index + 1]
            shape.waypoints.add(index + 1, WaypointImpl(shape, after.row, after.column, waypoint.isHorizontal))
        }
        if (index != 0) {
            val before = shape.waypoints[index - 1]
            shape.waypoints.add(index, WaypointImpl(shape, before.row, before.column, waypoint.isHorizontal))
        }
        waypoint.isHorizontal = ! waypoint.isHorizontal
    }

    override fun undo() {
        val index = shape.waypoints.indexOf(waypoint)
        if (index < 0) {
            log.warn("preMoveWaypoint. Control point not found.")
            return
        }

        if (index < shape.waypoints.size) {
            shape.waypoints.removeAt(index + 1)
        }
        if (index > 0) {
            shape.waypoints.removeAt(index - 1)
        }
        waypoint.isHorizontal = ! waypoint.isHorizontal
    }

}


/**
 * This must be in a batch, which starts ends with [PostMoveWaypoint] which removes co-linear wayPoints.
 *
 */
class MoveWaypointImpl(

    connection: Connection,
    val controlPoint: WaypointImpl,
    val newRow: Int,
    val newColumn: Int

) : MoveWaypoint, ShapeChangeImpl<Connection>(connection) {

    override val label get() = "Move Box Corner"

    private val oldRow = controlPoint.row
    private val oldColumn = controlPoint.column

    override fun redo() {
        moveTo(newRow, newColumn)
    }

    override fun undo() {
        moveTo(oldRow, oldColumn)
    }

    private fun moveTo(row: Int, column: Int) {

        val controlPoints = shape.controlPoints()
        val index = controlPoints.indexOf(controlPoint)
        val prev = if (index == 0) null else controlPoints[index - 1] as WaypointImpl
        val next = if (controlPoint === controlPoints.last()) null else controlPoints[index + 1] as WaypointImpl

        controlPoint.row = row
        controlPoint.column = column
        if (controlPoint.isHorizontal) {
            prev?.row = row
            next?.column = column
        } else {
            prev?.column = column
            next?.row = row
        }

    }
}


