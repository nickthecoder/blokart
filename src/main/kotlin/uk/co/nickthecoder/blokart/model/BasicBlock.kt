package uk.co.nickthecoder.blokart.model

import uk.co.nickthecoder.glok.scene.Side

/**
 * See [BlockMetaData] for details.
 */
abstract class BasicBlock(
    val prototype: Char,
) {
    val variations = mutableListOf<BlockVariation>()

    init {
        addVariation(prototype)
    }

    fun addVariation(
        c: Char,
        thickness: Int = 0,
        rounded: Boolean = false,
        lineStyle: Int = 0
    ) {
        val variation = BlockVariation(this, c, thickness, rounded, lineStyle)
        variations.add(variation)

        val existing = BlockMetaData.variationsByChar[c]
        if (existing == null) {
            BlockMetaData.variationsByChar[c] = variation
        }
    }

    override fun toString() = "${javaClass.simpleName} '$prototype'"
}

interface HorizontalPart
interface VerticalPart

class HorizontalBlock(prototype: Char) : BasicBlock(prototype), HorizontalPart
class VerticalBlock(prototype: Char) : BasicBlock(prototype), VerticalPart


abstract class LineCapBlock(prototype: Char, val direction: Side) : BasicBlock(prototype)

class HLineCapBlock(prototype: Char, isWest: Boolean) :
    LineCapBlock(prototype, if (isWest) Side.LEFT else Side.RIGHT), HorizontalPart

class VLineCapBlock(prototype: Char, isNorth: Boolean) :
    LineCapBlock(prototype, if (isNorth) Side.TOP else Side.BOTTOM), VerticalPart


abstract class LineMarkerBlock(prototype: Char) : BasicBlock(prototype)
class UniversalLineMarkerBlock(prototype: Char) : LineMarkerBlock(prototype), HorizontalPart, VerticalPart
class NorthLineMarkerBlock(prototype: Char) : LineMarkerBlock(prototype), VerticalPart
class SouthLineMarkerBlock(prototype: Char) : LineMarkerBlock(prototype), VerticalPart
class EastLineMarkerBlock(prototype: Char) : LineMarkerBlock(prototype), HorizontalPart
class WestLineMarkerBlock(prototype: Char) : LineMarkerBlock(prototype), HorizontalPart
class NorthSouthLineMarkerBlock(prototype: Char) : LineMarkerBlock(prototype), VerticalPart
class EastWestLineMarkerBlock(prototype: Char) : LineMarkerBlock(prototype), HorizontalPart


/**
// [isLeft] and [isTop] are named after the position of the corner within a box.
// i.e. '┌' `isTop=true isLeft=true`
 */
class CornerBlock(prototype: Char, val isTop: Boolean, val isLeft: Boolean) :
    BasicBlock(prototype), HorizontalPart, VerticalPart

class TBlock(prototype: Char, val missing: Side) :
    BasicBlock(prototype), HorizontalPart, VerticalPart

class CrossroadsBlock(prototype: Char) :
    BasicBlock(prototype), HorizontalPart, VerticalPart

class BackgroundBlock(prototype: Char) : BasicBlock(prototype)
