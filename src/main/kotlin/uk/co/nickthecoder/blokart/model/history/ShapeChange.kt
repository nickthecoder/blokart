package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.Shape
import uk.co.nickthecoder.glok.history.ChangeImpl

interface ShapeChange<S : Shape> : DiagramChange

abstract class ShapeChangeImpl<S : Shape>(open val shape: S) : ShapeChange<S>, ChangeImpl {

    override val document get() = shape.diagram.document
    override val diagram get() = shape.diagram

    protected var extraLines = 0


}
