package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.*
import uk.co.nickthecoder.glok.history.ChangeImpl

interface DeleteShape : DiagramChange {
    val shape: Shape
}

internal class DeleteShapeImpl(override val shape: Shape) : DeleteShape, ChangeImpl {

    override val label get() = "New Shape"
    override val document get() = shape.diagram.document
    override val diagram get() = shape.diagram as DiagramImpl

    private val index = when (shape) {
        is Box -> diagram.boxes.indexOf(shape)
        is DiagramLabel -> diagram.labels.indexOf(shape)
        is Connection -> diagram.connections.indexOf(shape)
        else -> - 1
    }

    override fun redo() {
        when (shape) {
            is Box -> diagram.boxes.remove(shape)
            is DiagramLabel -> diagram.labels.remove(shape)
            is Connection -> diagram.connections.remove(shape)
        }
    }

    override fun undo() {
        when (shape) {
            is Box -> diagram.boxes.add(index, shape)
            is DiagramLabel -> diagram.labels.add(index, shape)
            is Connection -> diagram.connections.add(index, shape)
        }
    }
}
