package uk.co.nickthecoder.blokart.model

interface ControlPoint {
    val shape: Shape
    val row: Int
    val column: Int
}

interface Waypoint : ControlPoint {
    /**
     * Is the line segment which precedes this [Waypoint] horizontal?
     */
    val isHorizontal: Boolean
}

class WaypointImpl(
    override val shape: Connection,
    row: Int,
    override var column: Int,
    override var isHorizontal: Boolean
) : Waypoint {

    override var row: Int = row
        set(v) {
            field = v
            shape.updateDescription()
        }

    override fun toString() = "Waypoint @ $row , $column ${if (isHorizontal) "--" else "|"}"
}

class CornerControlPoint(

    override val shape: Box,
    val isRowA: Boolean,
    val isColumnA: Boolean

) : ControlPoint {

    override val row: Int get() = if (isRowA) shape.rowA else shape.rowB
    override val column: Int get() = if (isColumnA) shape.columnA else shape.columnB

    override fun toString() = "Corner @ $row , $column"

}


class GroupControlPoint(

    override val shape: ShapeGroup,
    val isLower: Boolean,

) : ControlPoint {

    override val row: Int get() = if (isLower) shape.topRow() else shape.bottomRow()
    override val column: Int get() = if (isLower) shape.leftColumn() else shape.rightColumn()

    override fun toString() = "Corner @ $row , $column"

}

class LabelControlPoint(
    override val shape: DiagramLabel
) : ControlPoint {
    override val row get() = shape.row
    override val column get() = shape.fromColumn

    override fun toString() = "LabelControlPoint @ $row , $column"
}
