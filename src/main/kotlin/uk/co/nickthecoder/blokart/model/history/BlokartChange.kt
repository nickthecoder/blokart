package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.Diagram
import uk.co.nickthecoder.blokart.model.Document
import uk.co.nickthecoder.blokart.model.Shape
import uk.co.nickthecoder.glok.history.Change
import javax.swing.text.html.parser.Parser

/**
 * All changes to a [Document] implement this interface.
 * There are two basic types :
 *
 * * [PlainTextChange] : Only affects the plain-text part of the document
 *   These are created when you type/delete in the plain-text area of the document.
 * * [DiagramChange] : Only affects one of the document's diagrams.
 *   However, this can also indirectly alter the plain-text, if the diagram grows or shrinks
 *   in the Y direction.
 *
 * Some notable [DiagramChange]s :
 * * [PlainTextToDiagram] : A portion of the plain-text is converted to a [Diagram].
 * * [DiagramToPlainText] : The reverse of [PlainTextToDiagram].
 * * [InsertDiagram] : Adds a blank diagram region within the document.
 *
 * The model ([Document], [Diagram], [Shape] etc.) must only be changed within
 * `undo` and `redo` of a [BlokartChange], otherwise the `History` mechanism
 * will get out-of-sync.
 *
 * Note. [Parser] is only ever called from [PlainTextToDiagram], and therefore
 * it is allowed to mutate the model.
 */
interface BlokartChange : Change {
    override val document: Document
}

interface StructuralChange : BlokartChange

/**
 *
 */
interface DiagramChange : BlokartChange {

    val diagram: Diagram
}
