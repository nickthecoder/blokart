package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.Diagram
import uk.co.nickthecoder.blokart.model.DiagramImpl
import uk.co.nickthecoder.blokart.model.DocumentImpl
import uk.co.nickthecoder.glok.history.ChangeImpl

interface MergeDiagrams : StructuralChange {
    val oldDiagram: Diagram
    val before: Diagram?
    val after: Diagram?

    val newDiagram: Diagram
}

class MergeDiagramsImpl(

    override val oldDiagram: Diagram,
    override val before: Diagram?,
    override val after: Diagram?

) : MergeDiagrams, ChangeImpl {

    override val label get() = "Merge Diagrams"
    override val document get() = oldDiagram.document

    override val newDiagram: Diagram = DiagramImpl(
        document,
        before?.fromRow ?: oldDiagram.fromRow,
        after?.toRow ?: oldDiagram.toRow
    )

    init {
        copyShapes(oldDiagram, newDiagram as DiagramImpl)
        before?.let { copyShapes(it, newDiagram) }
        after?.let { copyShapes(it, newDiagram) }
    }

    private fun copyShapes(from: Diagram, to: DiagramImpl) {
        val rowDelta = from.fromRow - to.fromRow
        for (box in from.boxes) {
            to.boxes.add(box.copyTo(to).also { MoveShapeImpl.moveShape(it, rowDelta, 0) })
        }
        for (connection in from.connections) {
            to.connections.add(connection.copyTo(to).also { MoveShapeImpl.moveShape(it, rowDelta, 0) })
        }
        for (label in from.labels) {
            to.labels.add(label.copyTo(to).also { MoveShapeImpl.moveShape(it, rowDelta, 0) })
        }
    }

    override fun redo() {
        val diagrams = (document as DocumentImpl).diagrams
        diagrams.remove(oldDiagram)
        before?.let { diagrams.remove(it) }
        after?.let { diagrams.remove(it) }
        diagrams.add(newDiagram)
    }

    override fun undo() {
        val diagrams = (document as DocumentImpl).diagrams
        diagrams.remove(newDiagram)
        after?.let { diagrams.add(it) }
        before?.let { diagrams.add(it) }
        diagrams.add(oldDiagram)
    }
}
