package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.Box
import uk.co.nickthecoder.blokart.model.CornerControlPoint

interface MoveBoxCorner : ShapeChange<Box>

class MoveBoxCornerImpl(
    val box: Box,
    val controlPoint: CornerControlPoint,
    val newRow: Int,
    val newColumn: Int
) : MoveBoxCorner, ShapeChangeImpl<Box>(box) {

    override val label get() = "Move Box Corner"

    private val oldRow = controlPoint.row
    private val oldColumn = controlPoint.column


    override fun redo() {
        if (controlPoint.isRowA) {
            shape.rowA = newRow
        } else {
            shape.rowB = newRow
        }
        if (controlPoint.isColumnA) {
            shape.columnA = newColumn
        } else {
            shape.columnB = newColumn
        }
    }

    override fun undo() {
        if (controlPoint.isRowA) {
            shape.rowA = oldRow
        } else {
            shape.rowB = oldRow
        }
        if (controlPoint.isColumnA) {
            shape.columnA = oldColumn
        } else {
            shape.columnB = oldColumn
        }
    }
}
