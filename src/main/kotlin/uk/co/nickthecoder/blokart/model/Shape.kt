package uk.co.nickthecoder.blokart.model

import uk.co.nickthecoder.blokart.BlokartSettings
import uk.co.nickthecoder.blokart.model.history.*
import uk.co.nickthecoder.glok.property.boilerplate.ObservableString
import kotlin.math.max
import kotlin.math.min

/**
 * Part of a [Diagram].
 * Coordinates are saved as (row,column) relative to the [Diagram],
 * _not_ relative to the document as a whole.
 *
 * Start and end rows/columns are _both_ inclusive.
 * The coordinates refer to the center of a glyph, not at the left or right edges of the glyph.
 * Therefore, the width of a box is `[rightColumn] - [leftColumn] + 1`
 * The same applies for heights.
 */
interface Shape {

    val diagram: Diagram

    /**
     * A short, human-readable description of this shape.
     */
    val observableDescription: ObservableString
    val description: String

    /**
     * [topRow], [bottomRow], [leftColumn], [rightColumn] form the bounding box for this shape.
     */
    fun topRow(): Int

    /**
     * [topRow], [bottomRow], [leftColumn], [rightColumn] form the bounding box for this shape.
     */
    fun bottomRow(): Int

    /**
     * [topRow], [bottomRow], [leftColumn], [rightColumn] form the bounding box for this shape.
     */
    fun rightColumn(): Int

    /**
     * [topRow], [bottomRow], [leftColumn], [rightColumn] form the bounding box for this shape.
     */
    fun leftColumn(): Int

    /**
     * Returns `true` iff this shape is at [row], [column].
     * This is used to select shapes with the mouse pointer.
     *
     * Unfilled boxes only return `true` for points at the Box's edge (not its center).
     * (just as it would in a Vector drawing program, such as Inkscape).
     */
    fun isTouching(row: Int, column: Int): Boolean

    fun controlPoints(): List<ControlPoint>
    fun canMoveControlPoint(controlPoint: ControlPoint, toRow: Int, toColumn: Int) = true

    fun render(grid: List<CharArray>)

    fun copyTo(diagram: Diagram): Shape

    fun move(rowDelta: Int, columnDelta: Int) {
        var correctedColumnDelta = columnDelta
        val additionalShapes = if (BlokartSettings.moveLabelsWithBoxes && this is Box) diagram.labels.filter {
            this.contains(it)
        } else {
            emptyList()
        }
        val touchingWaypoints = if (BlokartSettings.moveConnectionsWithBoxes && this is Box) {
            touchingWaypoints()
        } else {
            emptyList()
        }

        diagram.document.history.batch {
            val newLeft = leftColumn() + correctedColumnDelta
            val newTop = topRow() + rowDelta
            val newBottom = bottomRow() + rowDelta
            if (newLeft < 0) {
                correctedColumnDelta -= newLeft
            }
            if (newTop < 0) {
                addChange(GrowDiagramImpl(diagram, true, - newTop))
            } else if (newBottom >= diagram.height) {
                addChange(GrowDiagramImpl(diagram, false, newBottom - diagram.height + 1))
            }
            addChange(MoveShapeImpl(this@Shape, rowDelta, correctedColumnDelta, additionalShapes, touchingWaypoints))

        }
    }
}

interface BoxOrConnection : Shape {
    var rounded: Boolean
    var thickness: Int
    var lineStyle: Int

    fun changeThickness(newThickness: Int) {
        ChangeThicknessImpl(this, newThickness).now()
    }

    fun changeLineStyle(newStyle: Int) {
        ChangeLineStyleImpl(this, newStyle).now()
    }

    fun changeRounded(newValue: Boolean) {
        ChangeRoundedImpl(this, newValue).now()
    }
}

fun List<CharArray>.fill(
    fromRow: Int, fromColumn: Int,
    toRow: Int, toColumn: Int,
    char: Char
) {
    for (row in fromRow..toRow) {
        for (column in fromColumn..toColumn) {
            this[row][column] = char
        }
    }
}

fun List<CharArray>.merge(row: Int, column: Int, blockVariation: BlockVariation) {
    val existing = this[row][column]
    val combination = blockVariation.combinations[existing]
    this[row][column] = combination ?: blockVariation.c
}

fun List<CharArray>.mergeHLine(row: Int, fromColumn: Int, toColumn: Int, bv: BlockVariation) {
    for (column in min(fromColumn, toColumn)..max(fromColumn, toColumn)) {
        merge(row, column, bv)
    }
}

fun List<CharArray>.mergeVLine(fromRow: Int, toRow: Int, column: Int, bv: BlockVariation) {
    for (row in min(fromRow, toRow)..max(fromRow, toRow)) {
        merge(row, column, bv)
    }
}


fun List<CharArray>.mergeHLineExclusive(row: Int, fromColumn: Int, toColumn: Int, bv: BlockVariation) {
    for (column in min(fromColumn, toColumn) + 1 until max(fromColumn, toColumn)) {
        merge(row, column, bv)
    }
}

fun List<CharArray>.mergeVLineExclusive(fromRow: Int, toRow: Int, column: Int, bv: BlockVariation) {
    for (row in min(fromRow, toRow) + 1 until max(fromRow, toRow)) {
        merge(row, column, bv)
    }
}
