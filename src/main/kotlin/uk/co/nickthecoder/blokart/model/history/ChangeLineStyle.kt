package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.BoxOrConnection
import uk.co.nickthecoder.blokart.model.Diagram
import uk.co.nickthecoder.blokart.model.Document
import uk.co.nickthecoder.glok.history.ChangeImpl

interface ChangeLineStyle : DiagramChange {
    val shape: BoxOrConnection
    val newLineStyle: Int
}

class ChangeLineStyleImpl(
    override val shape: BoxOrConnection,
    override val newLineStyle: Int
) : ChangeLineStyle, ChangeImpl {

    override val label get() = "Line Style"
    override val diagram: Diagram get() = shape.diagram
    override val document: Document get() = shape.diagram.document

    private val oldLineStyle = shape.lineStyle

    override fun redo() {
        shape.lineStyle = newLineStyle
    }

    override fun undo() {
        shape.lineStyle = oldLineStyle
    }
}

