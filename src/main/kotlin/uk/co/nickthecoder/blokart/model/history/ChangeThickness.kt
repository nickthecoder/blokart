package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.BoxOrConnection
import uk.co.nickthecoder.blokart.model.Diagram
import uk.co.nickthecoder.blokart.model.Document
import uk.co.nickthecoder.glok.history.ChangeImpl


interface ChangeThickness : DiagramChange {
    val shape: BoxOrConnection
    val newThickness: Int
}

class ChangeThicknessImpl(
    override val shape: BoxOrConnection,
    override val newThickness: Int
) : ChangeThickness, ChangeImpl {

    override val label get() = "Thickness"
    override val diagram: Diagram get() = shape.diagram
    override val document: Document get() = shape.diagram.document

    private val oldThickness = shape.thickness

    override fun redo() {
        shape.thickness = newThickness
    }

    override fun undo() {
        shape.thickness = oldThickness
    }

    // TODO Merge?
}

