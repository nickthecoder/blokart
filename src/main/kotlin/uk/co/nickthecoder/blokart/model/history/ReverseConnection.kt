package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.Connection
import uk.co.nickthecoder.blokart.model.WaypointImpl

class ReverseConnectionImpl(shape: Connection) : ShapeChangeImpl<Connection>(shape) {
    override val label get() = "Reverse Connection"

    override fun redo() {
        shape.waypoints.reverse()
        if (shape.waypoints.size % 2 == 0) {
            for (point in shape.waypoints) {
                (point as WaypointImpl).isHorizontal = ! point.isHorizontal
            }
        }

    }

    override fun undo() {
        redo()
    }
}
