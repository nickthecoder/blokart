package uk.co.nickthecoder.blokart.model

import uk.co.nickthecoder.glok.util.log

/**
 * See [BlockMetaData] for details.
 */
class BlockVariation(
    val basicBlock: BasicBlock,
    val c: Char,
    val thickness: Int,
    val rounded: Boolean,
    val lineStyle: Int
) {
    /**
     * The keys are characters that this [BlockVariation] can be combined with,
     * and the value is the combination of the two characters.
     */
    val combinations = mutableMapOf<Char, Char>()

    fun addCombination(with: Char, result: Char) {
        val existing = combinations[with]
        if (existing != null && existing != result) {
            log.warn("Combining '$c' with '$with' is defined as '$existing' but we are now setting it to '$result'")
        }
        combinations[with] = result
    }

    override fun toString() = "'$c' : $basicBlock thickness=$thickness rounded=$rounded lineStyle=$lineStyle combos=${combinations.size}"
}
