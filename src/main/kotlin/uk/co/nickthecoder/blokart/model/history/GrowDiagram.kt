package uk.co.nickthecoder.blokart.model.history

import uk.co.nickthecoder.blokart.model.Diagram
import uk.co.nickthecoder.blokart.model.DiagramImpl
import uk.co.nickthecoder.blokart.model.Document
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.history.ChangeImpl

interface GrowDiagram : DiagramChange {
    val atTop: Boolean
    val growBy: Int
}

class GrowDiagramImpl(
    override val diagram: Diagram,
    override val atTop: Boolean,
    override val growBy: Int
) : GrowDiagram, ChangeImpl {

    override val label get() = if (growBy > 0) "Grow Diagram" else "Shrink Diagram"
    override val document: Document get() = diagram.document

    override fun redo() {
        addRemoveLines(growBy)
        if (atTop) {
            for (shapes in diagram.allShapes) {
                for (shape in shapes) {
                    MoveShapeImpl.moveShape(shape, growBy, 0)
                }
            }
        }
        (diagram as DiagramImpl).toRow += growBy
    }

    override fun undo() {
        (diagram as DiagramImpl).toRow -= growBy
        if (atTop) {
            for (shapes in diagram.allShapes) {
                for (shape in shapes) {
                    MoveShapeImpl.moveShape(shape, - growBy, 0)
                }
            }
        }
        addRemoveLines(- growBy)
    }

    private fun addRemoveLines(lines: Int) {
        val row = if (atTop) diagram.fromRow else (diagram.toRow + if (lines < 0) lines else 0)
        document.changingPlainText {
            if (lines > 0) {
                document.textDocument.insert(
                    TextPosition(row, 0),
                    "\n".repeat(lines)
                )
            } else if (lines < 0) {
                document.textDocument.delete(
                    TextPosition(row, 0),
                    TextPosition(row - lines, 0)
                )
            }
        }
    }
}

