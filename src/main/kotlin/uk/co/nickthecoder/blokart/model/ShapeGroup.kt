package uk.co.nickthecoder.blokart.model

import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.property.boilerplate.SimpleStringProperty

class ShapeGroup(override val diagram: Diagram) : Shape {

    val shapes = mutableListOf<Shape>().asMutableObservableList()

    @Suppress("unused")
    private val shapesListener = shapes.addListener {
        description = "Group of ${shapes.size}"
    }

    override val observableDescription = SimpleStringProperty("Group of ${shapes.size}")
    override var description by observableDescription

    override fun leftColumn() = shapes.minOfOrNull { it.leftColumn() } ?: 0
    override fun rightColumn() = shapes.maxOfOrNull { it.rightColumn() } ?: 0
    override fun topRow(): Int = shapes.minOfOrNull { it.topRow() } ?: 0
    override fun bottomRow(): Int = shapes.maxOfOrNull { it.bottomRow() } ?: 0


    private val controlPoints = listOf(
        GroupControlPoint(this, true),
        GroupControlPoint(this, false)
    )

    override fun copyTo(diagram: Diagram): Shape {
        TODO("Not yet implemented")
    }

    override fun controlPoints(): List<ControlPoint> = if (shapes.isEmpty()) emptyList() else controlPoints

    override fun isTouching(row: Int, column: Int): Boolean {
        for (shape in shapes) {
            if (shape.isTouching(row, column)) return true
        }
        return false
    }

    override fun render(grid: List<CharArray>) {
    }

}

