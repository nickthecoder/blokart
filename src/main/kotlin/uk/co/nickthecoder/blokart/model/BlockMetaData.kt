package uk.co.nickthecoder.blokart.model

import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.text.BitmapFont
import kotlin.math.abs

/**
 * Metadata about each of the glyphs used drawing and parsing boxes and lines
 *
 * See [Box-drawing Characters](https://en.wikipedia.org/wiki/Box-drawing_character)
 *
 * ## Drawing
 *
 * Drawing with glyphs is tricky. Lines and boxes have various thickness and line styles.
 *
 *     ──────   ━━━━━━  ══════  ╌╌╌╌╌╌  ┄┄┄┄┄┄  ╍╍╍╍╍╍  ┅┅┅┅┅┅
 *
 * A [HorizontalBlock] `─` has many [BlockVariation]s `─`, `━`, `═` etc.
 *
 * But the line may also cross vertical lines, so a thin vertical line may end up being draw like so :
 *
 *     ─┴─╫──┼─┰─
 *
 * So we need to know which character to draw from a combination of two different glyphs.
 * e.g. `─` + `║` = `╫`
 *
 * When drawing a horizontal line which cuts through `║`, we first need to pick the [BlockVariation] :
 *
 *     val variation = [bestHorizontalLine] ( thickness = 0 )
 *
 * Then we need to combine it with the character already at that position in the diagram :
 *
 *     val finalCharacter = variation.combineWith( '║' )
 *
 * ## Parsing
 *
 * When we parse a plain-text document it is useful to understand the shape of each glyph, without
 * caring about thickness or line styles. e.g. When parsing this line of plain-text :
 *
 *     Hello ─┴─┐└──┼─┰─ World
 *
 * We need to look for horizontal lines, and we can see there are two. Notice there is no whitespace
 * separating them. `┐` is immediately followed by `└`.
 * To do this, for each character in the String, we first find its corresponding [BlockVariation] :
 *
 *     val variation = [variationsByChar].get(c).
 *
 * To get the information about the shape (ignoring line thickness, line styles etc.) :
 *
 *     val basicBlock : BasicBlock = variation.basicBlock
 *
 * Then test if it is horizontal :
 *
 *     if (basicBlock is HorizontalPart) ...
 *
 * But a horizontal line also ends if it is a corner...
 *
 *     if (basicBlock is CornerBlock) ...
 *
 */
object BlockMetaData {

    val variationsByChar = mutableMapOf<Char, BlockVariation>()

    val variationCharacters by lazy { variationsByChar.keys }

    val lineMarkersByChar = mutableMapOf<Char, LineMarker>()

    val lineMarkerCharacters by lazy { lineMarkersByChar.keys }

    // region ==== BasicBlocks ====
    private val crossroads = CrossroadsBlock('┼')
    private val horizontal = HorizontalBlock('─')
    private val vertical = VerticalBlock('│')

    // These are named after the position of the corner within a box.
    // i.e. '┌' `isTop=true isLeft=true`
    private val cornerTopLeft = CornerBlock('┌', isTop = true, isLeft = true)
    private val cornerTopRight = CornerBlock('┐', isTop = true, isLeft = false)
    private val cornerBottomLeft = CornerBlock('└', isTop = false, isLeft = true)
    private val cornerBottomRight = CornerBlock('┘', isTop = false, isLeft = false)

    private val tMissingTop = TBlock('┬', Side.BOTTOM)
    private val tMissingRight = TBlock('┤', Side.RIGHT)
    private val tMissingBottom = TBlock('┴', Side.BOTTOM)
    private val tMissingLeft = TBlock('├', Side.LEFT)

    val background = BackgroundBlock('░')

    // NOTE, These are named after the direction of the line,
    // Not, the position of the end-cap.
    private val northEndCap = VLineCapBlock('╷', true)
    private val eastEndCap = HLineCapBlock('╴', false)
    private val southEndCap = VLineCapBlock('╵', false)
    private val westEndCap = HLineCapBlock('╶', true)

    val defaultLineMarker = LineMarker('▲', '▶', '▼', '◀')
    // These characters aren't fixed width, and I don't know of another 2 way lineMarker, so I've commented
    // out this feature for now. Reintroduce it if I find suitable glyphs.
    //val northSouthLineMarker = LineMarker('⬮', '⬬')
    val universalLineMarker = LineMarker('⬤')
    val endCapsBySide = mapOf(
        northEndCap.direction to northEndCap,
        eastEndCap.direction to eastEndCap,
        southEndCap.direction to southEndCap,
        westEndCap.direction to westEndCap
    )

    /**
     * The basic blocks shapes which form boxes and lines.
     */
    private val basicBlocks: List<BasicBlock> = listOf(
        horizontal, vertical,
        crossroads,
        northEndCap, eastEndCap, southEndCap, westEndCap,

        cornerTopLeft, cornerTopRight, cornerBottomLeft, cornerBottomRight,

        tMissingTop, tMissingRight, tMissingBottom, tMissingLeft,

        defaultLineMarker.blocksBySide[Side.TOP] !!,
        defaultLineMarker.blocksBySide[Side.RIGHT] !!,
        defaultLineMarker.blocksBySide[Side.BOTTOM] !!,
        defaultLineMarker.blocksBySide[Side.LEFT] !!,

        //northSouthLineMarker.blocksBySide[Side.TOP] !!,
        //northSouthLineMarker.blocksBySide[Side.RIGHT] !!,

        universalLineMarker.blocksBySide[Side.TOP] !!,

        background
    )

    val basicBlocksByChar = basicBlocks.associateBy { it.prototype }

    // endregion ==== BasicBlocks ====

    // region ==== LineMarkers ====
    val lineMarkers: List<LineMarker> = listOf(
        lineMarker('▲', '▶', '▼', '◀'),
        lineMarker('△', '▷', '▽', '◁'),

        lineMarker('◆'),
        lineMarker('◇'),
        lineMarker('◈'),
        lineMarker('◯'),
        lineMarker('⬤'),
        lineMarker('⨁'),
        lineMarker('⨂'),
        lineMarker('◉'),
    )
    // endregion LineMarkers

    init {
        // Lines
        thicker('─', '━', '═')
        thicker('│', '┃', '║')

        dashed('─', '╌', '┄')
        dashed('│', '┆', '┊')
        dashed('━', '╍', '┅')
        dashed('┃', '┇', '┋')

        // endCaps
        thicker('╵', '╹')
        thicker('╶', '╺')
        thicker('╷', '╻')
        thicker('╴', '╸')

        thicker(background.prototype, '▒', '▓', '█')


        // Corners
        thicker('┌', '┏', '╔')
        rounded('┌', '╭')

        thicker('┐', '┓', '╗')
        rounded('┐', '╮')

        thicker('┘', '┛', '╝')
        rounded('┘', '╯')

        thicker('└', '┗', '╚')
        rounded('└', '╰')

        // Straight Lines
        combination('╶', '╴', horizontal, '─')
        combination('╵', '╷', vertical, '│')


        // T-Junctions
        combination('│', '╶', tMissingLeft, '├')
        combination('┆', '╶', tMissingLeft, '├')
        combination('┊', '╶', tMissingLeft, '├')
        combination('┋', '╶', tMissingLeft, '├')
        combination('┃', '╶', tMissingLeft, '┠')
        combination('║', '╶', tMissingLeft, '╟')

        combination('│', '╺', tMissingLeft, '┝')
        combination('┆', '╺', tMissingLeft, '├')
        combination('┊', '╺', tMissingLeft, '┝')
        combination('┋', '╺', tMissingLeft, '├')
        combination('┃', '╺', tMissingLeft, '┣')
        combination('║', '╺', tMissingLeft, '╟')


        combination('│', '╴',  tMissingRight,'┤')
        combination('┆', '╴', tMissingRight,'┤')
        combination('┊', '╴', tMissingRight,'┤')
        combination('┋', '╴', tMissingRight,'┤')
        combination('┃', '╴', tMissingRight,'┨')
        combination('║', '╴', tMissingRight,'╢')

        combination('│', '╸', tMissingRight,'┥')
        combination('┆', '╸', tMissingRight,'┥')
        combination('┊', '╸', tMissingRight,'┥')
        combination('┋', '╸', tMissingRight,'┥')
        combination('┃', '╸', tMissingRight,'┫')
        combination('║', '╸', tMissingRight,'╢')


        combination('─', '╵', tMissingBottom,'┴')
        combination('╌', '╵', tMissingBottom,'┴')
        combination('┄', '╵', tMissingBottom,'┴')
        combination('━', '╵', tMissingBottom,'┷')
        combination('═', '╵', tMissingBottom,'╧')

        combination('─', '╹', tMissingBottom,'┸')
        combination('╌', '╹', tMissingBottom,'┸')
        combination('┄', '╹', tMissingBottom,'┸')
        combination('━', '╹', tMissingBottom,'┻')
        combination('═', '╹', tMissingBottom,'╧')


        combination('─', '╷', tMissingTop,'┬')
        combination('╌', '╷', tMissingTop,'┬')
        combination('┄', '╷', tMissingTop,'┬')
        combination('━', '╷', tMissingTop,'┯')
        combination('═', '╷', tMissingTop,'╤')

        combination('─', '╻', tMissingTop,'┰')
        combination('╌', '╻', tMissingTop,'┰')
        combination('┄', '╻', tMissingTop,'┰')
        combination('━', '╻', tMissingTop,'┰')
        combination('═', '╻', tMissingTop,'╤')


        // Crossroads
        combineCrossroads('─', '│', '┼')
        combineCrossroads('╌', '│', '┼')
        combineCrossroads('┄', '│', '┼')
        combineCrossroads('━', '│', '┿')
        combineCrossroads('╍', '│', '┿')
        combineCrossroads('┅', '│', '┿')
        combineCrossroads('═', '│', '╪')

        combineCrossroads('─', '┆', '┼')
        combineCrossroads('╌', '┆', '┼')
        combineCrossroads('┄', '┆', '┼')
        combineCrossroads('━', '┆', '┿')
        combineCrossroads('╍', '┆', '┿')
        combineCrossroads('┅', '┆', '┿')
        combineCrossroads('═', '┆', '╪')

        combineCrossroads('─', '┊', '┼')
        combineCrossroads('╌', '┊', '┼')
        combineCrossroads('┄', '┊', '┼')
        combineCrossroads('━', '┊', '┿')
        combineCrossroads('╍', '┊', '┿')
        combineCrossroads('┅', '┊', '┿')
        combineCrossroads('═', '┊', '╪')

        combineCrossroads('─', '┃', '╂')
        combineCrossroads('╌', '┃', '╂')
        combineCrossroads('┄', '┃', '╂')
        combineCrossroads('━', '┃', '╋')
        combineCrossroads('╍', '┃', '╋')
        combineCrossroads('┅', '┃', '╋')
        combineCrossroads('═', '┃', '╪')

        combineCrossroads('─', '┇', '╂')
        combineCrossroads('╌', '┇', '╂')
        combineCrossroads('┄', '┇', '╂')
        combineCrossroads('━', '┇', '╋')
        combineCrossroads('╍', '┇', '╋')
        combineCrossroads('┅', '┇', '╋')
        combineCrossroads('═', '┇', '╪')

        combineCrossroads('─', '┋', '╂')
        combineCrossroads('╌', '┋', '╂')
        combineCrossroads('┄', '┋', '╂')
        combineCrossroads('━', '┋', '╋')
        combineCrossroads('╍', '┋', '╋')
        combineCrossroads('┅', '┋', '╋')
        combineCrossroads('═', '┋', '╪')

        combineCrossroads('─', '║', '╫')
        combineCrossroads('╌', '║', '╫')
        combineCrossroads('┄', '║', '╫')
        combineCrossroads('━', '║', '╫')
        combineCrossroads('╍', '║', '╫')
        combineCrossroads('┅', '║', '╫')
        combineCrossroads('═', '║', '╬')


        combineCrossroads( '╷', '┴', '┼' )
        combineCrossroads( '╷', '┷', '┿' )
        combineCrossroads( '╷', '┸', '┼' )
        combineCrossroads( '╷', '╧', '╪' )
        combineCrossroads( '╻', '┴', '┼' )
        combineCrossroads( '╻', '┸', '╂' )
        combineCrossroads( '╻', '┷', '┿' )
        combineCrossroads( '╻', '╧', '╪' )

        combineCrossroads( '╵', '┬', '┼' )
        combineCrossroads( '╵', '┰', '┼' )
        combineCrossroads( '╵', '┯', '┿' )
        combineCrossroads( '╵', '╤', '╪' )
        combineCrossroads( '╹', '┬', '┼' )
        combineCrossroads( '╹', '┰', '╋' )
        combineCrossroads( '╹', '┯', '┿' )
        combineCrossroads( '╹', '╤', '╪' )

        combineCrossroads( '╴', '├', '┼' )
        combineCrossroads( '╴', '┝', '╂' )
        combineCrossroads( '╴', '┠', '╂' )
        combineCrossroads( '╴', '╟', '╫' )
        combineCrossroads( '╸', '├', '┼' )
        combineCrossroads( '╸', '┝', '┿' )
        combineCrossroads( '╸', '┠', '╂' )
        combineCrossroads( '╸', '╟', '╫' )

        combineCrossroads( '╶', '┤', '┼' )
        combineCrossroads( '╶', '┥', '╂' )
        combineCrossroads( '╶', '┨', '╂' )
        combineCrossroads('╶', '╢', '╫')
        combineCrossroads('╺', '┤', '┼')
        combineCrossroads('╺', '┥', '┼')
        combineCrossroads('╺', '┨', '╂')
        combineCrossroads('╺', '╢', '╫')

        //println("Variation by Char : ")
        //for ((key, value) in variationsByChar) {
        //    println("'$key' -> $value")
        //}
    }

    private fun combineCrossroads(c1: Char, c2: Char, combination: Char) {
        combination(c1, c2, crossroads, combination)
    }

    private fun combination(c1: Char, c2: Char, basicBlock: BasicBlock, combination: Char) {
        val c1Variation = variationsByChar[c1] !!
        val c2Variation = variationsByChar[c2] !!

        c1Variation.addCombination(c2, combination)
        c2Variation.addCombination(c1, combination)

        basicBlock.addVariation(combination, c1Variation.thickness, c1Variation.rounded, c1Variation.lineStyle)
    }

    private fun lineMarker(c: Char): LineMarker {
        val lineMarker = LineMarker(c)

        lineMarkersByChar[c] = lineMarker

        universalLineMarker.blocksBySide[Side.TOP]?.addVariation(c)
        return lineMarker
    }

    private fun lineMarker(north: Char, east: Char, south: Char, west: Char): LineMarker {
        val lineMarker = LineMarker(north, east, south, west)

        lineMarkersByChar[north] = lineMarker
        lineMarkersByChar[east] = lineMarker
        lineMarkersByChar[south] = lineMarker
        lineMarkersByChar[west] = lineMarker

        defaultLineMarker.blocksBySide[Side.TOP]?.addVariation(north)
        defaultLineMarker.blocksBySide[Side.RIGHT]?.addVariation(east)
        defaultLineMarker.blocksBySide[Side.BOTTOM]?.addVariation(south)
        defaultLineMarker.blocksBySide[Side.LEFT]?.addVariation(west)

        return lineMarker
    }

    private fun thicker(prototype: Char, vararg derived: Char) {
        val source = variationsByChar[prototype] !!
        for ((index, c) in derived.withIndex()) {
            source.basicBlock.addVariation(
                c,
                source.thickness + 1 + index,
                source.rounded,
                source.lineStyle
            )
        }
    }

    private fun dashed(prototype: Char, vararg derived: Char) {
        val source = variationsByChar[prototype] !!
        for ((index, c) in derived.withIndex()) {
            source.basicBlock.addVariation(
                c,
                source.thickness,
                source.rounded,
                source.lineStyle + 1 + index
            )
        }
    }

    private fun rounded(prototype: Char, derived: Char) {
        val source = variationsByChar[prototype] !!
        source.basicBlock.addVariation(
            derived,
            source.thickness,
            true,
            source.lineStyle
        )
    }
}

fun bestHorizontalLine(thickness: Int = 0, lineStyle: Int = 0): BlockVariation {
    return BlockMetaData.basicBlocksByChar['─'] !!.variations.best(thickness, lineStyle)
}

fun bestVerticalLine(thickness: Int = 0, lineStyle: Int = 0): BlockVariation {
    return BlockMetaData.basicBlocksByChar['│'] !!.variations.best(thickness, lineStyle)
}

fun bestEndCap(side: Side, thickness: Int = 0): BlockVariation =
    BlockMetaData.endCapsBySide[side] !!.variations.best(thickness)

fun bestCorner(
    top: Boolean,
    left: Boolean,
    thickness: Int = 0,
    rounded: Boolean = false
): BlockVariation {
    val basic = if (top) {
        if (left) '┌' else '┐'
    } else {
        if (left) '└' else '┘'
    }
    return BlockMetaData.basicBlocksByChar[basic] !!.variations.filter { it.rounded == rounded }.best(thickness)
}

fun List<BlockVariation>.best(
    thickness: Int = 0,
    lineStyle: Int = 0
): BlockVariation {
    var best: BlockVariation? = null
    var bestScore = Int.MAX_VALUE
    for (graphic in this) {
        val score = abs(thickness - graphic.thickness) * 2 +
            abs(lineStyle - graphic.lineStyle)

        if (score < bestScore) {
            bestScore = score
            best = graphic
        }
    }
    return best ?: throw Exception("BlockVariation not : $this")
}
