package uk.co.nickthecoder.blokart

enum class Mode {
    SELECT, BOX, LINE, LABEL
}
