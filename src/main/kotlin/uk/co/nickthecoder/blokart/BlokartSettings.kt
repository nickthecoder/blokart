package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Color

object BlokartSettings : AbstractSettings(BlokartApp.preferences()) {

    // region ==== General ====

    val minXDistanceForMidLineMarkersProperty by intProperty(5)
    var minXDistanceForMidLineMarkers by minXDistanceForMidLineMarkersProperty

    val minYDistanceForMidLineMarkersProperty by intProperty(5)
    var minYDistanceForMidLineMarkers by minYDistanceForMidLineMarkersProperty

    val trimEndsOfLinesProperty by booleanProperty(true)
    var trimEndOfLines by trimEndsOfLinesProperty

    // endregion

    // region ==== Appearance ====

    val lineHeightProperty by floatProperty(1.2f)

    val toolIconSizeProperty by intProperty(32)

    // endregion Appearance

    // region ==== Layout ====

    val showMenuBarProperty by booleanProperty(true)

    val showToolBarProperty by booleanProperty(true)

    val showStatusBarProperty by booleanProperty(true)

    // endregion ==== Layout ====

    // region ==== Blokart Colors ====

    val darkDiagramTextColorProperty by colorProperty(Color["#0a0"])
    val lightDiagramTextColorProperty by colorProperty(Color["#0a0"])

    val darkControlPointColorProperty by colorProperty(Color["#0a02"])
    val lightControlPointColorProperty by colorProperty(Color["#0a02"])

    val darkDiagramBackgroundColorProperty by colorProperty(Color["#ccf"])
    val lightDiagramBackgroundColorProperty by colorProperty(Color["#ccf"])

    // endregion blokart colors

    // region ==== Comments ====
    val enableCommentsProperty by booleanProperty(true)
    var enableComments by enableCommentsProperty

    val commentsProperty by stringProperty("*\n//\n#")
    var comments by commentsProperty
    // endregion

    // region ==== Snippets ====
    val snippetsProperty by stringProperty(
        """
        Blokart Attribution (Markdown) : Created with [Blokart](https://gitlab.com/nickthecoder/blokart)
        Blokart Attribution (HTML) : Created with <a href="https://gitlab.com/nickthecoder/blokart">Blokart</a>
        Blokart Attribution (PlainText) : Created with Blokart : https://gitlab.com/nickthecoder/blokart
        
        """.trimIndent()
    )
    var snippets by snippetsProperty
    // endregion

    // region ==== Transient Settings ====
    // These don't appear in the SettingsDialog, they are toggle buttons in the toolbar.

    val moveLabelsWithBoxesProperty by booleanProperty(true)
    var moveLabelsWithBoxes by moveLabelsWithBoxesProperty

    val moveConnectionsWithBoxesProperty by booleanProperty(true)
    var moveConnectionsWithBoxes by moveConnectionsWithBoxesProperty

    // endregion

    //region ==== Places ====

    // endregion Places

    // region ==== Character Map ====
    val characterMapProperty by stringProperty(
        """
        [Symbols]
        ¡ ¢ £ ¤ ¥ ¦ § ¨ © « ¬ ® ¯ ° ± ² ³ µ ¶ ¹ » ¼ ½ ¾ ¿ × ÷
        
        [Arrows]
        ⮘ ⮙ ⮚ ⮛ ⮜ ⮝ ⮞ ⮟   
        ⬁ ⬂ ⬃ ⬄ ⬅ ⬆ ⬇ ⬈ ⬉ ⬊ ⬋ ⬌ ⬍
        
        [Currency]
        ₡ ₢ ₣ ₤ ₥ ₦ ₧ ₨ ₩ ₪ ₫ € ₭ ₮ ₯ ₰ ₱ ₲ ₳ ₴ ₵ ₶ ₷ ₸ ₹ ₺
        
        [Shapes]
        ▶ ▷ ▼ ▽ ◀ ◁ ▲ △ ◆ ◇ ◯ ⨁ ⨂ ◉ ◈
        ⬒ ⬓ ⬔ ⬕ ⬖ ⬗ ⬘ ⬙
        ⬚ ⬛ ⬜ ⬟ ⬠ ⬡ ⬢ ⬣ ⬤ ⬥ ⬦ ⬧ ⬨
        
        [Icons]
        ≡
        
        [Bullets]
        ✓ ✔ ✕ ✖ ✗ ✘ ✙ ✚ ✛ ✜ ✢ ✣ ✤ ✥ ✦ ✧ ✭ ✮ ✯ ✰ ✱ ✲ ✳ ✴ 
        ✵ ✶ ✷ ✸ ✹ ✺ ✻ ✼ ✽ ✾ ✿ ❀ ❁ ❂ ❃ ❄ ❅ ❆ ❇ ❈ ❉ ❊ ❋
        
        """.trimIndent()
    )
    // endregion Character Map

    // region ==== Window State ====

    val sceneWidthProperty by floatProperty(1000f)
    var sceneWidth by sceneWidthProperty

    val sceneHeightProperty by floatProperty(800f)
    var sceneHeight by sceneHeightProperty

    val maximizedProperty by booleanProperty(false)
    var maximized by maximizedProperty

    //val windowXProperty by intProperty(-1)
    //var windowX by windowXProperty

    //val windowYProperty by intProperty(-1)
    //var windowY by windowYProperty

    // endregion

    // region ==== init ====

    init {

        lineHeightProperty.bidirectionalBind(BlokartTheme.lineHeightProperty)

        darkDiagramTextColorProperty.bidirectionalBind(BlokartTheme.darkDiagramColorProperty)
        lightDiagramTextColorProperty.bidirectionalBind(BlokartTheme.lightDiagramColorProperty)
        darkDiagramBackgroundColorProperty.bidirectionalBind(BlokartTheme.darkDiagramBackgroundColorProperty)
        lightDiagramBackgroundColorProperty.bidirectionalBind(BlokartTheme.lightDiagramBackgroundColorProperty)
        darkControlPointColorProperty.bidirectionalBind(BlokartTheme.darkControlPointColorProperty)
        lightControlPointColorProperty.bidirectionalBind(BlokartTheme.lightControlPointColorProperty)

        rememberDefaultValues()
        autoSaveChanged()
    }


    override fun autoSaveIgnoredProperties(): Set<Property<*>> = setOf(
        sceneWidthProperty, sceneHeightProperty, maximizedProperty
    )

    // endregion init

}
