package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.State
import uk.co.nickthecoder.blokart.model.BoxOrConnection
import uk.co.nickthecoder.blokart.model.Shape
import uk.co.nickthecoder.blokart.model.history.ChangeLineStyleImpl
import uk.co.nickthecoder.glok.scene.dsl.choiceBox

class EditLineStyle(state: State) : EditShape(state, "Line Style") {

    private val choiceBox = choiceBox<Int> {
        overrideMinWidth = 70f
        items.addAll(0, 1, 2)
        converter = { value ->
            when (value) {
                1 -> "╎"
                2 -> "┊"
                else -> "│"
            }
        }
        value = 0
    }

    init {
        choiceBox.valueProperty.addListener { updateShape() }

        visibleProperty.bindTo(state.selectedShapeHasLinesProperty)

        inner.children.add(choiceBox)
    }

    private fun updateShape() {
        update {
            (selectedShape as? BoxOrConnection)?.let { shape ->
                ChangeLineStyleImpl(shape, choiceBox.value ?: 0).now()
            }
        }
    }

    override fun updateGUI(shape: Shape?) {
        update {
            if (shape is BoxOrConnection) {
                choiceBox.value = shape.lineStyle
            } else {
                choiceBox.value = 0
            }
        }
    }
}
