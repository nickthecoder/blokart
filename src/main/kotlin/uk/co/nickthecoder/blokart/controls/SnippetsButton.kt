package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.BlokartSettings
import uk.co.nickthecoder.blokart.State
import uk.co.nickthecoder.glok.control.MenuButton
import uk.co.nickthecoder.glok.control.WrappedNode
import uk.co.nickthecoder.glok.scene.dsl.menuItem

class SnippetsButton(state: State) : WrappedNode<MenuButton>(MenuButton("Snippet…")) {

    init {
        with(inner) {
            disabledProperty.bindTo(state.caretInDiagramProperty)
            onShowing {
                items.clear()
                for (line in BlokartSettings.snippets.split("\n")) {
                    val colon = line.indexOf(':')
                    if (colon > 0) {
                        val name = line.substring(0, colon).trim()
                        val text = line.substring(colon + 1).trim()
                        + menuItem(name) {
                            onAction {
                                state.currentTab?.documentView?.let { documentView ->
                                    documentView.document.changingPlainText {
                                        documentView.textArea.replaceSelection(text)
                                        documentView.textArea.requestFocus()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
