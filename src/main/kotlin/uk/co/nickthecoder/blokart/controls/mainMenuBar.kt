package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.BlokartActions
import uk.co.nickthecoder.blokart.BlokartSettings
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.scene.dsl.menuBar
import uk.co.nickthecoder.glok.theme.Tantalum

fun mainMenuBar(commands: Commands) = menuBar {
    visibleProperty.bindTo( BlokartSettings.showMenuBarProperty)

    commands.build(Tantalum.iconSizeProperty) {
        with(BlokartActions) {
            + menu(FILE) {
                + menuItem(FILE_NEW)
                + menuItem(FILE_OPEN)
                + menuItem(FILE_SAVE)
                + menuItem(FILE_SAVE_AS)
                + Separator()
                + menuItem(FILE_SETTINGS)
            }

            + menu(EDIT) {
                + menuItem(EDIT_UNDO)
                + menuItem(EDIT_REDO)
                + menuItem(EDIT_DELETE)
                + menuItem(SELECT_ALL)
                + menuItem(SELECT_NONE)
            }

            + menu(INSERT) {
                + menuItem(INSERT_PLAIN_BEFORE)
                + menuItem(INSERT_PLAIN_AFTER)
                + menuItem(INSERT_DIAGRAM)
                + menuItem(PLAIN_TO_DIAGRAM)
            }

            + menu(VIEW) {
                + checkMenuItem(VIEW_PLACES)
                + checkMenuItem(VIEW_CHARACTER_MAP)
                + checkMenuItem(VIEW_NODE_INSPECTOR)
            }

        }
    }
}
