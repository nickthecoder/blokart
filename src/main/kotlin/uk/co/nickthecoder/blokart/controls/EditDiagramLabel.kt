package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.State
import uk.co.nickthecoder.blokart.model.DiagramLabel
import uk.co.nickthecoder.blokart.model.Shape
import uk.co.nickthecoder.blokart.model.history.ChangeLabelImpl
import uk.co.nickthecoder.blokart.model.history.DeleteShapeImpl
import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.theme.styles.FIXED_WIDTH

class EditDiagramLabel(state: State) : EditShape(state, "Label") {

    private val textField = TextField().apply {
        style(FIXED_WIDTH)
        growPriority = 1f

        onKeyPressed { event ->
            if (event.key == Key.ENTER) {
                updateShape()
            }
        }
    }

    init {
        textField.focusedProperty.addChangeListener { _, _, focused ->
            if (! focused) {
                updateShape()
            }
        }

        growPriority = 1f
        visibleProperty.bindTo(state.labelSelectedProperty)

        inner.children.add(textField)
    }

    private fun updateShape() {
        update {
            (selectedShape as? DiagramLabel)?.let { label ->
                if (textField.text.isBlank()) {
                    DeleteShapeImpl(label).now()
                } else {
                    ChangeLabelImpl(label, textField.text).now()
                }
            }
        }
    }

    override fun updateGUI(shape: Shape?) {
        update {
            if (shape is DiagramLabel) {
                textField.text = shape.text
                textField.requestFocus()
                textField.caretIndex = 0
                textField.anchorIndex = textField.text.length
            }
        }
    }

}
