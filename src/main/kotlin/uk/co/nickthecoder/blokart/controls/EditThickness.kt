package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.State
import uk.co.nickthecoder.blokart.model.BoxOrConnection
import uk.co.nickthecoder.blokart.model.Shape
import uk.co.nickthecoder.blokart.model.history.ChangeThicknessImpl
import uk.co.nickthecoder.glok.scene.dsl.intSpinner


class EditThickness(state: State) : EditShape(state, "Thickness") {

    private val thicknessSpinner = intSpinner(0) {
        editor.prefColumnCount = 2
        max = 2
    }

    init {
        thicknessSpinner.valueProperty.addListener { updateShape() }

        visibleProperty.bindTo(state.selectedShapeHasLinesProperty)

        inner.children.add(thicknessSpinner)
    }

    private fun updateShape() {
        update {
            (selectedShape as? BoxOrConnection)?.let { shape ->
                ChangeThicknessImpl(shape, thicknessSpinner.value).now()
            }
        }
    }

    override fun updateGUI(shape: Shape?) {
        update {
            if (shape is BoxOrConnection) {
                thicknessSpinner.value = shape.thickness
            } else {
                thicknessSpinner.value = 0
            }
        }
    }

}
