package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.BlokartSettings
import uk.co.nickthecoder.blokart.State
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.dsl.*

fun mainStatusBar(state: State) = toolBar {
    visibleProperty.bindTo(BlokartSettings.showStatusBarProperty)

    side = Side.BOTTOM
    + label("Status") {
        textProperty.bindTo(state.selectedShapeDescription)
    }
    + formattedText(state.hintProperty) {}
    + spacer()

    + Separator()
    + vBox {
        + hBox {
            + text("X :")
            + text("") {
                alignment = Alignment.CENTER_RIGHT
                overridePrefWidth = 40f
                textProperty.bindTo(state.cursorColumnProperty.toObservableString(""))
            }
        }
        + hBox {
            + text("Y :")
            + text("") {
                alignment = Alignment.CENTER_RIGHT
                overridePrefWidth = 40f
                textProperty.bindTo(state.cursorRowProperty.toObservableString(""))
            }
        }
    }
}
