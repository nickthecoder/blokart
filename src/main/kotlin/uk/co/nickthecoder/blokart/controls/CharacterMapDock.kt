package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.BlokartSettings
import uk.co.nickthecoder.glok.control.TextAreaBase
import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.control.TextPosition
import uk.co.nickthecoder.glok.dock.Dock
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.property.boilerplate.OptionalNodeUnaryFunction
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.styles.FIXED_WIDTH


class CharacterMapDock(harbour: Harbour) : Dock(ID, harbour) {

    init {
        title = "Character Map"
        contentProperty.bindTo(OptionalNodeUnaryFunction(BlokartSettings.characterMapProperty) { settings ->
            buildContent(settings)
        })
    }

    private fun buildContent(settings: String): Node {
        val groups = mutableListOf<Pair<String, String>>()
        var groupName = "Unknown"
        var groupChars = ""
        fun finishGroup() {
            if (groupChars.isNotBlank()) {
                groups.add(Pair(groupName, groupChars))
            }
            groupChars = ""
        }
        for (line in settings.split("\n")) {
            if (line.startsWith("[") && line.endsWith("]")) {
                finishGroup()
                groupName = line.substring(1, line.length - 1)
            } else {
                groupChars += line
            }
        }
        finishGroup()

        return vBox {
            fillWidth = true

            toggleGroup {
                for ((name, chars) in groups) {

                    + accordionPane(name) {
                        padding(2)

                        content = scrollPane {
                            overridePrefHeight = 1000f

                            content = flowPane {
                                xSpacing = 4f
                                ySpacing = 4f

                                for (c in chars.filter { ! it.isWhitespace() }) {
                                    + button("$c") {
                                        style(FIXED_WIDTH)
                                        padding(2)
                                        focusAcceptable = false
                                        focusTraversable = false
                                        onAction { typeChar(c) }
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
    }

    private fun typeChar(c: Char) {
        val input = scene?.focusOwner
        when (input) {
            is TextField -> {
                input.replace(input.selectionStart, input.selectionEnd, c.toString())
                input.caretIndex = input.selectionEnd + 1
                input.anchorIndex = input.caretIndex
            }

            is TextAreaBase<*> -> {
                input.replace(input.selectionStart, input.selectionEnd, c.toString())
                input.caretPosition = TextPosition(input.selectionEnd.row, input.selectionEnd.column + 1)
                input.anchorPosition = input.caretPosition

            }
        }
    }

    companion object {
        const val ID = "CharacterMapDock"
    }
}
