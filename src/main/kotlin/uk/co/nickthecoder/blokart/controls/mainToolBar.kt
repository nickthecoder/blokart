package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.*
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.scene.dsl.menuItem
import uk.co.nickthecoder.glok.scene.dsl.spacer
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.scene.dsl.toolBar
import uk.co.nickthecoder.glok.theme.Tantalum

fun mainToolBar(commands: BlokartCommands, state: State) = toolBar {
    visibleProperty.bindTo(BlokartSettings.showToolBarProperty)

    commands.build(Tantalum.iconSizeProperty) {

        with(BlokartActions) {
            + button(FILE_NEW)
            + splitMenuButton(FILE_OPEN) {
                onShowing {
                    items.clear()
                    for (file in BlokartApp.recentFiles.allItems.take(15)) {
                        + menuItem(file.name) {
                            onAction { commands.openFile(file) }
                        }
                    }
                }
            }
            + button(FILE_SAVE)
            + button(FILE_SAVE_AS)

            + Separator()

            + button(EDIT_UNDO)
            + button(EDIT_REDO)

            + Separator()
            + button(INSERT_DIAGRAM)
            + button(INSERT_PLAIN_BEFORE)
            + button(INSERT_PLAIN_AFTER)
            + button(PLAIN_TO_DIAGRAM)
            + SnippetsButton(state)

            + spacer()

            // Options /  Settings
            + toggleButton(OPTION_MOVE_LABELS_WITH_BOXES)
            + toggleButton(OPTION_MOVE_CONNECTIONS_WITH_BOXES)
            + button(FILE_SETTINGS)

        }
    }
}
