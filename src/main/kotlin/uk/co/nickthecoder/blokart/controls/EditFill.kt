package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.State
import uk.co.nickthecoder.blokart.model.BlockMetaData
import uk.co.nickthecoder.blokart.model.Box
import uk.co.nickthecoder.blokart.model.Shape
import uk.co.nickthecoder.blokart.model.history.ChangeFillImpl
import uk.co.nickthecoder.glok.scene.dsl.choiceBox


class EditFill(state: State) : EditShape(state, "Solid") {

    private val choiceBox = choiceBox<Char?> {
        overrideMinWidth = 70f
        items.add(null)
        for (background in BlockMetaData.background.variations) {
            items.add(background.c)
        }
        converter = { value ->
            when (value) {
                null -> "No"
                else -> value.toString().repeat(6)
            }
        }
        // NOTE. The text value of the choiceBox is "<None>" unless we set, and then reset the value.
        // This is due to the converter being set AFTER the value is initially set to null.
        value = 'X'
        value = null
    }

    init {
        choiceBox.valueProperty.addListener { updateShape() }
        visibleProperty.bindTo(state.boxSelectedProperty)

        inner.children.add(choiceBox)
    }

    private fun updateShape() {
        update {
            (selectedShape as? Box)?.let { shape ->
                ChangeFillImpl(shape, choiceBox.value).now()
            }
        }
    }

    override fun updateGUI(shape: Shape?) {
        update {
            if (shape is Box) {
                choiceBox.value = shape.fill
            } else {
                choiceBox.value = null
            }
        }
    }
}
