package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.State
import uk.co.nickthecoder.blokart.model.BoxOrConnection
import uk.co.nickthecoder.blokart.model.Shape
import uk.co.nickthecoder.blokart.model.history.ChangeRoundedImpl
import uk.co.nickthecoder.glok.scene.dsl.toggleButton

class EditRounded(state: State) : EditShape(state, "Rounded") {

    private val roundedButton = toggleButton("╭") {
        overrideMinWidth = 50f
        selected = false
    }

    init {
        roundedButton.selectedProperty.addListener { updateShape() }

        visibleProperty.bindTo(state.selectedShapeHasLinesProperty)

        inner.children.add(roundedButton)
    }

    private fun updateShape() {
        update {
            (selectedShape as? BoxOrConnection)?.let { shape ->
                ChangeRoundedImpl(shape, roundedButton.selected).now()
            }
        }
    }

    override fun updateGUI(shape: Shape?) {
        update {
            if (shape is BoxOrConnection) {
                roundedButton.selected = shape.rounded
            } else {
                roundedButton.selected = false
            }
        }
    }
}
