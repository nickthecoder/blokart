package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.BlokartActions
import uk.co.nickthecoder.blokart.BlokartSettings
import uk.co.nickthecoder.blokart.State
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.spacer
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.glok.scene.dsl.toolBar
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.TINTED


fun selectionToolBar(commands : Commands, state : State) = toolBar {
    // Prevent shrinkage when nothing is selected.
    overrideMinHeight = 46f

    commands.build(Tantalum.iconSizeProperty) {

        with(BlokartActions) {

            + label("Nothing Selected") {
                visibleProperty.bindTo(state.selectedShapeProperty.isNull())
            }

            // Selected Shape controls...

            + EditDiagramLabel(state)
            + EditFill(state)
            + EditRounded(state)
            + EditThickness(state)
            + EditLineStyle(state)
            + EditLineMarkers(state)
            + button(EDIT_REVERSE_CONNECTION) {
                visibleProperty.bindTo(state.connectionSelectedProperty)
            }

            // Without this, when EditDiagramLabel is the only node showing, the
            // toolbar sometimes places it in the "overflow". Probably a bug in ToolBar.
            + spacer()
        }
    }
}
