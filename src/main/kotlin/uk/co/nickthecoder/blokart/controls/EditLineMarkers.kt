package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.State
import uk.co.nickthecoder.blokart.model.BlockMetaData
import uk.co.nickthecoder.blokart.model.Connection
import uk.co.nickthecoder.blokart.model.LineMarker
import uk.co.nickthecoder.blokart.model.Shape
import uk.co.nickthecoder.blokart.model.history.ChangeLineMarkerImpl
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.choiceBox


class EditLineMarkers(state: State) : EditShape(state, "Markers") {

    private val startMarker = choiceBox<LineMarker?> {
        overrideMinWidth = 100f
        tooltip = TextTooltip("Start Line-Marker")
        converter = { value -> value?.west?.toString() ?: "None" }
        items.add(null)
        items.addAll(BlockMetaData.lineMarkers)
        value = BlockMetaData.defaultLineMarker
        value = null
    }
    private val midMarker = choiceBox<LineMarker?> {
        overrideMinWidth = 100f
        tooltip = TextTooltip("Middle Line-Marker")
        converter = { value -> value?.east?.toString() ?: "None" }
        items.add(null)
        items.addAll(BlockMetaData.lineMarkers)
        value = BlockMetaData.defaultLineMarker
        value = null
    }
    private val endMarker = choiceBox<LineMarker?> {
        overrideMinWidth = 100f
        tooltip = TextTooltip("End Line-Marker")
        converter = { value -> value?.east?.toString() ?: "None" }
        items.add(null)
        items.addAll(BlockMetaData.lineMarkers)
        value = BlockMetaData.defaultLineMarker
        value = null
    }

    init {
        startMarker.valueProperty.addListener { updateShape() }.also {
            midMarker.valueProperty.addListener(it)
            endMarker.valueProperty.addListener(it)
        }
        state.selectedShapeProperty.addChangeListener { _, _, shape -> updateGUI(shape) }

        visibleProperty.bindTo(state.connectionSelectedProperty)

        inner.children.addAll(startMarker, midMarker, endMarker)
    }

    private fun updateShape() {
        update {
            (selectedShape as? Connection)?.let { shape ->
                ChangeLineMarkerImpl(shape, startMarker.value, midMarker.value, endMarker.value).now()
            }
        }
    }

    override fun updateGUI(shape: Shape?) {
        update {
            if (shape is Connection) {
                startMarker.value = shape.startMarker
                midMarker.value = shape.midMarker
                endMarker.value = shape.endMarker
            } else {
                startMarker.value = null
                midMarker.value = null
                endMarker.value = null
            }
        }
    }
}
