package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.State
import uk.co.nickthecoder.blokart.model.Shape
import uk.co.nickthecoder.glok.control.HBox
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.control.WrappedNode
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.dsl.spacing

// TODO Undo/redo won't update the GUI

abstract class EditShape(state: State, label: String) : WrappedNode<HBox>(HBox()) {

    protected var ignoreUpdates = false
    protected val selectedShape by state.selectedShapeProperty

    init {
        state.selectedShapeProperty.addChangeListener { _, _, shape ->
            updateGUI(shape)
        }

        with(inner) {
            spacing(10)
            alignment = Alignment.CENTER_LEFT

            + Separator()
            + Label(label)
        }
    }

    protected fun update(block: () -> Unit) {
        if (! ignoreUpdates) {
            ignoreUpdates = true
            block()
            ignoreUpdates = false
        }
    }

    protected abstract fun updateGUI(shape: Shape?)
}
