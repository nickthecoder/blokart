package uk.co.nickthecoder.blokart.controls

import uk.co.nickthecoder.blokart.BlokartActions
import uk.co.nickthecoder.blokart.BlokartSettings
import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.dsl.toolBar
import uk.co.nickthecoder.glok.theme.Tantalum

fun modeToolBar(commands: Commands) = toolBar(Side.LEFT) {

    commands.build(BlokartSettings.toolIconSizeProperty) {

        with(BlokartActions) {
            + propertyRadioButton2(MODE_SELECT)
            + propertyRadioButton2(MODE_BOX)
            + propertyRadioButton2(MODE_LINE)
            + propertyRadioButton2(MODE_LABEL)
        }
    }
}
