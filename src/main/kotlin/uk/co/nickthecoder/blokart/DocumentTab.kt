package uk.co.nickthecoder.blokart

import uk.co.nickthecoder.blokart.model.DiagramImpl
import uk.co.nickthecoder.blokart.model.DocumentImpl
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dialog.alert
import uk.co.nickthecoder.glok.property.boilerplate.ObservableString
import uk.co.nickthecoder.glok.property.boilerplate.StringBinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.optionalFileProperty
import java.io.File

class DocumentTab(file: File?) : Tab("") {

    val documentView = DocumentView(DocumentImpl())

    // region ==== Properties ====
    val fileProperty by optionalFileProperty(file)
    var file by fileProperty

    val modeProperty = documentView.modeProperty
    // endregion Properties

    /**
     * Either "New Document", or the full path of [file].
     * If the document has been modified, then it is prefixed by `"* "`.
     * This is used for the window's title.
     */
    val titleProperty: ObservableString =
        StringBinaryFunction(fileProperty, documentView.document.history.isSavedProperty) { f, saved ->
            if (f == null) {
                "New Document"
            } else {
                if (saved) {
                    "${f.name} @ ${f.parent}"
                } else {
                    "${f.name} @ ${f.parent} *"
                }
            }
        }

    init {
        documentView.document.history.saved()
        textProperty.bindTo(
            StringBinaryFunction(fileProperty, documentView.document.history.isSavedProperty) { f, saved ->
                if (f == null) {
                    "New Document"
                } else {
                    if (saved) {
                        f.name
                    } else {
                        "${f.name} *"
                    }
                }
            }
        )

        content = documentView
        if (file == null) {
            documentView.document.setText("\n\n───\n\n")
            (documentView.document.diagrams[0] as DiagramImpl).apply {
                boxes.clear()
                connections.clear()
                labels.clear()
            }
        } else {
            documentView.document.setText(file.readText())
        }
        documentView.textArea.document.history.clear()
        documentView.document.history.clear()
        documentView.textArea.caretPosition = TextPosition(0, 0)
        documentView.textArea.anchorPosition = documentView.textArea.caretPosition

        onCloseRequested { event ->
            if (! documentView.document.history.isSaved) {
                confirmClose()
                event.consume()
            }
        }
    }

    private fun confirmClose() {
        val myStage = scene?.stage ?: return
        alert(
            myStage, "Save Document?", "Save Document?",
            "Do you want to save before closing?".trimIndent(),
            AlertType.CONFIRMATION, listOf(ButtonType.SAVE, ButtonType.DISCARD, ButtonType.CANCEL)
        ) { buttonType ->
            when (buttonType) {
                ButtonType.DISCARD -> {
                    tabBar?.tabs?.remove(this)
                }

                ButtonType.SAVE -> {
                    save()
                    if (documentView.document.history.isSaved) {
                        tabBar?.tabs?.remove(this)
                    }
                }

                else -> {}
            }
        }
    }

    fun save() {
        val file = file
        if (file == null) {
            saveAs()
        } else {
            val doc = documentView.document.textDocument
            val diagrams = documentView.document.diagrams
            val text = if (BlokartSettings.trimEndOfLines) {
                val trimmed = doc.lines.mapIndexed { row, line ->
                    if (diagrams.firstOrNull { row >= it.fromRow && row <= it.toRow } == null) {
                        line
                    } else {
                        line.trimEnd()
                    }
                }
                trimmed.joinToString(separator = "\n")
            } else {
                doc.text
            }
            file.writeText(text)
            documentView.document.history.saved()
        }
    }

    fun saveAs(close: Boolean = false) {
        FileDialog().apply {
            title = "Save As"
            file?.parentFile?.let { initialDirectory = it }
            extensions.add(ExtensionFilter("All files", "*"))
            showSaveDialog(scene !!.stage !!) { file ->
                if (file != null) {
                    this@DocumentTab.file = file
                    BlokartApp.recentFiles.remember(file)
                    save()
                    if (close) {
                        tabBar?.tabs?.remove(this@DocumentTab)
                    }
                }
            }
        }
    }

}
