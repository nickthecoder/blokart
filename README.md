Blokart
=======

Edit `ascii-art` diagrams embedded within a text document.

![screenshot](screenshot.png)

They say a picture is worth a thousand words (but takes up a million bytes;-)
So why not draw diagrams using text.

I use Blokart to edit diagrams within comment sections of my source code.

Note, the term `ascii-art` is misleading, because the ascii character-set doesn't contain
the glyphs required for drawing boxes. But `unicode-art` doesn't have the same ring to it!

Here's a random diagram showing many box and line types :

      ┌─────────┐    lines      ┏━━━━━━━━┓  Line markings...         ╔═══════════════╗
      │ Boxes   ├──────────────▶┃ Thick  ┃◯─────▷──────┐             ║ Double-Walled ║
      └─────────┘               ┗━━━━━━━━┛             │             ╚═══════════════╝
                                 △                     ◯                   △
      ┏━━━━━━━━━━━━━━━━━━━┓      │                        ╻                │
      ┃                   ┃      │                        ┃                │
      ┠─────┬─────────────┨      └────────────────────────╂────────────────┘
      ┃     │             ┃          Where lines cross... ┃
      ┃     │             ┃                               ┃
      ┃     │             ┃                               ┃
      ┃     │             ┃                               ╹
      ┗━━━━━┷━━━━━━━━━━━━━┛

Look in the [examples](examples/index.md) folder, for more examples.


See [Box Drawing Characters](https://en.wikipedia.org/wiki/Box-drawing_character)

Status
======

It's finished!
There are a few bugs to squash, but I'm happily using it to document my other projects.


How it Works
============

Blokart loads and saves plain text documents.
However, when a document is loaded, it is scanned for boxes and lines etc.
and these parts of the document are stored as `Diagram` and `Shape` objects.

The remaining (plain-text) parts of the document are edited just like a regular text-editor.

The diagram editor knows nothing about the plain-text, only the `Shape` objects.
So moving and resizing boxes and lines is similar to using a vector diagram editor,
such as Inkscape.

Whenever a diagram is changed, the corresponding plain-text is rebuilt.

There are only 3 `Shape` types :

1. `Box`
2. `Connection` : A line with a start and end point and any number of way-points.
3. `DiagramLabel` : Text, which is edited as it would be in a vector diagram editor.
   i.e. Move it by dragging.

Dependencies
============

Blokart uses has 3 external dependencies :

1. A Java virtual machine (aka JRE)
2. `Kotlin's` runtime library (Blokart is written in Kotlin).
3. `Glok` (a GUI toolkit, also written by me).


Compile, Build, Run
===================

Compile and build :

    ./gradlew

Run using gradle :

    ./gradlew run

Run directly from the command line :

    build/install/blokart/bin/blokart

Dokka
=====

The default CSS used by Dokka doesn't work well with Blokart.
Code blocks have a large line-height (specified in pixels - WTF?), and the font doesn't
keep all glyphs the same width (I'm not sure why). This is the offending CSS in styles/style.css :

To fix these issues, I add the following to the end of styles/style.css :

    code.block { font-family: monospace; line-height: 120%; }

Which can be achieved using the following command (unix-like only, not Windows) :

    echo 'code.block { font-family: monospace; line-height: 120%; } /* fix for blokart */' >> styles/style.css

But if you wish to include this as part of an automated build,
the following is better, as it ensures the fix is only added once.

    grep blokart styles/style.css > /dev/null || echo 'code.block { font-family: monospace; line-height: 120%; } /* fix for blokart */' >> styles/style.css

Here's a gradle task which does this :

    // Fix Dokka's style sheet, overriding code-block's line height and font-family.
    task<Exec>("fixDokka") {
        dependsOn(":dokkaHtmlMultiModule")
        val src = "build/dokka/htmlMultiModule/" // Change to "build/dokka/html/" for single module projects???
        commandLine( "bash", "-c", "grep blokart '$src/styles/style.css' > /dev/null || echo 'code.block { font-family: monospace; line-height: 120%; } /* fix for blokart */' >> '$src/styles/style.css'" )
    }

KDoc
====

KDoc claims that asterisks are optional :

> Just like with Javadoc, KDoc comments start with /** and end with */. Every line of the comment may begin with an asterisk, which is not considered part of the contents of the comment.

However, when I omit them, KDoc doesn't detect code blocks.
So this is how I format KDoc comments which include diagrams (or other blocks) :

    /**
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    
    *    ┌───────────┐
    *    │  Diagram  │
    *    │           │
    *    └───────────┘

    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.    
    */
    class Foo {
    }

Note the extra white space after the asterisks (I think 4 spaces are required).
    
IntelliJ
========

By default, comments in code from external libraries use `Rendered View`.
But this is broken. It squashes the comments into about 40 columns, so diagrams are mangled.

You can turn this off, ad-hoc, by clicking the icon in the left gutter.
Or permanently using :

    File -> Settings -> Editor -> Render Mode -> Render Documentation Comments (untick)
